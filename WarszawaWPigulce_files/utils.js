window.avUtils = window.avUtils || {
     createCallback: function(fn, args, scope) {
         return function() {
             if(!fn){
                 return this.emptyFn;
             }

             var fnArgs = [].concat(args || []);
             fnArgs.push.apply(fnArgs, arguments);
             return fn.apply(scope || this, fnArgs);
         };
     },

    emptyFn: function () {},

    loadResource: function(url, callback, async) {
        var xmlhttp;
        xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                callback(xmlhttp.responseText);
            }
        };

        xmlhttp.open("GET", avDebug.avReplaceUrl(url), (async || true));
        xmlhttp.send();
    },

    loadExternalScript: function (nodeForScript, url, successCallback, failureCallback) {
        var head = nodeForScript || document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.onload = this.createCallback(successCallback, [], this);
        script.onerror = this.createCallback(failureCallback, [], this);
        head.appendChild(script);
    },

    applyTemplate: function (template, properties) {
        var returnValue = "";
        var templateFragments = template.split("((");
        returnValue += templateFragments[0];
        for (var i = 1; i < templateFragments.length; i++) {
            var fragmentSections = templateFragments[i].split("))", 2);
            returnValue += properties[fragmentSections[0]];
            returnValue += fragmentSections[1];
        }
        return returnValue;
    },

    getPlacementDetailsUrl: function(placementName, adType, dimensions, adsAmount) {
        var baseUrl = avConfig.getPlacementByName(placementName).urlScript + "placement_details_" + dimensions;

        var avatarTypes = ["awatar"];
        var exclusiveTypes = ["exclusive_big", "exclusive_narrow", "exclusive_normal", "exclusive_sky"];
        var imageTypes = ["imgDB", "imgDP", "imgK", "imgMP", "imgSP", "imgSW", "img750x100", "img750x200"];
        var textTypes = ["box"];

        if (avatarTypes.indexOf(adType) !== -1) {
            return baseUrl + "_avatar.html"
        } else if (exclusiveTypes.indexOf(adType) !== -1) {
            return baseUrl + "_exclusive.html";
        } else if (imageTypes.indexOf(adType) !== -1) {
            return baseUrl + "_image.html";
        } else if (textTypes.indexOf(adType) !== -1) {
            if (adsAmount === 1) {
                return baseUrl + "_text_1.html";
            } else {
                return baseUrl + "_text_2.html";
            }
        } else {
            return baseUrl + ".html";
        }
    },

    addShadow: function(iframeOfPlacement, adType) {
        iframeOfPlacement.style.boxShadow = "1px 2px 15px darkgrey";
        if(adType === "imgDP") {
            iframeOfPlacement.style.borderRadius = "10px 10px 0px 0px"
        } else {
            iframeOfPlacement.style.borderRadius = "10px";
        }
    },

    setLabelUrl : function(placementContent, labelUrl) {
        var placement = placementContent.parentElement.querySelector(".labelUrl");
        if(placement){
            placement.href = labelUrl;
        }
    },

    logToServer: function(severity, placementName, messageObject){
        var messageString = 'no message';
        var url = avConfig.scriptUrl;

        if(messageObject) {
            messageString = '';
            for (var key in messageObject) {
                var value = messageObject[key];
                messageString += key + ':' + value + "_";
            }
        }
        if(placementName){
            url = avConfig.getPlacementByName(placementName).urlScript;
            messageString = placementName +':'+ messageString;
        }
        var finalUrl = url  + severity + '?message='+encodeURI(messageString);
        avUtils.loadExternalScript(document.getElementsByTagName('head')[0], finalUrl, function(){});
    },

    // Object created for changing size of specific elements with text
    avFonts: {

        scaleFont: function(texts) {
            var cssOverflowProps = [];
            var properlySized = [];
            var min = [];
            var max = [];

            function pushInitialValues(i){
                var maxHeight= texts[i].clientHeight;
                max.push(maxHeight);
                min.push(2); //min height
                properlySized.push(false);
            }

            function saveCssOveflowProp(i){
                cssOverflowProps.push(texts[i].style.overflow);
                if ( !cssOverflowProps[i] || cssOverflowProps[i] === "visible" ){
                    texts[i].style.overflow = "hidden";
                }
            }

            function recoverCssOverflowProp(i) { texts[i].style.overflow = cssOverflowProps[i]; }

            function isEveryProperlyScaled(){ return properlySized.indexOf(false)==-1; }

            function isProperlyScaled(i){ return properlySized[i]==true; }

            function setProperlyScaled(i) { properlySized[i]=true; }

            function writeNewFontSize (i) { texts[i].style.fontSize = ~~((max[i] + min[i]) / 2) + "px"; }

            function isOverflowing(elem) {
                return elem.clientWidth < elem.scrollWidth
                    || elem.clientHeight < elem.scrollHeight;
            }

            function checkAndBinarySearchValue (i) {
                if (max[i] >= min[i]){
                    var middle = ~~((max[i] + min[i]) / 2);
                    if (isOverflowing(texts[i])) {
                        max[i] = middle - 1;
                    } else {
                        min[i] = middle + 1;
                    }
                } else {
                    setProperlyScaled(i);
                }
            }

            //main algorithm
            for (var i = 0; i<texts.length; i++){
                pushInitialValues(i); //reads DOM
            } //two loops because of layout trashing
            for (var i = 0; i<texts.length; i++){
                saveCssOveflowProp(i); //writes DOM
            }
            while(!isEveryProperlyScaled()) {
                for (var i = 0; i<texts.length; i++){
                    if(!isProperlyScaled(i)) {
                        writeNewFontSize(i);  //writes DOM
                    }
                } //two loops because of layout trashing
                for (var i = 0; i<texts.length; i++){
                    if(!isProperlyScaled(i)){
                        checkAndBinarySearchValue(i); //reads DOM
                    }
                }
            }
            for (var i = 0; i < texts.length; i++) {
                recoverCssOverflowProp(i); //writes DOM
            }
        },

        correctScaling: function(texts) {
            var elements = {};

            for (var i = 0; i < texts.length; i++) {
                var node = texts[i];
                var nodeClasses = node.classList.toString();

                if (typeof(elements[nodeClasses]) === "undefined") {
                    elements[nodeClasses] = [node];
                } else {
                    elements[nodeClasses].push(node);
                }
            }

            function setToAllSize(sizePx, nodes) {
                for (var i = 0; i < nodes.length; i++) {
                    var node = nodes[i];
                    node.style.fontSize = sizePx + "px";
                }
            }

            function correctIfStillOneOff(sizePx, nodes) {
                for (var i = 0; i < nodes.length; i++) {
                    var node = nodes[i];
                    if (node.clientWidth <= node.scrollWidth
                        || node.clientHeight <= node.scrollHeight) {
                        sizePx -= 1;
                    }
                }
                setToAllSize(sizePx, nodes);
            }

            for (var key in elements) {
                var fixedFontSizePx = 999;

                var nodesToFix = elements[key];
                for (var i = 0; i < nodesToFix.length; i++) {
                    var node = nodesToFix[i];
                    var nodeFontSizePx = parseInt(node.style.fontSize.replace("px", ""));
                    if (fixedFontSizePx > nodeFontSizePx) {
                        fixedFontSizePx = nodeFontSizePx;
                    }
                }

                setToAllSize(fixedFontSizePx, nodesToFix);
                correctIfStillOneOff(fixedFontSizePx, nodesToFix);
            }


        },

        createParentObserverToScaleFont: function(parentNode, childNodesCssSelector) {
            if (typeof MutationObserver === "undefined") {
                // IE 10, bad performance
                parentNode.addEventListener("DOMNodeInserted", function(e) {
                    var nodesToScale = parentNode.querySelectorAll(childNodesCssSelector);
                    avUtils.avFonts.scaleFont(nodesToScale);
                    avUtils.avFonts.correctScaling(nodesToScale);
                }, false);
            } else {
                var observer = new MutationObserver(function(mutations) {
                    var texts = [];
                    for (var i = 0; i < mutations.length; i++) {
                        var mutation = mutations[i];
                        texts = mutation.target.querySelectorAll(childNodesCssSelector);
                    }

                    avUtils.avFonts.scaleFont(texts);
                    avUtils.avFonts.correctScaling(texts);
                    observer.disconnect();
                });

                var config = { attributes: false, childList: true, characterData: false };
                observer.observe(parentNode, config);
            }
        }
    },

    avSlide : {
        shown: false,
        placement: null,
        window: window,

        showPlacement: function(showIt) {
            if (showIt) {
                avUtils.avSlide.placement.style.transform = "translateX(0%)";
            } else {
                avUtils.avSlide.placement.style.transform = "translateX(-110%)";
            }
        },

        showPlacementAfterScroll: function() {
            var halfScreenScrolled = function() {
                return avUtils.avSlide.window.innerHeight / 2 < avUtils.avSlide.window.pageYOffset;
            };

            var togglePlacement = function () {
                avUtils.avSlide.shown = !avUtils.avSlide.shown;
                avUtils.avSlide.showPlacement(avUtils.avSlide.shown);
            };

            var scrolledToTop = avUtils.avSlide.shown && !halfScreenScrolled();
            var scrolledHalfScreenFromTop = !avUtils.avSlide.shown && halfScreenScrolled();

            if (scrolledToTop || scrolledHalfScreenFromTop) {
                togglePlacement();
            }
        },

        detachScrollListener: function () {
            avUtils.avSlide.window.removeEventListener("scroll", avUtils.avSlide.showPlacementAfterScroll);
        },

        addTouchStyles: function() {
            avUtils.avSlide.placement.style.position = "fixed";
            avUtils.avSlide.placement.style.zIndex = "9999999";
            avUtils.avSlide.placement.style.margin = "0";
            avUtils.avSlide.placement.style.bottom = "0";
            avUtils.avSlide.placement.style.left = "0";
            avUtils.avSlide.placement.style.marginLeft = "8px";
            avUtils.avSlide.placement.style.marginBottom = "4px";
            avUtils.avSlide.placement.style.transition = "0.5s ease-in-out";
        },

        init: function() {
            avUtils.avSlide.placement = document.querySelector(".av-container-fixed");
            if (avUtils.avSlide.placement != null) {
                avUtils.avSlide.addTouchStyles();
                avUtils.avSlide.showPlacementAfterScroll();
                avUtils.avSlide.window.addEventListener("scroll", avUtils.avSlide.showPlacementAfterScroll);
            }
        }
    }
};

window.avDebug = window.avDebug || {
    divID: 'avDebugDiv',
    AV_COOKIE_NAME: "AV_REPLACE_URL",
    AV_DEBUG_VISIBLE: "AV_DEBUG_VISIBLE",

    urlReplacement: function(key, value){
        return {
            name: key,
            value: value
        }
    },

    getExistingCookieList: function(){
        var out = [];
        var urlCookieString = this.avReadCookie(this.AV_COOKIE_NAME);
        if(urlCookieString){
            var urlRegexpString = urlCookieString.split("~");
            for(var i=0; i<urlRegexpString.length; i++){
                var urlReplacementSplit = urlRegexpString[i].split("=>");
                if(urlReplacementSplit[0]){
                    out.push(this.urlReplacement(urlReplacementSplit[0], urlReplacementSplit[1]));
                }
            }
        }
        return out;
    },

    getCookieString: function(urlReplacements){
        if(!urlReplacements){
            return "";
        }
        var out = '';
        for(var i=0; i<urlReplacements.length; i++){
            out += urlReplacements[i].name + "=>" + urlReplacements[i].value + "~";
        }
        return out;
    },

    showDebug: function(){
        if(this.avReadCookie(this.AV_DEBUG_VISIBLE)){
            this.avShowDebug();
        }
    },

    avReplaceUrl: function(url){
        var existingCookieList = this.getExistingCookieList();
        for(var i=0; i< existingCookieList.length; i++){
            url = url.replace(existingCookieList[i].name, existingCookieList[i].value);

        }
        return url;
    },

    avAddReplaceUrl:function(url, replacement){
        var cookieList = this.getExistingCookieList();
        cookieList.push(this.urlReplacement(url, replacement));
        this.avClearReplaceUrl();
        this.avCreateCookie(this.AV_COOKIE_NAME, this.getCookieString(cookieList), 1);
        if(this.avReadCookie(this.AV_DEBUG_VISIBLE)){
            this.avHideDebug();
            this.avShowDebug();
        }
    },

    avClearReplaceUrl:function(){
        this.avEraseCookie(this.AV_COOKIE_NAME);
    },

    avCreateCookie:function(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days*24*60*60*1000));
            var expires = "; expires=" + date.toUTCString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    },

    avReadCookie: function(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },

    avEraseCookie: function (name) {
        this.avCreateCookie(name,"",-1);
    },

    avShowDebug: function(){
        var currentState = 'existing url replacements:<br/>';
        var existingCookieList = this.getExistingCookieList();
        for(var i=0; i < existingCookieList.length; i++){
            currentState += i+") " + existingCookieList[i].name + " <b>=></b> " + existingCookieList[i].value + " <a href=\"#\"onclick='avDebug.deleteUrlReplacement(\""+existingCookieList[i].name+"\"); return false;'>del</a> <br/>";
        }

        currentState+= '<br/>';
        var html = '';
        html += '<div id="'+this.divID+'">'
                + currentState
                + '<input type="text" id="avDebugKey"/>'
                + '<input type="text" id="avDebugValue"/>'
                + '<input type="submit" value="Dodaj url replace" onclick="avDebug.avAddReplaceUrl(document.getElementById(\'avDebugKey\').value, document.getElementById(\'avDebugValue\').value); return false;">'
                + ' <a href="#" onclick="avDebug.avHideDebug(); return false;">[X]</a> '
                + '</div>';
        this.avAppendHtml(document.body, html);
        this.avCreateCookie(this.AV_DEBUG_VISIBLE, "1", 1);
    },

    avHideDebug: function(){
        this.avEraseCookie(this.AV_DEBUG_VISIBLE);
        var element = document.getElementById(this.divID);
        element.outerHTML = "";
        delete element;
    },

    avAppendHtml: function(el, str) {
        if(!el)return;
        var div = document.createElement('div');
        div.innerHTML = str;
        while (div.children.length > 0) {
            el.appendChild(div.children[0]);
        }
    },

    deleteUrlReplacement: function(key){
        var existingCookieList = this.getExistingCookieList();
        var urlReplacements = [];
        for(var i=0; i< existingCookieList.length; i++){
            var keyExistsed = existingCookieList[i].name;
            if(keyExistsed !== key){
                urlReplacements.push(this.urlReplacement(keyExistsed, existingCookieList[i].value));
            }
        }
        this.avCreateCookie(this.AV_COOKIE_NAME, this.getCookieString(urlReplacements), 1);
        if(this.avReadCookie(this.AV_DEBUG_VISIBLE)){
            this.avHideDebug();
            this.avShowDebug();
        }
    }
};

avDebug.showDebug();

