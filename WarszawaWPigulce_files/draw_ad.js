(function() {
    var createIframe = function(widthPx, heightPx) {
        var iframe = document.createElement("iframe");
        iframe.setAttribute("style", "width: " + widthPx + "px; height: " + heightPx + "px; margin: 0; padding: 0; border: 0; outline: 0; overflow: hidden");
        return iframe;
    };


    var processPlacements = function() {
        var placementScripts = avConfig.placements;
        for (var i = 0; i < placementScripts.length; i++) {
            var placementScript = placementScripts[i];
            if (!placementScript || !placementScript.initialized) {
                continue; //do not process scripts that does not exists
            }

            var processPlacement = function(responseText, placementScript){
                var placementId = placementScript.node.getAttribute("data-placement-id");
                var placementHash = placementScript.node.getAttribute("data-hash");
                var placementName = placementScript.name;
                var placement = document.getElementById(placementId);
                var iframe = createIframe(
                    placementScript.iframeDimensions.widthPx,
                    placementScript.iframeDimensions.heightPx
                );

                placement.appendChild(iframe);
                iframe.setAttribute("class", "av-iframe");

                var iframeDoc = iframe.contentWindow.document;
                iframeDoc.open();
                iframeDoc.write(responseText);
                iframeDoc.close();

                var preparePlacement = function (placement, iframeDoc, placementHash, adTypes, placementName) {
                    var placementContent = iframeDoc.querySelector("#av-placement-content");

                    var jsonApiConfig = {
                        hash: placementHash,
                        placementContent: placementContent,
                        adTypes: adTypes
                    };
                    avPlacementReadyCallback(placement, jsonApiConfig, placementName);
                };

                if (iframeDoc.readyState === "complete") {
                    preparePlacement(placement, iframeDoc, placementHash, null, placementName);
                } else {
                    var onLoadArgs = {
                        func: preparePlacement,
                        plac: placement,
                        ifrDoc: iframeDoc,
                        hash: placementHash,
                        placementName: placementName
                    };

                    var onLoadIframe = (function (args) {
                        return function () {
                            args.func(args.plac, args.ifrDoc, args.hash, null, args.placementName);
                        }
                    })(onLoadArgs);

                    iframe.addEventListener("load", onLoadIframe);
                }
            };

            var createPlacementCallback = (function(processPlacement, placementScript) {
                return function(responseText) {
                    processPlacement(responseText, placementScript);
                }
            })(processPlacement, placementScript);

            var dimensions = placementScript.templateDimensions ? placementScript.templateDimensions : placementScript.iframeDimensions;
            var size = dimensions.widthPx + "x" + dimensions.heightPx;
            avUtils.loadResource(placementScript.urlScript + "placement_" + size + ".html", createPlacementCallback);

        }
    };

    var avPlacementReadyCallback = function(placement, jsonApiConfig, placementName) {
        var url = 'https://adsearch.adkontekst.pl/_/jsonapi/?prefix=akon&nc=2';
        var successCallack = avJsonApiLoadedFunction(jsonApiConfig, placementName);
        var failureCallack = function(){
            drawPlaceholder(jsonApiConfig.placementContent, placementName);
        };
        avUtils.loadExternalScript(placement, url, successCallack, failureCallack);
        return false;
    };

    var getRandomImg = function(arr) {
        var tmpPosition = Math.floor(Math.random()*arr.length);
        var img = arr[tmpPosition];
        arr.splice(tmpPosition,1);

        return img;
    };

    var drawPlaceholder = function(placementContent, placementName) {
        if (placementContent != null) {
            var placement = avConfig.getPlacementByName(placementName);
            var generateArrayOfImages = function(dimensions) {
                var arrayOfImages = [];
                if (dimensions.widthPx === 750 && dimensions.heightPx === 200) {
                    arrayOfImages = ["750x200_4.jpg", "750x200_9.jpg", "750x200_10.jpg", "750x200_11.jpg"];
                } else {
                    for (var i = 1; i < 13; i++) {
                        arrayOfImages.push(dimensions.widthPx + "x" + dimensions.heightPx + "_" + i + ".jpg");
                    }
                }
                return arrayOfImages;
            };

            var dimensions = placement.templateDimensions ? placement.templateDimensions : placement.iframeDimensions;

            var imgs = generateArrayOfImages(dimensions);
            var toUse = [];
            var rootDir = dimensions.widthPx + "x" + dimensions.heightPx;

            if(toUse.length === 0) {
                for(var i = 0; i < imgs.length; ++i) {
                    toUse.push(imgs[i]);
                }
            }

            var placeholderLabelUrl = "http://www.adkontekst.pl/";
            var image = document.createElement("IMG");
            image.src = "https://vomitter.awangarda.nscontext.eu/get/14ad3fb5e92835eab28b5d754c39bd25/159/_" + rootDir + "/" + getRandomImg(toUse);
            var a = document.createElement("a");
            a.target = "_blank";
            a.href = placeholderLabelUrl;
            a.appendChild(image);

            if (placement.isSlider) {
                avUtils.setLabelUrl(placementContent, placeholderLabelUrl);
                placementContent.innerHTML = a.outerHTML;
            } else {
                placementContent.parentElement.innerHTML = a.outerHTML;
            }
        }
    };

    var drawSingleAd = function(configs, template, placementContent, onError) {
        try {
            var html = '';
            for (var i = 0; i < configs.length; i++) {
                var config = configs[i];
                html += avUtils.applyTemplate(template, config);
            }
            if(config.isDebug){
                html += '<input type="button" value="dbg" style="position: absolute; left: 10px; top: 10px; margin: 3px" '
                        + 'onclick="avUtils.loadExternalScript(document.getElementsByTagName(\'head\')[0], \''+ avConfig.staticUrl +'dbg_box.js\')">';
            }
            placementContent.innerHTML += html;
        } catch (err) {
            onError();
        }
    };

    var drawGadgetAd = function(ad, iframe, placementName, placementContent, onError) {
        if (ad.customJs == null) {
            onError();
        } else {
            var templateDimensions = avConfig.getPlacementByName(placementName).templateDimensions;
            var iframeDimensions = avConfig.getPlacementByName(placementName).iframeDimensions;
            var form = templateDimensions == null ? {
                x : iframeDimensions.widthPx,
                y : iframeDimensions.heightPx
            } : {
                x : templateDimensions.widthPx,
                y : templateDimensions.heightPx
            };

            // fixing emiter object for gadgetAds drawn by customJs
            ad.a = ad.clickUrl;
            ad.desc = ad.description;
            ad.url = ad.destUrl;
            ad.typeName = ad.type;

            var lastCreatedIframe = null;

            var utils = {
                getIfrDoc: function(ifr) {
                    if (!ifr) return null;
                    if (ifr.contentDocument) return ifr.contentDocument;
                    try {
                        if (ifr.contentWindow) return ifr.contentWindow.document;
                    } catch(e) {}
                    return null;
                },

                createIframe: function(parent, x, y, src) {
                    var ifr = createIframe(x, y);
                    parent.appendChild(ifr);
                    if (src) {
                        ifr.src = src;
                    }
                    lastCreatedIframe = ifr;
                    return ifr;
                }
            };

            var fixBodyOfIframe = function(ifr) {
                ifr.contentWindow.document.body.style.margin = "0";
            };

            ad.customJs(placementContent, ad, form, utils);
            try {
                if (lastCreatedIframe != null) {
                    fixBodyOfIframe(lastCreatedIframe);
                }
            } catch (err) {
                avUtils.logToServer('error', placementName, err);
            }
        }
    };

    var drawAds = function(placementContent, placementName, response, onError) {
        try {
            var ad = response.ads[0];
            var iframeOfPlacement = placementContent.ownerDocument.defaultView.frameElement;
            if (ad.type === "gadgetAd") {
                drawGadgetAd(ad, iframeOfPlacement, placementName, placementContent, onError);
            } else {
                var adsTypeConfigsMap = {};

                var labelUrl = response.placement.labelUrl;
                avUtils.setLabelUrl(placementContent, labelUrl);

                var isDebug = typeof(response.debugInfo) != 'undefined';

                for (var i = 0; i < response.ads.length; i++) {
                    var ad = response.ads[i];
                    if(!ad.toShow)continue;

                    var config = {
                        description: ad.description,
                        title: ad.title,
                        image: ad.images[0].url,
                        destinationUrl: ad.destUrl,
                        clickUrl: ad.clickUrl,
                        isDebug: isDebug
                    };

                    if (adsTypeConfigsMap[ad.type] == null) {
                        adsTypeConfigsMap[ad.type] = [config];
                    } else {
                        adsTypeConfigsMap[ad.type].push(config);
                    }
                }

                var adType = Object.keys(adsTypeConfigsMap)[0];
                var configs = adsTypeConfigsMap[adType];

                var placement = avConfig.getPlacementByName(placementName);
                if (placement.addShadowOnDraw) {
                    avUtils.addShadow(iframeOfPlacement, adType);
                }

                var dimensions = placement.templateDimensions ? placement.templateDimensions : placement.iframeDimensions;
                var size = dimensions.widthPx + "x" + dimensions.heightPx;

                var placementDetailsUrl = avUtils.getPlacementDetailsUrl(placementName, adType, size, configs.length);
                avUtils.loadResource(placementDetailsUrl, function(template) {
                    drawSingleAd(configs, template, placementContent, onError)
                });
            }
        } catch (err) {
            avUtils.logToServer('error', placementName, err);
            onError();
        }
    };

    var avJsonApiLoadedFunction = function(jsonApiConfig, placementName) {
        var hash = jsonApiConfig.hash;
        var placementContent = jsonApiConfig.placementContent;

        var words = '';

        var params = {};

        params.content = {};
        params.content.breadcrumb = [];
        params.content.search = [];
        params.content.search = words.split(' ');

        params.typescount = avConfig.getPlacementByName(placementName).typescount;

        params.hash = hash;

        params.callback_onerror = function (response) {
            avConfig.isEmitterProcessing = false;
            avUtils.logToServer('error', placementName, response);
            drawPlaceholder(placementContent, placementName);
        };

        params.callback_onsuccess = function (response) {
            avConfig.isEmitterProcessing = false;
            var onError = function () {
                drawPlaceholder(placementContent, placementName);
            };
            drawAds(placementContent, placementName, response, onError);
        };

        var executeJson = function() {

            if(avConfig.isEmitterProcessing) {
                setTimeout(executeJson, 10);
            }
            else {
                avConfig.isEmitterProcessing = true;
                qa_json_akon.loadAds(params);
            }
        };

        return executeJson;
    };

    processPlacements();
})();