(function(){
    var placement = avConfig.getPlacementByName('14ad3fb5e92835eab28b5d754c39bd25');
    placement.typescount = {
        awatar: 2,
        box: 2,
        exclusive_big: 1,
        exclusive_narrow: 0,
        exclusive_normal: 0,
        exclusive_sky: 0,
        flash: 0,
        flashDB: 0,
        flashDP: 0,
        flashK: 0,
        flashSW: 0,
        gadgetAd: 1,
        imgDB: 0,
        imgDP: 1,
        imgK: 0,
        imgMP: 0,
        imgSP: 0,
        imgSW: 0
    };

    placement.iframeDimensions = {
        widthPx: 300,
        heightPx: 275
    };

    placement.templateDimensions = {
        widthPx: 300,
        heightPx: 250
    };

    placement.isSlider = true;
    placement.addShadowOnDraw = true;

}());