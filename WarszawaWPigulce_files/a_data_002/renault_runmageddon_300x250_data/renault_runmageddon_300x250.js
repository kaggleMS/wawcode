(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.CompoundPath_6 = function() {
	this.initialize(img.CompoundPath_6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,46,58);


(lib.Path = function() {
	this.initialize(img.Path);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,34,34);


(lib.bgg = function() {
	this.initialize(img.bgg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,455,330);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.tx2_2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgKA7QgDgEAAgHQAAgHADgEQADgDAHAAQAIAAACADQAEAEAAAHQAAAHgEAEQgCACgIAAQgHAAgDgCgAgJAWIgChTIAWAAIgBBTg");
	this.shape.setTransform(288.2,117.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AAUAtQAAAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAgBIAAgyQAAgKgEgEQgEgFgKAAQgFAAgGACIgIADIAABCIgYAAIAAhGIAAgCQAAgBAAAAQAAAAAAAAQgBAAAAAAQAAgBgBAAIgGgCIACgLIAbAAQAAAAABAAQAAAAAAABQAAAAAAAAQABAAAAABIAAAFQAGgFAGgCQAHgCAHAAQASAAAJAIQAIAJAAATIAAAkIABADIABABIAHADIgCAKg");
	this.shape_1.setTransform(280,118.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgUAsQgJgDgGgFQgFgGgDgJQgCgJAAgMQAAgLACgJQADgJAFgFQAGgGAJgCQAJgDALAAQAMAAAJADQAIACAGAGQAGAFACAJQADAJAAALQAAAZgLALQgLAKgYAAQgLAAgJgCgAgRgVQgFAHAAAOQAAAPAFAHQAGAGALAAQAMAAAFgGQAGgHAAgPQAAgOgGgHQgFgGgMAAQgLAAgGAGg");
	this.shape_2.setTransform(268.8,118.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgbA+QgHgDgEgFQgFgGgCgJQgCgJAAgMQAAgXAJgLQAIgLAVAAQARAAAJAJIAAgtIAWAAIAABsIABADIACABIAGADIgCAKIgUAAIgCAAIgCgEIgBgFQgFAGgHADQgJACgJAAQgKAAgIgCgAgTgDQgGAGAAAPQAAAPAGAHQAEAGAMAAQAHAAAFgCIAIgEIAAgcQgBgMgEgEQgFgFgKAAQgMAAgEAGg");
	this.shape_3.setTransform(257.9,117);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgaA+QgIgDgFgFQgEgGgDgJQgCgJAAgMQABgXAIgLQAJgLAVAAQARAAAIAJIAAgtIAXAAIAABsIABADIACABIAHADIgCAKIgUAAIgEAAIgBgEIgBgFQgFAGgHADQgIACgKAAQgKAAgHgCgAgUgDQgEAGAAAPQAAAPAEAHQAGAGALAAQAHAAAFgCIAHgEIAAgcQABgMgFgEQgFgFgKAAQgLAAgGAGg");
	this.shape_4.setTransform(246.7,117);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgPAsQgJgDgFgFQgHgGgCgJQgEgJAAgMQAAgLADgJQADgJAFgFQAFgGAJgCQAIgDAKAAQAXAAAJAKQAKALAAAYIAAADIgBAEIg9AAQABAMAHAFQAGAFAOAAQAPAAAOgEIAAARIgOADIgRABQgMAAgKgCgAgNgYQgFAFgBAMIAnAAQAAgMgEgFQgFgFgKAAQgJAAgFAFg");
	this.shape_5.setTransform(235.9,118.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgKBAIgIAAIAAgSIAJABIAHAAQAMABAGgFQAFgFAAgKIAAgJQgIAIgSAAQgJABgIgDQgIgCgEgGQgFgFgCgKQgCgHAAgNQAAgYAJgLQAJgLAUAAQAUAAAKALIABgJIASAAIAABXQAAAMgDAHQgDAIgFAEQgHAFgIACQgIACgKAAIgKgBgAgPgnQgFAGAAAPQAAAPAFAGQAFAGAKAAQAMAAAFgEQAEgFAAgLIAAgcIgHgFQgGgCgIAAQgKAAgFAHg");
	this.shape_6.setTransform(225.1,120.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgkAoQgIgHAAgOQAAgNAJgGQAJgGATAAQALAAAKADIAAgIQAAgJgFgEQgGgEgLAAQgNAAgPAFIAAgSQAPgEAQAAQAVAAAKAIQAKAJAAATIAAAkIAAADIADABIAGADIgCAKIgUAAIgDAAIgCgEIAAgDQgJAJgSAAQgSAAgJgGgAgRALQgEACAAAGQAAAGAEADQADACAJAAQAKAAAEgDQAFgEAAgHIAAgFQgIgDgKAAQgJAAgEADg");
	this.shape_7.setTransform(214.8,118.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AAwAtQAAAAAAAAQgBAAAAAAQAAAAAAgBQAAAAAAgBIAAgzQAAgKgEgEQgDgEgJAAQgGAAgGACIgJAEIABAEIAAAIIAAA1IgWAAIAAg1QAAgKgDgEQgDgEgKAAQgGAAgFACIgIADIAABCIgXAAIAAhGIAAgCQAAgBAAAAQgBAAAAAAQAAAAAAAAQgBgBAAAAIgGgCIACgLIAbAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAAAFQAGgFAHgCQAGgCAIAAQAJAAAHADQAGADADAFQAGgFAHgDQAIgDAJAAQATAAAIAJQAIAIAAARIAAAmIAAADIACABIAGADIgCAKg");
	this.shape_8.setTransform(201.2,118.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AAVAtQgBAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAgBIAAgyQAAgKgEgEQgEgFgJAAQgHAAgFACIgJADIAABCIgWAAIAAhGIgBgCQAAgBAAAAQAAAAgBAAQAAAAAAAAQgBgBAAAAIgGgCIACgLIAbAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAAFQAHgFAHgCQAGgCAHAAQASAAAIAIQAJAJAAATIAAAkIAAADIACABIAHADIgCAKg");
	this.shape_9.setTransform(187.2,118.7);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgOArQgIgCgFgEQgFgFgCgHQgDgHAAgJIAAgkIAAgDIgCgBIgHgCIACgLIAdAAQAAAAAAAAQABAAAAAAQAAABAAAAQAAAAAAABIAAAxQAAALAFAEQAEAFAJAAQALAAAEgFQAFgEAAgLIAAgzIAXAAIAAA1QAAATgKAJQgKAIgXAAQgKAAgIgCg");
	this.shape_10.setTransform(175.6,118.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AAdA9IgbguIgXAAIAAAuIgXAAIAAhoIgBgCIgCgBIgHgDIACgLIA8AAQAXABAKAIQALAKAAATQAAAQgHAHQgGAIgOAEIAeAwgAgVgCIAaAAQANAAAFgFQAFgEAAgLQAAgKgFgFQgFgFgNAAIgaAAg");
	this.shape_11.setTransform(164.2,117.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AAdA9IgbguIgXAAIAAAuIgXAAIAAhoIgBgCIgCgBIgHgDIACgLIA8AAQAXABAKAIQALAKAAATQAAAQgHAHQgGAIgOAEIAeAwgAgVgCIAaAAQANAAAFgFQAFgEAAgLQAAgKgFgFQgFgFgNAAIgaAAg");
	this.shape_12.setTransform(147.1,117.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AAjA9IgEgPIgEgOIg1AAIgEAOIgEAPIgYAAIAFgTIAIgWIAIgWIAIgWIAJgUIAIgQIAZAAIAIAQIAJAUIAIAWIAJAWIAHAWIAFATgAgUAPIApAAQgKgbgLgaQgLAagJAbg");
	this.shape_13.setTransform(134.6,117.2);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgVA9IgLgBIAAgTQAKACAJAAQAMAAAEgGQAFgEABgNIAAhRIAXAAIAABSQABAVgLAKQgKAJgVAAIgMAAg");
	this.shape_14.setTransform(123.6,117.3);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgxA9IAAhoIgBgCIgCgBIgHgDIACgLIA2AAQAQAAAMAEQAMAEAIAHQAIAHADALQAEAMAAAPQAAAQgEALQgDAMgIAHQgIAIgMADQgMADgQABgAgaAqIAVAAQAKAAAIgCQAIgDAFgFQAFgFACgIQACgIAAgLQAAgKgCgIQgCgIgFgFQgFgFgIgCQgIgDgKAAIgVAAg");
	this.shape_15.setTransform(112.7,117.2);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAjA9IgEgPIgEgOIg1AAIgEAOIgEAPIgYAAIAFgTIAIgWIAIgWIAIgWIAJgUIAIgQIAZAAIAIAQIAJAUIAIAWIAJAWIAHAWIAFATgAgUAPIApAAQgKgbgLgaQgLAagJAbg");
	this.shape_16.setTransform(99.6,117.2);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AAcA9IgmgzIgLAKIAAApIgXAAIAAhoIAAgCIgDgBIgGgDIABgLIAeAAQAAAAABABQAAAAAAAAQAAAAAAABQAAAAAAABIAAAxIAvg0IAbAAIgyA4IA0BBg");
	this.shape_17.setTransform(87.6,117.2);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgJAyQgJgHABgRIAAglIAAgDIgDgBIgLgDIACgNIAMAAIAAgaIAVAAIAAAaIAcAAIAAARIgcAAIAAAnQAAAIAEAEQADADAIAAQAHAAAGgCIAAASQgIACgIAAQgSAAgHgIg");
	this.shape_18.setTransform(72.8,117.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgJA/QAAAAgBAAQAAAAAAAAQgBAAAAgBQAAAAAAgBIAAhqIAAgDIgCgBIgGgDIACgKIAbAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAABrIABADIACABIAGADIgCAKg");
	this.shape_19.setTransform(66.6,116.9);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgOArQgIgCgFgEQgFgFgCgHQgDgHAAgJIAAgkIAAgDIgCgBIgHgCIACgLIAdAAQAAAAAAAAQABAAAAAAQAAABAAAAQAAAAAAABIAAAxQAAALAFAEQAEAFAJAAQALAAAEgFQAFgEAAgLIAAgzIAXAAIAAA1QAAATgKAJQgKAIgXAAQgKAAgIgCg");
	this.shape_20.setTransform(58.2,118.9);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgkAoQgIgHAAgOQAAgNAJgGQAJgGATAAQALAAAKADIAAgIQAAgJgFgEQgGgEgLAAQgNAAgPAFIAAgSQAPgEAQAAQAVAAAKAIQAKAJAAATIAAAkIAAADIADABIAGADIgCAKIgUAAIgDAAIgCgEIAAgDQgJAJgSAAQgSAAgJgGgAgRALQgEACAAAGQAAAGAEADQADACAJAAQAKAAAEgDQAFgEAAgHIAAgFQgIgDgKAAQgJAAgEADg");
	this.shape_21.setTransform(48.1,118.8);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AAUAtQAAAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAgBIAAgyQAAgKgEgEQgEgFgKAAQgFAAgGACIgIADIAABCIgYAAIAAhGIAAgCQAAgBAAAAQAAAAgBAAQAAAAAAAAQgBgBAAAAIgGgCIACgLIAbAAQAAAAABAAQAAAAAAABQAAAAAAAAQABAAAAABIAAAFQAFgFAIgCQAGgCAHAAQARAAAKAIQAIAJAAATIAAAkIABADIACABIAGADIgCAKg");
	this.shape_22.setTransform(37.2,118.7);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgPAsQgJgDgGgFQgGgGgDgJQgDgJAAgMQABgLACgJQACgJAGgFQAFgGAJgCQAIgDAKAAQAXAAAJAKQAKALAAAYIAAADIgBAEIg9AAQABAMAGAFQAIAFANAAQAOAAAPgEIAAARIgPADIgQABQgMAAgKgCgAgNgYQgFAFgBAMIAnAAQAAgMgEgFQgFgFgKAAQgJAAgFAFg");
	this.shape_23.setTransform(26.4,118.8);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAdA9IgbguIgXAAIAAAuIgXAAIAAhoIgBgCIgCgBIgHgDIACgLIA8AAQAXABAKAIQALAKAAATQAAAQgHAHQgGAIgOAEIAeAwgAgVgCIAaAAQANAAAFgFQAFgEAAgLQAAgKgFgFQgFgFgNAAIgaAAg");
	this.shape_24.setTransform(15.1,117.2);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AggApIAAgJIAvg+IguAAIAAgKIA/AAIAAAJIgvA+IAwAAIAAAKg");
	this.shape_25.setTransform(247.7,96.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgUAqIAAhGIAAgBIgBgBIgGgCIACgHIARAAQAAAAABAAQAAAAAAAAQAAAAAAABQABAAAAAAIAAAJQAIgMASAAIAEAAIADAAIgCAMIgDgBIgFAAQgIAAgGADQgFACgEAEIAAA/g");
	this.shape_26.setTransform(240.2,96.2);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgaAhQgLgKAAgXQABgKACgJQACgIAFgFQAEgGAIgCQAHgCAKAAQASAAAJAKQAJAKgBAWIAAABIAAADIg8AAQAAAPAIAHQAHAHAQAAQANAAANgFIAAALIgMADIgPABQgXAAgKgKgAgRgaQgFAHgBAPIAwAAQgBgPgFgHQgFgHgMAAQgMAAgHAHg");
	this.shape_27.setTransform(231.9,96.3);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgDA6IAAhGIgBgBIgBgBIgFgCIACgHIAQAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAABQgAgEgqQgCgCAAgEQAAgFACgCQACgCAEAAQAFAAACACQACACAAAFQAAAIgJAAQgEAAgCgCg");
	this.shape_28.setTransform(224.8,94.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgLA6QgHgDgFgGIgBAKIgKAAIAAhrIAAgCIgBgBIgGgCIACgHIARAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAAsQAFgGAGgCQAHgDAJAAQASAAAKAKQAHALAAAVQAAAXgHALQgKAKgSAAQgKAAgHgDgAgPgJQgGAGAAAMIAAAhIALAGQAGACAGAAQAOAAAGgIQAGgHAAgSQAAgRgGgGQgGgIgOAAQgMAAgFAFg");
	this.shape_29.setTransform(217.7,94.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgdA8IgCgBIAAgKIACAAIACAAIAHgBIAFgCIAFgGIADgIIAEgJIgkhSIAPAAIAaBBIAYhBIAOAAIgjBdIgFANIgGAIQgEACgFABQgFACgGAAIgDAAg");
	this.shape_30.setTransform(208.1,98.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AAUApIgGgOIgGgSIgCgHIgCgIIgCgIIgCgGIAAAAIgBAGIgCAIIgCAIIgCAHIgGASIgGAOIgPAAIgHgSIgIgVIgHgVIgHgVIAOAAIADAJIACAJIADAKIADAIIAGARIAFAQIABAAIAFgQIAGgRIACgIIADgJIADgJIACgIIALAAIACAIIADAJIADAJIACAIIAGARIAGAQIAFgQIAGgRIADgIIADgKIADgJIACgJIAOAAIgHAVIgHAVIgIAVIgHASg");
	this.shape_31.setTransform(196.7,96.3);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgDA6IAAhGIgBgBIgBgBIgFgCIACgHIAQAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAABQgAgEgqQgCgCAAgEQAAgFACgCQACgCAEAAQAFAAACACQACACAAAFQAAAIgJAAQgEAAgCgCg");
	this.shape_32.setTransform(183,94.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AALA6QgFgDABgHQAAgLANgOIgGABIgHAAQgWAAgLgKQgKgKAAgWQgBgLADgJQACgHAFgGQAFgFAHgDQAHgCAKAAQASAAAJAKQAIAKAAAWIAAADIAAACIg8AAQAAAPAIAGQAHAHAQAAQANAAAOgEIAAALQgKAJgEAFQgEAFAAAFQABAEACACQACACAGABIAEgBIADgBIAAAJIgFAAIgFABQgJAAgFgEgAgRgsQgFAGgBAPIAwAAQgBgPgFgGQgFgIgMABQgMgBgHAIg");
	this.shape_33.setTransform(172,98.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AAYAqQAAAAAAAAQgBAAAAAAQAAAAAAgBQAAAAAAAAIAAgxQAAgMgFgFQgFgFgLAAIgHAAIgHACIgGACIgEACIAABCIgNAAIAAhGIAAgBIgCgBIgFgCIACgHIARAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAAAGQAFgEAHgDQAGgCAIAAQARAAAIAHQAHAIAAARIAAAoIAAABIACABIAFACIgCAHg");
	this.shape_34.setTransform(162.2,96.2);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgeAlQgIgFABgNQAAgNAHgFQAIgFASAAQANAAAKADIAAgLQAAgLgGgEQgFgFgNAAQgNAAgMAEIAAgLIANgCIAOgBQASAAAIAHQAJAHAAARIAAApIAAABIACABIAEACIgBAHIgOAAIgBAAIgBgBIAAgHQgIAKgTAAQgRAAgHgGgAgTAHQgFAEAAAIQAAAHAFAEQAFADAKAAQAMAAAFgEQAGgEAAgKIAAgHQgJgEgMAAQgMAAgFADg");
	this.shape_35.setTransform(152.4,96.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgDA6IAAhGIgBgBIgBgBIgFgCIACgHIAQAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAABQgAgEgqQgCgCAAgEQAAgFACgCQACgCAEAAQAFAAACACQACACAAAFQAAAIgJAAQgEAAgCgCg");
	this.shape_36.setTransform(145.2,94.6);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AAyAqQAAAAgBAAQAAAAAAAAQAAAAAAgBQgBAAAAAAIAAgzQABgLgFgEQgEgFgLAAQgHAAgGACQgHABgDADIABAGIAAAHIAAA1IgNAAIAAg0QABgLgFgEQgEgFgLAAIgHAAIgGACIgGACIgDACIAABCIgOAAIAAhGIAAgBIgCgBIgFgCIACgHIARAAQAAAAABAAQAAAAAAAAQAAAAAAABQABAAAAAAIAAAFQAEgEAGgCQAGgCAJAAQAKAAAGACQAGADACAGQAGgFAIgDQAGgDAKAAQAQAAAHAHQAHAHAAAQIAAAqIABABIAAABIAGACIgBAHg");
	this.shape_37.setTransform(135.5,96.2);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AggApIAAgJIAvg+IguAAIAAgKIA/AAIAAAJIgvA+IAwAAIAAAKg");
	this.shape_38.setTransform(123.5,96.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgeAlQgHgFgBgNQAAgNAIgFQAJgFARAAQAMAAALADIAAgLQAAgLgFgEQgHgFgLAAQgOAAgMAEIAAgLIANgCIAOgBQASAAAIAHQAJAHAAARIAAApIABABIAAABIAGACIgCAHIgOAAIgBAAIAAgBIgBgHQgHAKgUAAQgQAAgIgGgAgTAHQgFAEAAAIQAAAHAFAEQAFADALAAQAKAAAHgEQAFgEAAgKIAAgHQgKgEgLAAQgMAAgFADg");
	this.shape_39.setTransform(110.2,96.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AAYAqQAAAAAAAAQgBAAAAAAQAAAAAAgBQAAAAAAAAIAAgxQAAgMgFgFQgFgFgLAAIgHAAIgHACIgGACIgEACIAABCIgNAAIAAhGIAAgBIgCgBIgFgCIACgHIARAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAAAGQAFgEAHgDQAGgCAIAAQARAAAIAHQAHAIAAARIAAAoIAAABIACABIAFACIgCAHg");
	this.shape_40.setTransform(100.2,96.2);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AALA6QgEgDAAgHQAAgLANgOIgGABIgHAAQgXAAgKgKQgLgKAAgWQABgLACgJQACgHAFgGQAEgFAIgDQAHgCAKAAQASAAAJAKQAJAKgBAWIAAADIAAACIg8AAQAAAPAIAGQAHAHAQAAQANAAANgEIAAALQgJAJgDAFQgFAFAAAFQABAEACACQACACAGABIAEgBIACgBIAAAJIgEAAIgFABQgJAAgFgEgAgRgsQgFAGgBAPIAwAAQgBgPgFgGQgFgIgMABQgMgBgHAIg");
	this.shape_41.setTransform(86.1,98.2);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgDA6IAAhGIgBgBIgBgBIgFgCIACgHIAQAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAABQgAgEgqQgCgCAAgEQAAgFACgCQACgCAEAAQAFAAACACQACACAAAFQAAAIgJAAQgEAAgCgCg");
	this.shape_42.setTransform(79,94.6);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgdAnIAAgLQANAFAPAAQAJAAAFgEQAFgEAAgGQAAgGgDgDQgEgDgFgBIgMgDQgGgBgFgCQgGgCgEgEQgEgEAAgJQAAgMAJgGQAJgFAOAAIAOABIANACIgCALQgMgEgMAAQgJAAgGADQgEAEAAAGQAAAFADADQAEACAFACIAMACIALAEQAGACADAEQAEAFAAAIQABANgJAGQgJAGgOAAQgRAAgMgEg");
	this.shape_43.setTransform(72.9,96.3);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AggA6IAAgJIAvg+IguAAIAAgLIA/AAIAAAKIgvA9IAwAAIAAALgAgGgrQgCgCAAgDQAAgFACgCQACgCAEAAQAFAAACACQACACAAAFQAAADgCACQgCACgFABQgEgBgCgCg");
	this.shape_44.setTransform(60,94.7);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AgeAlQgIgFAAgNQABgNAHgFQAIgFASAAQAMAAALADIAAgLQAAgLgGgEQgFgFgNAAQgMAAgNAEIAAgLIANgCIAOgBQASAAAIAHQAJAHAAARIAAApIAAABIABABIAGACIgCAHIgOAAIgBAAIgBgBIAAgHQgHAKgUAAQgRAAgHgGgAgTAHQgFAEAAAIQAAAHAFAEQAFADAKAAQAMAAAFgEQAGgEAAgKIAAgHQgJgEgMAAQgMAAgFADg");
	this.shape_45.setTransform(51,96.3);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AAUApIgGgOIgGgSIgCgHIgCgIIgCgIIgCgGIAAAAIgBAGIgCAIIgCAIIgCAHIgGASIgGAOIgPAAIgHgSIgIgVIgHgVIgHgVIAOAAIADAJIACAJIADAKIADAIIAGARIAFAQIABAAIAFgQIAGgRIACgIIADgJIADgJIACgIIALAAIACAIIADAJIADAJIACAIIAGARIAGAQIAFgQIAGgRIADgIIADgKIADgJIACgJIAOAAIgHAVIgHAVIgIAVIgHASg");
	this.shape_46.setTransform(39.1,96.3);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AggAzQgJgLAAgXQAAgVAJgLQAIgKATAAQAJAAAHADQAHACAEAGIAAguIANAAIAABrIABACIABABIAFACIgCAHIgOAAIgBAAIAAgCIgBgIQgFAGgHADQgHADgKAAQgTAAgIgKgAgVgGQgGAGAAARQAAASAGAHQAGAIAOAAQAHAAAGgCIAKgGIAAghQAAgMgGgGQgFgFgMAAQgOAAgGAIg");
	this.shape_47.setTransform(26.9,94.6);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgYA4QgKgDgHgIQgGgHgDgLQgDgLAAgQQAAgPADgLQADgLAGgHQAHgHAKgEQALgDANAAQAOAAALADQAKAEAGAHQAHAHADALQADALAAAPQAAAQgDALQgDALgHAHQgGAIgKADQgLADgOAAQgNAAgLgDgAgRgsQgIADgFAGQgFAFgCAJQgCAJAAAMQAAANACAJQACAJAFAGQAFAFAIADQAHADAKAAQALAAAHgDQAIgDAEgFQAFgGADgJQACgJAAgNQAAgMgCgJQgDgJgFgFQgEgGgIgDQgHgDgLAAQgKAAgHADg");
	this.shape_48.setTransform(15.3,94.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.tx2_2, new cjs.Rectangle(7,83,286.2,46.8), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 1 — kopia
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AAAAJIgNATIgHgFIAOgTIgWgHIACgIIAXAIIAAgYIAHAAIAAAYIAXgIIACAIIgWAHIAOATIgHAFg");
	this.shape.setTransform(163.8,8.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgJBMQgDgDAAgHQAAgGADgDQAEgDAFAAQAGAAADADQAEADAAAGQAAAHgEADQgDACgGAAQgFAAgEgCgAgIAdIgBhqIASAAIgBBqg");
	this.shape_1.setTransform(157.4,15.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgLBNQgBAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAgBIAAg6IgTAJIAAgWIATgIIAAgzIAAgEIgEgBIgHgDIADgOIAhAAQAAAAABABQAAAAAAAAQABAAAAABQAAAAAAABIAAA4IATgJIAAAVIgTAIIAAA2IABAEIACACIAIADIgDAMg");
	this.shape_2.setTransform(150.5,15.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AguA2IAAgTIA3hDIg1AAIAAgVIBYAAIAAATIg2BDIA4AAIAAAVg");
	this.shape_3.setTransform(141,17.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgZBJQgKgFgIgKQgGgIgDgPQgDgOAAgVQAAgTADgOQADgPAGgKQAIgIAKgFQALgEAOgBQAPABALAEQAKAFAHAIQAHAKADAPQAEAOgBATQABAVgEAOQgDAPgHAIQgHAKgKAFQgLADgPAAQgOAAgLgDgAgWgoQgHANAAAbQAAAcAHANQAHANAPAAQAQAAAGgNQAIgNgBgcQABgbgIgNQgGgMgQAAQgPAAgHAMg");
	this.shape_4.setTransform(123.3,15.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgZBJQgKgFgHgKQgIgIgCgPQgDgOAAgVQAAgTADgOQACgPAIgKQAHgIAKgFQALgEAOgBQAPABALAEQAKAFAHAIQAHAKADAPQADAOAAATQAAAVgDAOQgDAPgHAIQgHAKgKAFQgLADgPAAQgOAAgLgDgAgWgoQgHANAAAbQAAAcAHANQAHANAPAAQAPAAAHgNQAIgNgBgcQABgbgIgNQgHgMgPAAQgPAAgHAMg");
	this.shape_5.setTransform(110.1,15.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AguBIIAAgWIAQAEIAQABQALgBAHgCQAIgCAFgGQAFgGADgIQACgJABgLQgJANgXAAQgbAAgMgMQgLgLAAgYQAAgcAMgLQANgMAdgBQAOAAALAFQAKADAHAJQAHAIADANQADANAAATQAAAVgDAQQgEAQgJAKQgIAKgNAFQgNAFgRgBQgSABgQgFgAgVgvQgHAGAAAQQAAAOAHAGQAGAGAOAAQAOAAAHgFQAHgHAAgMQAAgegbAAQgOgBgHAHg");
	this.shape_6.setTransform(96.8,15.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgxBGIAAgXQARAGATAAQASAAAIgFQAJgGAAgOQAAgNgJgGQgJgFgTAAIgPAAIgPABIAAhPIBYAAIAAAVIg+AAIAAAmIAPgBQAcAAANALQANAKAAAYQAAAYgOALQgPALggAAQgVAAgRgFg");
	this.shape_7.setTransform(78.4,15.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AgqBCQgNgJAAgWQAAgQAHgIQAHgJAPgCQgNgEgGgJQgHgHAAgPQAAgWAMgJQANgKAbAAQAbAAANAKQANAJAAAWQgBAPgGAHQgHAJgNAEQAQACAHAJQAHAIAAAQQAAAWgNAJQgNALgegBQgcABgOgLgAgVARQgGAGAAAKQAAAMAGAFQAGAGAPAAQAPAAAGgGQAHgFAAgMQAAgKgGgGQgGgGgQgCQgPACgGAGgAgSgyQgHAGAAAKQABAKAFAGQAGAEANACQAOgCAFgEQAHgGAAgKQAAgKgHgGQgFgEgOAAQgNAAgFAEg");
	this.shape_8.setTransform(65.2,15.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AglAuQgKgHAAgPQAAgQAKgHQAKgHAWABQAPgBANAFIAAgOQAAgNgHgGQgHgGgPABQgQAAgPAEIAAgNIAQgEIARgBQAXABAKAIQALAJAAAWIAAAxIAAADIABABIAHACIgCAJIgSAAIgBAAIAAgBIgBgJQgJANgZgBQgUABgJgIgAgYAJQgGAFAAAKQAAAJAGAEQAGAEAOABQAOgBAHgFQAGgGAAgLIAAgJQgLgEgPAAQgOAAgHADg");
	this.shape_9.setTransform(48.2,17.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgoAzIAAgLIA7hNIg5AAIAAgNIBOAAIAAAMIg7BMIA8AAIAAANg");
	this.shape_10.setTransform(37.5,17.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgoBIIAAgMIA7hMIg5AAIAAgNIBOAAIAAALIg7BNIA8AAIAAANgAgHg1QgCgCAAgFQAAgGACgCQADgDAEAAQAGAAADADQACACAAAGQAAAFgCACQgDADgGAAQgEAAgDgDg");
	this.shape_11.setTransform(22.7,15.7);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgdAqQgLgJAAgWIAAgwIgBgCIgHgDIACgJIAVAAQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAABIAAA7QAAAPAHAHQAGAHAOAAQAPAAAHgHQAGgHAAgPIAAg9IARAAIAAA+QAAAWgLAJQgKAKgYAAQgXAAgKgKg");
	this.shape_12.setTransform(11.4,17.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgQBfIgFgBIAAgNIAEABIADAAQAKAAAEgGQADgFAAgKIAAhjIAAgBIgCgBIgFgDIACgJIAUAAQAAAAABAAQAAAAAAABQAAAAABAAQAAABAAAAIAABvQAAAPgIAKQgHAJgQAAIgFAAgAAChLQgCgCAAgGQAAgGACgCQADgDAGAAQAFAAADADQADACAAAGQAAALgLAAQgGAAgDgDg");
	this.shape_13.setTransform(2.4,18);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AAUAtQAAAAAAAAQAAAAgBAAQAAgBAAAAQAAAAAAgBIAAgyQAAgKgEgEQgEgFgKAAQgFAAgGACIgJADIAABCIgXAAIAAhGIAAgCQAAgBAAAAQAAAAAAAAQgBAAAAAAQAAgBgBAAIgGgCIACgLIAbAAQAAAAABAAQAAAAAAABQAAAAAAAAQABAAAAABIAAAFQAFgFAHgCQAHgCAHAAQASAAAJAIQAIAJAAATIAAAkIABADIABABIAHADIgCAKg");
	this.shape_14.setTransform(272,-7.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgUAsQgJgDgGgFQgFgGgDgJQgCgJAAgMQAAgLACgJQADgJAFgFQAGgGAJgCQAJgDALAAQAMAAAJADQAIACAGAGQAGAFACAJQADAJAAALQAAAZgLALQgLAKgYAAQgLAAgJgCgAgRgVQgFAHAAAOQAAAPAFAHQAGAGALAAQAMAAAFgGQAGgHAAgPQAAgOgGgHQgFgGgMAAQgLAAgGAGg");
	this.shape_15.setTransform(260.8,-7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgbA+QgHgDgEgFQgFgGgDgJQgBgJAAgMQgBgXAKgLQAIgLAVAAQARAAAJAJIAAgtIAXAAIAABsIAAADIACABIAHADIgDAKIgUAAIgCAAIgCgEIgBgFQgFAGgIADQgHACgKAAQgKAAgIgCgAgTgDQgFAGgBAPQABAPAFAHQAEAGAMAAQAHAAAFgCIAIgEIAAgcQgBgMgEgEQgFgFgKAAQgMAAgEAGg");
	this.shape_16.setTransform(249.9,-8.8);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgaA+QgIgDgFgFQgEgGgCgJQgDgJAAgMQABgXAIgLQAKgLAUAAQARAAAJAJIAAgtIAWAAIAABsIABADIACABIAGADIgBAKIgUAAIgEAAIgBgEIgBgFQgFAGgHADQgJACgJAAQgKAAgHgCgAgUgDQgEAGAAAPQAAAPAEAHQAGAGALAAQAHAAAFgCIAIgEIAAgcQAAgMgFgEQgFgFgKAAQgLAAgGAGg");
	this.shape_17.setTransform(238.7,-8.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgPAsQgJgDgFgFQgHgGgCgJQgDgJgBgMQAAgLADgJQACgJAGgFQAGgGAHgCQAJgDAKAAQAXAAAJAKQAKALAAAYIAAADIgBAEIg9AAQABAMAHAFQAGAFAOAAQAOAAAPgEIAAARIgOADIgRABQgMAAgKgCgAgNgYQgFAFgBAMIAnAAQAAgMgEgFQgEgFgLAAQgJAAgFAFg");
	this.shape_18.setTransform(227.9,-7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgKBAIgIAAIAAgSIAIABIAIABQAMAAAGgFQAFgFABgKIAAgJQgKAJgQgBQgKAAgIgCQgHgDgFgFQgFgGgCgIQgCgJAAgMQAAgYAJgLQAJgLAVAAQATAAAKALIABgJIASAAIAABXQAAALgDAIQgDAHgFAGQgHAEgIACQgIACgKAAIgKgBgAgPgnQgFAGAAAPQAAAQAFAFQAFAHAKgBQAMAAAEgEQAFgFAAgLIAAgcIgHgFQgGgCgIAAQgKAAgFAHg");
	this.shape_19.setTransform(217.1,-5.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgkAoQgIgHAAgOQAAgNAJgGQAJgGATAAQALAAAKADIAAgIQAAgJgFgEQgGgEgLAAQgNAAgPAFIAAgSQAPgEAQAAQAVAAAKAIQAKAJAAATIAAAkIAAADIADABIAGADIgCAKIgUAAIgDAAIgCgEIAAgDQgJAJgSAAQgSAAgJgGgAgRALQgEACAAAGQAAAGAEADQADACAJAAQAKAAAEgDQAFgEAAgHIAAgFQgIgDgKAAQgJAAgEADg");
	this.shape_20.setTransform(206.8,-7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AAwAtQAAAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBIAAgzQAAgKgEgEQgDgEgJAAQgGAAgGACIgJAEIABAEIAAAIIAAA1IgWAAIAAg1QAAgKgDgEQgDgEgKAAQgGAAgFACIgIADIAABCIgXAAIAAhGIAAgCQAAgBAAAAQgBAAAAAAQAAAAAAAAQgBgBAAAAIgGgCIACgLIAbAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAAAFQAGgFAHgCQAGgCAIAAQAJAAAHADQAGADADAFQAGgFAHgDQAIgDAJAAQATAAAIAJQAIAIAAARIAAAmIAAADIACABIAGADIgCAKg");
	this.shape_21.setTransform(193.2,-7.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AAVAtQgBAAAAAAQAAAAgBAAQAAgBAAAAQAAAAAAgBIAAgyQAAgKgEgEQgEgFgJAAQgHAAgFACIgJADIAABCIgWAAIAAhGIgBgCQAAgBAAAAQAAAAgBAAQAAAAAAAAQgBgBAAAAIgGgCIACgLIAbAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAAFQAGgFAIgCQAGgCAHAAQASAAAIAIQAJAJAAATIAAAkIAAADIADABIAGADIgCAKg");
	this.shape_22.setTransform(179.2,-7.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgOArQgIgCgFgEQgFgFgCgHQgDgGAAgKIAAgkIAAgDIgCgBIgHgDIACgKIAdAAQAAAAAAAAQABAAAAAAQAAABAAAAQAAAAAAAAIAAAyQAAALAFAEQAEAFAJAAQALAAAEgFQAFgEAAgLIAAgzIAXAAIAAA1QAAATgKAJQgKAIgXAAQgKAAgIgCg");
	this.shape_23.setTransform(167.6,-6.9);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AAdA8IgbgtIgXAAIAAAtIgXAAIAAhmIgBgDIgCgCIgHgCIACgKIA8AAQAXAAAKAJQALAIAAAUQAAAQgHAIQgGAIgOADIAeAvgAgVgCIAaAAQANAAAFgEQAFgFAAgLQAAgKgFgFQgFgFgNABIgaAAg");
	this.shape_24.setTransform(156.2,-8.6);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AAdA8IgbgtIgXAAIAAAtIgXAAIAAhmIgBgDIgCgCIgHgCIACgKIA8AAQAXAAAKAJQALAIAAAUQAAAQgHAIQgGAIgOADIAeAvgAgVgCIAaAAQANAAAFgEQAFgFAAgLQAAgKgFgFQgFgFgNABIgaAAg");
	this.shape_25.setTransform(139.1,-8.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAjA8IgEgOIgEgOIg1AAIgEAOIgEAOIgYAAIAFgTIAIgVIAIgWIAIgWIAJgUIAIgPIAZAAIAIAPIAJAUIAIAWIAJAWIAHAVIAFATgAgUAOIApAAQgKgbgLgZQgLAZgJAbg");
	this.shape_26.setTransform(126.6,-8.6);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgVA9IgLgCIAAgTQAJADAKAAQAMAAAEgFQAFgGABgMIAAhQIAYAAIAABRQAAAVgLAKQgKAJgVAAIgMAAg");
	this.shape_27.setTransform(115.6,-8.5);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgxA8IAAhmIgBgDIgCgCIgHgCIACgKIA2AAQAQgBAMAEQAMADAIAIQAIAHADALQAEAMAAAPQAAAQgEALQgDAMgIAIQgIAGgMAEQgMADgQAAgAgaAqIAVAAQAKAAAIgCQAIgCAFgGQAFgFACgIQACgIAAgLQAAgKgCgIQgCgIgFgFQgFgFgIgDQgIgCgKAAIgVAAg");
	this.shape_28.setTransform(104.7,-8.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AAjA8IgEgOIgEgOIg1AAIgEAOIgEAOIgYAAIAFgTIAIgVIAIgWIAIgWIAJgUIAIgPIAZAAIAIAPIAJAUIAIAWIAJAWIAHAVIAFATgAgUAOIApAAQgKgbgLgZQgLAZgJAbg");
	this.shape_29.setTransform(91.6,-8.6);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AAbA8IglgyIgKALIAAAnIgYAAIAAhmIAAgDIgDgCIgGgCIABgKIAeAAQAAAAABAAQAAAAAAAAQAAAAAAABQABAAAAABIAAAxIAugzIAbAAIgyA3IAzBAg");
	this.shape_30.setTransform(79.6,-8.6);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgJAyQgJgHABgRIAAglIAAgDIgDgBIgLgDIACgNIAMAAIAAgaIAVAAIAAAaIAcAAIAAARIgcAAIAAAnQAAAIAEAEQAEADAHAAQAHAAAGgCIAAASQgIACgIAAQgSAAgHgIg");
	this.shape_31.setTransform(64.8,-8.2);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgJA/QAAAAgBAAQAAAAAAAAQgBgBAAAAQAAAAAAgBIAAhqIAAgDIgCgBIgGgDIACgKIAbAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAABrIABADIACABIAGADIgCAKg");
	this.shape_32.setTransform(58.6,-8.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgOArQgIgCgFgEQgFgFgCgHQgDgGAAgKIAAgkIAAgDIgCgBIgHgDIACgKIAdAAQAAAAAAAAQABAAAAAAQAAABAAAAQAAAAAAAAIAAAyQAAALAFAEQAEAFAJAAQALAAAEgFQAFgEAAgLIAAgzIAXAAIAAA1QAAATgKAJQgKAIgXAAQgKAAgIgCg");
	this.shape_33.setTransform(50.2,-6.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgkAoQgIgHAAgOQAAgNAJgGQAJgGATAAQALAAAKADIAAgIQAAgJgFgEQgGgEgLAAQgNAAgPAFIAAgSQAPgEAQAAQAVAAAKAIQAKAJAAATIAAAkIAAADIADABIAGADIgCAKIgUAAIgDAAIgCgEIAAgDQgJAJgSAAQgSAAgJgGgAgRALQgEACAAAGQAAAGAEADQADACAJAAQAKAAAEgDQAFgEAAgHIAAgFQgIgDgKAAQgJAAgEADg");
	this.shape_34.setTransform(40.1,-7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AAUAtQAAAAAAAAQAAAAgBAAQAAgBAAAAQAAAAAAgBIAAgyQAAgKgEgEQgEgFgKAAQgFAAgGACIgIADIAABCIgYAAIAAhGIAAgCQAAgBAAAAQAAAAAAAAQgBAAAAAAQgBgBAAAAIgGgCIACgLIAbAAQAAAAABAAQAAAAAAABQAAAAAAAAQABAAAAABIAAAFQAGgFAGgCQAHgCAHAAQASAAAJAIQAIAJAAATIAAAkIABADIACABIAGADIgCAKg");
	this.shape_35.setTransform(29.2,-7.1);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgPAsQgJgDgFgFQgHgGgCgJQgEgJAAgMQABgLACgJQACgJAGgFQAFgGAJgCQAIgDAKAAQAWAAAKAKQAKALAAAYIAAADIgBAEIg9AAQABAMAHAFQAGAFAOAAQAPAAAOgEIAAARIgOADIgRABQgMAAgKgCgAgNgYQgFAFgBAMIAnAAQAAgMgEgFQgFgFgKAAQgJAAgFAFg");
	this.shape_36.setTransform(18.4,-7);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AAdA8IgbgtIgXAAIAAAtIgXAAIAAhmIgBgDIgCgCIgHgCIACgKIA8AAQAXAAAKAJQALAIAAAUQAAAQgHAIQgGAIgOADIAeAvgAgVgCIAaAAQANAAAFgEQAFgFAAgLQAAgKgFgFQgFgFgNABIgaAAg");
	this.shape_37.setTransform(7.1,-8.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(-1,-21,281,51.9), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 1 — kopia
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgaBkIAAgVIADABIADAAQALAAADgFQAEgFAAgKIAAhcIgBgEIgCgBIgIgDIACgOIAiAAQAAAAABABQAAAAAAAAQAAAAABABQAAAAAAABIAABvQAAALgCAHQgCAIgFAFQgFAFgIACQgHADgLAAIgLgBgAgBhIQgEgDAAgJQAAgIAEgFQADgDAJAAQAJAAAEADQADAFAAAIQAAAJgDADQgEAEgJAAQgJAAgDgEg");
	this.shape.setTransform(173.8,150.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgSA2QgLgEgIgGQgHgIgDgKQgEgLAAgPQAAgOADgKQADgLAHgHQAGgHALgDQAKgEANAAQAbAAALAOQAMAMAAAeIAAADIgBAFIhKAAQABAPAIAGQAIAHARAAQASAAASgGIAAAVIgSAFIgUAAQgPAAgMgCgAgQgeQgGAHgBAOIAwAAQgBgOgFgHQgFgFgMAAQgMAAgGAFg");
	this.shape_1.setTransform(165.9,150.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AAaA3QgBAAAAAAQgBAAAAAAQAAgBAAAAQAAAAAAgBIAAg+QgBgMgFgFQgEgGgMAAQgIAAgGACIgLAEIAABRIgcAAIAAhWIAAgDIgDgBIgIgEIADgNIAhAAQABAAAAAAQABABAAAAQAAAAAAABQAAAAAAABIAAAGQAHgGAJgDQAIgCAJAAQAVAAALAKQALAKAAAYIAAAsIAAAEIACABIAJADIgDANg");
	this.shape_2.setTransform(153.5,150.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgsAwQgKgHAAgSQAAgQALgHQALgHAXAAQANgBANAEIAAgKQAAgLgHgFQgGgEgOAAQgRAAgRAFIAAgWQASgEATgBQAaAAANALQAMAKAAAXIAAAtIAAAEIADABIAIADIgDANIgYAAIgEgBIgCgEIgBgEQgLALgWAAQgWABgKgJgAgVANQgFADAAAHQAAAHAEADQAFAEALAAQALAAAGgFQAGgDAAgKIAAgGQgKgEgMABQgLgBgFAEg");
	this.shape_3.setTransform(141.1,150.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AAWA2IgLggIgKgiIgBAAIgJAiIgMAgIgdAAIgLgcIgKgbIgIgbIgHgZIAbAAIAFATIAFATIAHAUIAHAWIABAAIAGgUIAGgWIAGgRIAEgSIAZAAIAFASIAEARIAIAWIAGAUIABAAIAGgWIAGgUIAGgTIAFgTIAbAAIgGAZIgIAbIgKAbIgLAcg");
	this.shape_4.setTransform(125.7,150.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AgZA2QgKgEgHgGQgHgIgDgKQgDgLAAgPQAAgOADgKQADgLAHgHQAHgHAKgDQALgEAOAAQAPAAAKAEQALADAHAHQAHAHADALQADAKAAAOQAAAegNAOQgNANgegBQgOAAgLgCgAgUgZQgHAHAAASQAAATAHAIQAGAHAOABQAPgBAGgHQAHgIAAgTQAAgSgHgHQgGgJgPABQgOgBgGAJg");
	this.shape_5.setTransform(110.1,150.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgMA+QgKgJAAgVIAAgtIAAgEIgCgCIgOgDIADgRIANAAIAAgfIAbAAIAAAfIAiAAIAAAWIgiAAIAAAwQAAAKAFAEQAEAEAJAAQAJAAAHgCIAAAWQgJACgLAAQgVAAgKgJg");
	this.shape_6.setTransform(99.1,149.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AgJBNIAAhWIAAgDIgDgBIgIgEIADgNIAhAAQABAAAAAAQABABAAAAQAAAAAAABQAAAAAAABIAABogAgIgwQgEgEAAgIQAAgJAEgEQAEgDAIAAQAJAAAEADQAEAEAAAJQAAAIgEAEQgEAEgJAAQgIAAgEgEg");
	this.shape_7.setTransform(92,148.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AA7A3QAAAAgBAAQAAAAAAAAQgBgBAAAAQAAAAAAgBIAAg/QAAgMgEgFQgFgFgLAAQgHAAgGACIgMAFIABAFIABAKIAABBIgbAAIAAhBQAAgMgEgFQgEgFgMAAQgHAAgHACIgJAEIAABRIgdAAIAAhWIAAgDIgDgBIgHgEIADgNIAhAAQABAAAAAAQAAABABAAQAAAAAAABQAAAAAAABIAAAGQAHgGAHgDQAIgCAKAAQAMAAAIADQAHAEAFAGQAHgGAIgEQAJgDAMAAQAXAAAJAKQAKALAAAVIAAAuIAAAEIADABIAIADIgDANg");
	this.shape_8.setTransform(79.6,150.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgJBNIAAhWIAAgDIgDgBIgIgEIADgNIAhAAQABAAAAAAQABABAAAAQAAAAAAABQAAAAAAABIAABogAgIgwQgEgEAAgIQAAgJAEgEQAEgDAIAAQAJAAAEADQAEAEAAAJQAAAIgEAEQgEAEgJAAQgIAAgEgEg");
	this.shape_9.setTransform(66.5,148.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgLBNQAAAAgBAAQAAAAAAAAQgBAAAAgBQAAAAAAgBIAAiDIgBgDIgCgBIgIgEIACgNIAiAAQABAAAAABQAAAAABAAQAAAAAAABQAAAAAAABIAACDIAAADIADABIAIAEIgDAMg");
	this.shape_10.setTransform(61.1,148.2);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AgJBNIAAhWIAAgDIgDgBIgIgEIADgNIAhAAQABAAAAAAQAAABABAAQAAAAAAABQAAAAAAABIAABogAgIgwQgEgEAAgIQAAgJAEgEQAEgDAIAAQAJAAAEADQAEAEAAAJQAAAIgEAEQgEAEgJAAQgIAAgEgEg");
	this.shape_11.setTransform(50.1,148.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgJBNIAAhWIAAgDIgDgBIgIgEIADgNIAhAAQABAAAAAAQABABAAAAQAAAAAAABQAAAAAAABIAABogAgIgwQgEgEAAgIQAAgJAEgEQAEgDAIAAQAJAAAEADQAEAEAAAJQAAAIgEAEQgEAEgJAAQgIAAgEgEg");
	this.shape_12.setTransform(44.4,148.2);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgeA3IAAhWIAAgDIgDgBIgHgEIADgNIAhAAQABAAAAAAQAAABABAAQAAAAAAABQAAAAAAABIAAAMQAEgJAIgEQAIgEALAAIAHAAIAFABIgFAWIgFAAIgFgBQgIAAgHADQgHADgFAFIAABMg");
	this.shape_13.setTransform(37.7,150.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgSA2QgLgEgIgGQgHgIgDgKQgEgLAAgPQAAgOADgKQADgLAHgHQAGgHALgDQAKgEANAAQAbAAALAOQAMAMAAAeIAAADIgBAFIhKAAQABAPAIAGQAIAHARAAQASAAASgGIAAAVIgSAFIgUAAQgPAAgMgCgAgQgeQgGAHgBAOIAwAAQgBgOgFgHQgFgFgMAAQgMAAgGAFg");
	this.shape_14.setTransform(27.2,150.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgpAzIAAgWQAJADAJACIATACQAVAAAAgNQAAgFgFgDQgEgCgHgCIgOgCQgIgBgHgEQgGgEgFgFQgEgFAAgMQAAgJADgHQAEgGAGgEQAGgDAJgCQAIgCAJgBQAKAAAKACQALABAHADIgEAVIgQgEQgIgCgIABQgKAAgFACQgFAEAAAFQAAAFAFADIALADIAOADQAIABAHAEQAGAEAFAEQAEAHAAALQAAASgLAIQgLAJgXgBQgWABgSgGg");
	this.shape_15.setTransform(16.3,150.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AgaAhQgKgKAAgXQgBgKADgJQACgIAFgFQAFgGAHgCQAHgCAKAAQASAAAJAKQAIAKAAAWIAAABIAAADIg8AAQAAAPAIAHQAHAHAQAAQANAAAOgFIAAALIgOADIgOABQgWAAgLgKgAgRgaQgFAHgBAPIAwAAQgBgPgFgHQgFgHgMAAQgMAAgHAHg");
	this.shape_16.setTransform(213.8,125.4);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgDA6IAAhGIgBgBIgBgBIgFgCIACgHIAQAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAABQgAgEgqQgCgCAAgEQAAgFACgCQACgCAEAAQAFAAACACQACACAAAFQAAAIgJAAQgEAAgCgCg");
	this.shape_17.setTransform(207.2,123.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AAYAqQAAAAAAAAQgBAAAAAAQAAAAAAgBQAAAAAAAAIAAgxQAAgMgFgFQgFgFgLAAIgHAAIgHACIgGACIgEACIAABCIgNAAIAAhGIAAgBIgCgBIgFgCIACgHIARAAQAAAAAAAAQABAAAAAAQAAAAAAABQAAAAAAAAIAAAGQAFgEAHgDQAGgCAIAAQARAAAIAHQAHAIAAARIAAAoIAAABIACABIAFACIgCAHg");
	this.shape_18.setTransform(200.6,125.3);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgaAhQgKgKAAgXQgBgKADgJQACgIAFgFQAFgGAHgCQAHgCAKAAQASAAAJAKQAIAKAAAWIAAABIAAADIg8AAQAAAPAIAHQAHAHAQAAQANAAAOgFIAAALIgOADIgOABQgWAAgLgKgAgRgaQgFAHgBAPIAwAAQgBgPgFgHQgFgHgMAAQgMAAgHAHg");
	this.shape_19.setTransform(191.3,125.4);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AggA6IAAgKIAvg9IguAAIAAgLIA/AAIAAAKIgvA9IAwAAIAAALgAgGgrQgCgBAAgFQAAgEACgCQACgCAEAAQAFAAACACQACACAAAEQAAAFgCABQgCACgFAAQgEAAgCgCg");
	this.shape_20.setTransform(182.8,123.7);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgeAlQgIgFAAgNQABgNAHgFQAIgFASAAQAMAAALADIAAgLQAAgLgGgEQgFgFgNAAQgMAAgNAEIAAgLIANgCIAOgBQASAAAIAHQAJAHAAARIAAApIAAABIABABIAGACIgCAHIgOAAIgBAAIgBgBIAAgHQgHAKgUAAQgRAAgHgGgAgTAHQgFAEAAAIQAAAHAFAEQAFADAKAAQAMAAAFgEQAGgEAAgKIAAgHQgJgEgMAAQgMAAgFADg");
	this.shape_21.setTransform(174.3,125.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgdAnIAAgLQANAFAPAAQAJAAAFgEQAGgEgBgGQAAgGgDgDQgEgDgFgBIgMgDQgGgBgGgCQgFgCgEgEQgEgEABgJQgBgMAJgGQAJgFAOAAIAOABIANACIgCALQgLgEgNAAQgKAAgEADQgGAEAAAGQABAFADADQAEACAGACIALACIALAEQAGACADAEQAFAFAAAIQAAANgJAGQgJAGgPAAQgQAAgMgEg");
	this.shape_22.setTransform(165.8,125.4);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgeAhQgKgKAAgXQAAgWAKgKQAKgKAUAAQAWAAAJAKQAKAKAAAWQAAAXgKAKQgJAKgWAAQgUAAgKgKgAgTgYQgHAIAAAQQAAARAHAIQAGAHANAAQAOAAAHgHQAGgIAAgRQAAgQgGgIQgHgIgOAAQgNAAgGAIg");
	this.shape_23.setTransform(157,125.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgiA8IAAhpIAAgCIgCgBIgFgCIACgHIARAAQAAAAAAAAQABAAAAAAQAAABAAAAQAAAAAAABIAAAHQAEgGAHgCQAGgDAKAAQATAAAIAKQAJAKAAAXQAAAWgJAKQgIALgTgBQgJABgHgDQgGgDgFgEIAAArgAgPgrQgGAFAAANIAAAhIAKAGQAGACAIAAQANgBAGgHQAGgIAAgQQAAgRgGgHQgGgIgNAAQgMgBgGAGg");
	this.shape_24.setTransform(147.3,127.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgdA8IgCgBIAAgKIACAAIACAAIAHAAIAFgEIAFgEIADgJIAEgJIgkhSIAPAAIAaBBIAYhBIAOAAIgjBdIgFANIgGAHQgEAEgFABQgFABgGAAIgDAAg");
	this.shape_25.setTransform(138.2,127.3);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AAUApIgGgOIgGgSIgCgHIgCgIIgCgIIgCgGIAAAAIgBAGIgCAIIgCAIIgCAHIgGASIgGAOIgPAAIgHgSIgIgVIgHgVIgHgVIAOAAIADAJIACAJIADAKIADAIIAGARIAFAQIABAAIAFgQIAGgRIACgIIADgJIADgJIACgIIALAAIACAIIADAJIADAJIACAIIAGARIAGAQIAFgQIAGgRIADgIIADgKIADgJIACgJIAOAAIgHAVIgHAVIgIAVIgHASg");
	this.shape_26.setTransform(127.3,125.4);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgaAhQgLgKABgXQAAgKACgJQACgIAFgFQAEgGAIgCQAIgCAJAAQATAAAIAKQAJAKAAAWIAAABIAAADIg9AAQABAPAHAHQAIAHAPAAQANAAAOgFIAAALIgOADIgOABQgXAAgKgKgAgQgaQgHAHAAAPIAvAAQAAgPgFgHQgFgHgMAAQgNAAgFAHg");
	this.shape_27.setTransform(112.3,125.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgIAwQgGgGAAgPIAAgsIgBgBIgBgBIgKgCIABgIIALAAIAAgZIANAAIAAAZIAcAAIAAALIgcAAIAAAtQAAAJADAEQAEADAIABIAHgBIAGgBIAAALQgHACgIgBQgNABgHgHg");
	this.shape_28.setTransform(104.5,124.3);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgeAlQgIgFABgNQAAgNAIgFQAIgFARAAQANAAAKADIAAgLQAAgLgGgEQgGgFgMAAQgNAAgMAEIAAgLIANgCIAOgBQASAAAJAHQAIAHAAARIAAApIABABIABABIAEACIgBAHIgOAAIgBAAIgBgBIAAgHQgIAKgTAAQgRAAgHgGgAgTAHQgFAEAAAIQAAAHAFAEQAFADAKAAQALAAAGgEQAGgEAAgKIAAgHQgKgEgLAAQgMAAgFADg");
	this.shape_29.setTransform(97.2,125.4);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgHA8IgHAAIAAgLIAJABIAHAAQAXAAAAgUIAAgPQgEAGgGACQgIADgJgBQgSABgJgLQgIgKgBgWQABgXAIgKQAJgKASAAQAJAAAHACQAIACAFAFIABgHIALAAIAABVQAAALgDAGQgDAHgFADQgFAEgHACQgGABgHAAIgKgBgAgSgpQgFAHAAARQAAARAFAHQAHAHAMABQANgBAFgFQAGgGAAgLIAAgjIgKgEQgIgCgGAAQgMAAgHAIg");
	this.shape_30.setTransform(87.6,127.2);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgeAhQgKgKAAgXQAAgWAKgKQAKgKAUAAQAWAAAJAKQAKAKAAAWQAAAXgKAKQgJAKgWAAQgUAAgKgKgAgTgYQgHAIAAAQQAAARAHAIQAGAHANAAQAOAAAHgHQAGgIAAgRQAAgQgGgIQgHgIgOAAQgNAAgGAIg");
	this.shape_31.setTransform(78.2,125.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgLA6QgHgDgEgGIgCAKIgKAAIAAhrIgBgCIgBgBIgEgCIABgHIARAAQAAAAABAAQAAAAAAABQAAAAAAAAQABAAAAABIAAAsQAEgGAGgCQAHgDAJAAQATAAAJAKQAHALABAVQgBAXgHALQgJAKgTAAQgKAAgHgDgAgPgJQgGAGABAMIAAAhIAKAGQAFACAHAAQAOAAAGgIQAGgHAAgSQAAgRgGgGQgGgIgOAAQgMAAgFAFg");
	this.shape_32.setTransform(68.6,123.6);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgNBNIgDgBIAAgKIACAAIACAAQAJAAADgEQADgFAAgIIAAhPIgBgCIgBgBIgEgCIABgHIAQAAQABAAAAAAQAAAAABABQAAAAAAAAQAAAAAAABIAABZQAAANgGAHQgGAIgNAAIgEAAgAACg9QgCgCAAgEQAAgFACgCQACgCAFAAQAEAAACACQACACABAFQAAAIgJAAQgFAAgCgCg");
	this.shape_33.setTransform(57.3,125.5);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgdA8IgCgBIAAgKIACAAIACAAIAHAAIAFgEIAFgEIADgJIAEgJIgkhSIAPAAIAaBBIAYhBIAOAAIgjBdIgFANIgGAHQgEAEgFABQgFABgGAAIgDAAg");
	this.shape_34.setTransform(51.8,127.3);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AgUAqIAAhGIAAgBIgBgBIgGgCIACgHIARAAQAAAAABAAQAAAAAAAAQAAAAAAABQAAAAAAAAIAAAJQAJgMARAAIAFAAIADAAIgCAMIgDgBIgFAAQgIAAgGADQgFACgFAEIAAA/g");
	this.shape_35.setTransform(44.8,125.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AAVA8IgbgmIgJAJIAAAdIgOAAIAAhrIAAgCIgBgBIgFgCIABgHIARAAQABAAAAAAQAAAAAAABQABAAAAAAQAAAAAAABIAABIIAjglIAQAAIgjAkIAjAug");
	this.shape_36.setTransform(37.5,123.5);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AggAzQgJgLAAgXQAAgVAJgLQAIgKATAAQAJAAAHADQAHACAEAGIAAguIANAAIAABrIABACIABABIAFACIgCAHIgOAAIgBAAIAAgCIgBgIQgFAGgHADQgHADgKAAQgTAAgIgKgAgVgGQgGAGAAARQAAASAGAHQAGAIAOAAQAHAAAGgCIAKgGIAAghQAAgMgGgGQgFgFgMAAQgOAAgGAIg");
	this.shape_37.setTransform(28.5,123.6);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AgYA4QgKgDgHgIQgGgHgDgLQgDgLAAgQQAAgPADgLQADgLAGgHQAHgHAKgEQALgDANAAQAOAAALADQAKAEAGAHQAHAHADALQADALAAAPQAAAQgDALQgDALgHAHQgGAIgKADQgLADgOAAQgNAAgLgDgAgRgsQgIADgFAGQgFAFgCAJQgCAJAAAMQAAANACAJQACAJAFAGQAFAFAIADQAHADAKAAQALAAAHgDQAIgDAEgFQAFgGADgJQACgJAAgNQAAgMgCgJQgDgJgFgFQgEgGgIgDQgHgDgLAAQgKAAgHADg");
	this.shape_38.setTransform(17.3,123.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(9.1,112.1,211.4,79.3), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgEAFQgCgBAAgEQAAgDACgBQACgCACAAQAEAAABACQACABAAADQAAAEgCABQgBACgEAAQgCAAgCgCg");
	this.shape.setTransform(77.9,212.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_1.setTransform(73.4,209.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIAAgBIgBgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIAAABIABABIAEABIgBAGg");
	this.shape_2.setTransform(69.2,208.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#000000").s().p("AgSAaQgGgGAAgNIAAgeIgBgBIgEgCIABgFIANAAIABABIAAAkQAAAKAEAEQAEAEAIAAQAKAAAEgEQAEgEAAgKIAAglIAKAAIAAAmQAAANgGAGQgHAGgPAAQgNAAgHgGg");
	this.shape_3.setTransform(63.5,210.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#000000").s().p("AgWAcQgHgEAAgJQAAgKAHgFQAGgDANAAQAKAAAHACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgJADIAAgIIAJgCIALgBQAOAAAGAFQAGAGAAANIAAAfIABABIAAABIAFABIgBAGIgMAAIAAAAIgBgBIAAgGQgFAIgPAAQgMAAgGgFgAgOAGQgEACAAAHQAAAFAEADQAEACAHAAQAJAAAFgDQADgDAAgHIAAgGQgHgDgIAAQgJAAgEADg");
	this.shape_4.setTransform(56.3,210.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_5.setTransform(48.7,210.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#000000").s().p("AgUAZQgIgIAAgRQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_6.setTransform(41.2,210.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#000000").s().p("AAYAsIgWgkIgWAAIAAAkIgKAAIAAhOIgBgCIgEgCIABgFIAnAAQAQAAAGAGQAHAGAAAOQAAAMgFAFQgFAGgLABIAXAlgAgUAAIAYAAQALAAAEgDQAEgEAAgKQAAgJgEgEQgEgEgLAAIgYAAg");
	this.shape_7.setTransform(33.2,209.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#000000").s().p("AASAuQAAAAAAAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIAAgmQAAgIgDgEQgEgDgIAAIgFAAIgGABIgEACIgEABIAAAzIgKAAIAAhSIAAgBIgBgBIgDgBIABgGIAMAAQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAAgQAEgDAFgCQAFgCAHAAQANAAAFAGQAGAGgBAMIAAAfIABABIABABIAEABIgBAGg");
	this.shape_8.setTransform(265.9,193.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#000000").s().p("AgRAZQgHgHAAgSQAAgRAHgIQAHgHAQAAIAMABIAHACIgCAIIgGgCIgKgBQgKAAgFAGQgGAFAAANQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAJQgIACgKAAQgQAAgHgIg");
	this.shape_9.setTransform(258.9,195.1);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#000000").s().p("AgWAcQgHgEAAgJQAAgKAHgFQAGgDANAAQAKAAAHACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgJADIAAgIIAJgCIALgBQAOAAAGAFQAGAGAAANIAAAfIABABIAAABIAFABIgBAGIgMAAIAAAAIgBgBIAAgGQgFAIgPAAQgMAAgGgFgAgOAGQgEACAAAHQAAAFAEADQAEADAHgBQAJAAAFgDQADgDAAgHIAAgGQgHgDgIAAQgJAAgEADg");
	this.shape_10.setTransform(252,195.1);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_11.setTransform(244.4,195);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#000000").s().p("AgXAZQgHgHAAgSQAAgRAHgIQAIgHAPAAQARAAAHAHQAHAIAAARQAAASgHAHQgHAIgRAAQgPAAgIgIgAgPgSQgEAFAAANQAAANAEAGQAGAGAJAAQALAAAFgGQAEgGAAgNQAAgNgEgFQgFgGgLAAQgJAAgGAGg");
	this.shape_12.setTransform(236.7,195.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIgBgBIAAgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIAAABIABABIAEABIgBAGg");
	this.shape_13.setTransform(231.1,193.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#000000").s().p("AgWAcQgHgEAAgJQAAgKAHgFQAGgDANAAQAJAAAIACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgJADIAAgIIAKgCIAKgBQANAAAHAFQAGAGAAANIAAAfIABABIAAABIAFABIgBAGIgMAAIAAAAIgBgBIAAgGQgGAIgOAAQgNAAgFgFgAgOAGQgEACAAAHQAAAFAEADQADADAIgBQAJAAAFgDQADgDAAgHIAAgGQgHgDgIAAQgJAAgEADg");
	this.shape_14.setTransform(225.8,195.1);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#000000").s().p("AgXAeIAAgIQALADALAAQAIAAADgDQAEgDAAgFQAAgEgCgDIgIgCIgIgCIgJgDQgFgBgCgDQgDgEAAgGQAAgKAHgEQAGgEALAAIALABIAJACIgBAIQgJgDgJAAQgIAAgDADQgEACAAAFQAAADADACQACACAFACIAIACIAJACQAFABACAEQADADAAAHQAAAKgGAEQgHAFgLAAQgMAAgLgDg");
	this.shape_15.setTransform(218.9,195.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#000000").s().p("AAPAgIgEgLIgFgOIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFAOIgEALIgMAAIgFgOIgGgRIgGgPIgFgQIALAAIACAGIACAHIACAIIACAHIAFAMIAEAMIAAAAIAEgMIAFgMIACgHIACgHIACgGIABgHIAJAAIABAHIACAGIADAHIABAHIAFAMIAEAMIAFgMIAEgMIACgHIACgIIADgHIABgGIALAAIgFAQIgGAPIgGARIgFAOg");
	this.shape_16.setTransform(196.6,195.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IAMAAIATAyIASgyIALAAIgbBHIgDAJIgFAHIgGADIgIABIgDAAg");
	this.shape_17.setTransform(174,196.5);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAgmIgNAGIAAgIIANgFIAAgjIAAgBIgBgBIgEgBIABgGIAMAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAAkIANgGIAAAIIgNAGIAAAkIABABIAAABIAEABIgBAGg");
	this.shape_18.setTransform(168.4,193.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgXAnQgHgIAAgRQAAgRAHgIQAIgHAPgBQARABAHAHQAHAIAAARQAAARgHAIQgHAIgRgBQgPABgIgIgAgPgEQgEAEAAAOQAAAMAEAHQAGAFAJABQALgBAFgFQAEgHAAgMQAAgOgEgEQgFgHgLAAQgJAAgGAHgAgGgcIAPgSIALAAIgRASg");
	this.shape_19.setTransform(162.4,193.8);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgFAuIgGgBIAAgIIAHABIAFAAQASAAAAgPIAAgLQgDADgFACQgFACgHAAQgPABgGgIQgHgIAAgQQAAgSAHgIQAGgHAPgBQAFABAHABQAFACAEADIABgFIAIAAIAABBQAAAIgCAFQgCAEgEAEQgEACgFABIgKABIgHAAgAgOgfQgEAFAAAOQAAAMAEAGQAFAFAKABQAKAAADgFQAFgEAAgJIAAgaIgIgEIgKgCQgKAAgFAHg");
	this.shape_20.setTransform(154.6,196.5);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_21.setTransform(147.3,195.1);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgZAgIAAgHIAkgvIgjAAIAAgIIAxAAIAAAHIgkAvIAlAAIAAAIg");
	this.shape_22.setTransform(140.4,195.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgRAZQgHgHAAgSQAAgRAHgIQAHgHAQAAIAMABIAHACIgCAIIgGgCIgKgBQgKAAgFAGQgGAFAAANQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAJQgIACgKAAQgQAAgHgIg");
	this.shape_23.setTransform(133.9,195.1);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgZAgIAAgHIAkgvIgjAAIAAgIIAxAAIAAAHIgkAvIAlAAIAAAIg");
	this.shape_24.setTransform(127.3,195.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AgQAsIgNgDIAAgJIANAEIAPABQALAAAGgEQAFgEAAgIQAAgGgEgDQgEgEgGgCIgLgDIgNgDQgFgDgFgEQgDgFAAgIQAAgLAIgHQAHgGAQAAIAOABIAMADIgBAIIgMgDIgNgBQgKAAgFAEQgGADAAAJQAAAEACADQADADAEABIAIADIAIADIAKADQAEAAAEADQAEADACAEQACAEAAAGQAAANgHAGQgJAGgQAAIgPgBg");
	this.shape_25.setTransform(120,193.9);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgEAFQgCgBAAgEQAAgDACgBQABgCADAAQAEAAACACQABABAAADQAAAEgBABQgCACgEAAQgDAAgBgCg");
	this.shape_26.setTransform(100.5,197.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgRAsIAjhOIguAAIAAgJIA5AAIAAAIIgjBPg");
	this.shape_27.setTransform(94.8,193.9);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AAHAsIAAhNIgXAKIAAgIIAYgMIAJAAIAABXg");
	this.shape_28.setTransform(86.5,193.9);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgOArQgGgDgEgFQgDgGgDgIQgBgJAAgMQAAgLABgIQADgJADgGQAEgFAGgDQAHgCAHAAQAJAAAFACQAGADAFAFQAEAGABAJQACAIAAALQAAAMgCAJQgBAIgEAGQgFAFgGADQgFACgJAAQgHAAgHgCgAgJghQgEACgDAEQgDAEgBAHIgBAQIABARQABAHADAEQADAEAEADQAEACAFAAQAGAAAEgCQAEgDADgEQACgEABgHQACgHAAgKQAAgJgCgHQgBgHgCgEQgDgEgEgCQgEgCgGAAQgFAAgEACg");
	this.shape_29.setTransform(79.1,193.9);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgcAtIAAgIIAigiQAGgFADgEQACgGAAgFQAAgJgEgFQgFgDgKAAQgLAAgMADIAAgJIAMgDIANAAQAOgBAHAHQAHAGAAANQAAAGgDAHQgEAGgGAGIgdAdIArAAIAAAJg");
	this.shape_30.setTransform(71,193.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgEAFQgCgBAAgEQAAgDACgBQACgCACAAQAEAAACACQABABAAADQAAAEgBABQgCACgEAAQgCAAgCgCg");
	this.shape_31.setTransform(65.5,197.7);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#000000").s().p("AgaArIAAgJIAJACIAKABQAHAAAFgCQAGgCAEgFQACgEADgHQABgHAAgJQgCAGgGACQgFADgKAAQgPAAgGgGQgHgGAAgPQAAgPAHgHQAIgHAPAAQAJAAAFACQAGADAEAEQAEAFACAIQABAIAAALQAAAMgCAJQgCAKgEAGQgFAGgIACQgHADgJAAQgKAAgKgCgAgPgfQgFAFAAALQAAAKAFAFQAEAEAKAAQALAAAFgEQAFgEAAgJQAAgXgUAAQgLAAgEAFg");
	this.shape_32.setTransform(59.8,193.9);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AgOArQgFgDgFgFQgEgGgCgIQgBgJAAgMQAAgLABgIQACgJAEgGQAFgFAFgDQAHgCAHAAQAJAAAFACQAHADADAFQAEAGACAJQACAIAAALQAAAMgCAJQgCAIgEAGQgDAFgHADQgFACgJAAQgHAAgHgCgAgJghQgEACgDAEQgDAEgBAHIgBAQIABARQABAHADAEQADAEAEADQAEACAFAAQAGAAAEgCQAEgDADgEQACgEABgHQACgHAAgKQAAgJgCgHQgBgHgCgEQgDgEgEgCQgEgCgGAAQgFAAgEACg");
	this.shape_33.setTransform(52,193.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AgFAFQgBgBAAgEQAAgDABgBQACgCADAAQAEAAACACQABABAAADQAAAEgBABQgCACgEAAQgDAAgCgCg");
	this.shape_34.setTransform(46.3,197.7);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AAHAsIAAhNIgXAKIAAgIIAYgMIAJAAIAABXg");
	this.shape_35.setTransform(40.2,193.9);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AgcAtIAAgIIAigiQAGgFADgEQACgGAAgFQAAgJgEgFQgFgDgKAAQgLAAgMADIAAgJIAMgDIANAAQAOgBAHAHQAHAGAAANQAAAGgDAHQgEAGgGAGIgdAdIArAAIAAAJg");
	this.shape_36.setTransform(32.6,193.8);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("AgXAdQgFgFgBgKQABgJAFgFQAHgDANAAQAKAAAHACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgKADIAAgJIAKgBIALgBQAOAAAGAFQAHAGgBANIAAAfIABABIABAAIAEACIgBAGIgLAAIgBAAIAAgBIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGAEADQAEADAHgBQAJABAFgEQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_37.setTransform(266.2,179.9);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_38.setTransform(258.6,179.8);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IALAAIAVAyIASgyIALAAIgbBHIgEAJIgEAHIgHADIgJABIgCAAg");
	this.shape_39.setTransform(241.7,181.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_40.setTransform(235.5,179);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAHQAGgJANAAIADAAIADAAIgCAJIgCAAIgDgBQgIAAgEACIgGAFIAAAwg");
	this.shape_41.setTransform(230.7,179.8);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgEQAEgFAGgBQAFgCAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_42.setTransform(224.3,179.9);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#000000").s().p("AgMAvIAAg1IAAgBIgBgBIgHgCIABgFIAHAAIAAgJQAAgLAFgGQAGgEAKAAIAIAAIAEABIgCAIIgEgBIgFAAQgGAAgEADQgBACAAAIIAAAJIASAAIAAAIIgSAAIAAA2g");
	this.shape_43.setTransform(218.8,178.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#000000").s().p("AgXAZQgHgHAAgSQAAgQAHgIQAIgIAPAAQARAAAHAIQAHAIAAAQQAAASgHAHQgHAIgRAAQgPAAgIgIgAgPgSQgEAFAAANQAAANAEAGQAGAGAJAAQALAAAFgGQAEgGAAgNQAAgNgEgFQgFgGgLAAQgJAAgGAGg");
	this.shape_44.setTransform(212.4,179.9);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_45.setTransform(195.1,179.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#000000").s().p("AgXAdQgFgFAAgKQAAgJAFgFQAHgDANAAQAKAAAIACIAAgIQgBgIgEgDQgEgEgJAAQgKAAgKADIAAgJIAKgBIALgBQAOAAAGAFQAHAGAAANIAAAfIAAABIABAAIADACIgBAGIgKAAIgBAAIAAgBIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGADADQAFADAIgBQAIABAEgEQAEgDABgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_46.setTransform(187.6,179.9);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_47.setTransform(181.1,179);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#000000").s().p("AgPAsIgPgDIAAgJIAPAEIAOABQALAAAFgEQAGgEAAgIQAAgGgEgDQgEgEgGgCIgLgDIgMgDQgHgDgDgEQgEgFAAgIQAAgLAHgHQAIgGAQAAIAOABIANADIgCAIIgMgDIgMgBQgMAAgEAEQgGADAAAJQAAAEADADQACADADABIAIADIAJADIAJADQAFAAAEADQAEADACAEQACAEAAAGQAAANgIAGQgHAGgRAAIgOgBg");
	this.shape_48.setTransform(174.7,178.7);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#000000").s().p("AgEAFQgCgBAAgEQAAgDACgBQABgCADAAQAEAAACACQABABAAADQAAAEgBABQgCACgEAAQgDAAgBgCg");
	this.shape_49.setTransform(159.5,182.5);

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#000000").s().p("AAQAuIgVgdIgHAHIAAAWIgKAAIAAhSIAAgBIgBgBIgEgBIABgGIANAAQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAA3IAbgcIAMAAIgaAbIAbAjg");
	this.shape_50.setTransform(154.5,178.5);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#000000").s().p("AgRAaQgHgGAAgNIAAgeIgBgBIgEgCIABgFIANAAIABABIAAAkQAAAKAEAEQAEAEAIAAQAKAAAEgEQAEgEAAgKIAAglIAKAAIAAAmQAAANgGAGQgHAGgPAAQgNAAgGgGg");
	this.shape_51.setTransform(147.1,180);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_52.setTransform(140.7,179);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#000000").s().p("AgZAgIAAgIIAkguIgjAAIAAgIIAxAAIAAAHIgkAuIAlAAIAAAJg");
	this.shape_53.setTransform(135,179.9);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#000000").s().p("AgWAeIAAgIQAKADALAAQAIAAADgDQAEgDAAgFQAAgEgDgDIgHgCIgIgCIgJgDQgEgBgDgDQgDgEAAgGQAAgJAGgFQAHgEALAAIAKABIALABIgCAJQgJgDgKAAQgGAAgFADQgDACAAAFQAAADADACQADACAEACIAIACIAKACQADABAEAEQACAEAAAGQAAAJgHAGQgGAEgLAAQgNAAgJgDg");
	this.shape_54.setTransform(128.4,179.9);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#000000").s().p("AgOArQgFgDgEgFQgEgGgDgIQgBgJAAgMQAAgLABgIQADgJAEgGQAEgFAFgDQAGgCAIAAQAJAAAFACQAHADADAFQAFAGABAJQACAIAAALQAAAMgCAJQgBAIgFAGQgDAFgHADQgFACgJAAQgIAAgGgCgAgJghQgFACgCAEQgCAEgCAHIgBAQIABARQACAHACAEQACAEAFADQAEACAFAAQAGAAAEgCQAEgDADgEQACgEACgHQABgHAAgKQAAgJgBgHQgCgHgCgEQgDgEgEgCQgEgCgGAAQgFAAgEACg");
	this.shape_55.setTransform(111.7,178.7);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#000000").s().p("AgcApIAAgJQALAFALAAQAMAAAGgFQAGgFAAgJQAAgKgGgEQgHgEgNAAIgIAAIgIABIAAgsIAwAAIAAAIIgnAAIAAAdIAMgBQAQAAAIAGQAHAFABAOQAAAOgJAGQgIAHgRAAQgMgBgLgDg");
	this.shape_56.setTransform(103.9,178.8);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#000000").s().p("AgWAZQgIgHAAgSQAAgQAIgIQAHgIAPAAQAQAAAIAIQAHAIAAAQQAAASgHAHQgIAIgQAAQgPAAgHgIgAgOgSQgFAFgBANQABANAFAGQAEAGAKAAQALAAAEgGQAGgGAAgNQAAgNgGgFQgEgGgLAAQgKAAgEAGg");
	this.shape_57.setTransform(86.5,179.9);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#000000").s().p("AAQAuIgVgdIgHAHIAAAWIgKAAIAAhSIAAgBIgBgBIgEgBIABgGIANAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAA3IAbgcIAMAAIgaAbIAbAjg");
	this.shape_58.setTransform(79.4,178.5);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIgBgBIAAgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIABABIABABIADABIgBAGg");
	this.shape_59.setTransform(74.3,178.5);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IAMAAIATAyIASgyIALAAIgbBHIgDAJIgFAHIgGADIgIABIgDAAg");
	this.shape_60.setTransform(69.2,181.3);

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_61.setTransform(63,179);

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_62.setTransform(48.3,179);

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#000000").s().p("AgWAeIAAgIQAKADALAAQAHAAAEgDQAEgDAAgFQAAgEgCgDIgIgCIgIgCIgJgDQgEgBgDgDQgDgEAAgGQAAgJAGgFQAHgEALAAIALABIAKABIgCAJQgJgDgJAAQgHAAgFADQgDACAAAFQAAADADACQADACAEACIAIACIAJACQAFABADAEQACAEAAAGQAAAJgHAGQgGAEgLAAQgMAAgKgDg");
	this.shape_63.setTransform(42.7,179.9);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgEQAEgFAGgBQAFgCAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_64.setTransform(35.8,179.9);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#000000").s().p("AgKA6IgCAAIAAgIIACAAIABAAQAHAAACgCQACgEAAgGIAAg9IgBgBIgBAAIgCgDIABgEIAMAAIABAAIAABFQAAAJgEAGQgFAFgKAAIgDAAgAABguQgBgCAAgDQAAgDABgCQACgBAEgBQADABACABQACACAAADQgBAHgGgBQgEAAgCgBg");
	this.shape_65.setTransform(29.9,180);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#000000").s().p("AASAuQAAAAAAAAQAAAAAAgBQAAAAAAAAQgBAAAAgBIAAgmQAAgIgDgEQgEgDgIAAIgFAAIgGABIgEACIgEABIAAAzIgKAAIAAhSIAAgBIgBgBIgDgBIABgGIAMAAQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAAgQAEgDAFgCQAFgCAHAAQANAAAFAGQAGAGgBAMIAAAfIABABIABABIAEABIgBAGg");
	this.shape_66.setTransform(265.9,163.3);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#000000").s().p("AgRAZQgHgHAAgSQAAgRAHgHQAHgIAQAAIAMABIAHABIgCAIIgGgBIgKgBQgKAAgFAGQgGAFAAANQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAJQgIACgKAAQgQAAgHgIg");
	this.shape_67.setTransform(258.9,164.7);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IALAAIAVAyIASgyIAKAAIgbBHIgDAJIgEAHIgHADIgJABIgCAAg");
	this.shape_68.setTransform(252.1,166.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_69.setTransform(244.8,164.6);

	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.f("#000000").s().p("AgaAuIAAhRIAAgBIgBAAIgEgCIABgFIANAAIABABIAAAFQAEgEAEgCQAFgCAIAAQAOAAAGAIQAHAIAAARQAAARgHAIQgGAHgOAAQgHAAgGgCIgIgFIAAAhgAgLggQgFAEAAAJIAAAZIAIAEQAFACAFAAQAKAAAFgGQAEgGAAgMQAAgNgEgGQgFgGgKAAQgJAAgEAFg");
	this.shape_70.setTransform(237,166);

	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#000000").s().p("AATAvQgHAAgEgCQgDgDAAgGQAAgIAKgKIgFABIgFAAQgRAAgIgIQgIgIAAgQQAAgJACgGQACgHADgEQAEgEAGgCQAFgBAHAAQAPAAAGAHQAHAIAAARIAAACIgBACIguAAQABAKAGAGQAFAFAMAAQAKAAAKgEIAAAJQgHAHgDAEQgDAEAAAEQAAAAAAABQAAABABAAQAAABAAAAQABABAAAAQACACAEAAIADAAIACgBIAAAHIgDAAIgEAAgAgNgiQgEAFgBAMIAkAAQAAgMgEgFQgEgFgJAAQgJAAgFAFg");
	this.shape_71.setTransform(229.5,166.1);

	this.shape_72 = new cjs.Shape();
	this.shape_72.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_72.setTransform(223.1,163.8);

	this.shape_73 = new cjs.Shape();
	this.shape_73.graphics.f("#000000").s().p("AgWAeIAAgIQAKADALAAQAIAAADgDQAEgDAAgFQAAgEgDgDIgHgDIgIgBIgJgDQgEgBgDgDQgDgEAAgGQAAgJAGgFQAHgEALAAIAKABIALABIgCAIQgJgCgKAAQgGAAgFACQgDADAAAFQAAAEADABQADADAEABIAIACIAKACQADABAEAEQACAEAAAGQAAAJgHAGQgGAEgLAAQgNAAgJgDg");
	this.shape_73.setTransform(217.5,164.7);

	this.shape_74 = new cjs.Shape();
	this.shape_74.graphics.f("#000000").s().p("AgWAZQgIgHAAgSQAAgRAIgHQAHgIAPAAQARAAAHAIQAHAHAAARQAAASgHAHQgHAIgRAAQgPAAgHgIgAgOgSQgGAFAAANQAAANAGAGQAEAGAKAAQALAAAEgGQAFgGABgNQgBgNgFgFQgEgGgLAAQgKAAgEAGg");
	this.shape_74.setTransform(210.4,164.7);

	this.shape_75 = new cjs.Shape();
	this.shape_75.graphics.f("#000000").s().p("AgiAsIAAhOIgBgCIgEgCIABgFIAhAAQALAAAJADQAJACAFAGQAGAFACAIQADAJAAAKQAAAMgDAIQgCAIgGAFQgFAGgJACQgJADgLAAgAgXAjIASAAQAIAAAHgCQAGgCAFgEQAEgEACgHQACgHAAgJQAAgIgCgHQgCgHgEgEQgFgEgGgCQgHgCgIAAIgSAAg");
	this.shape_75.setTransform(201.7,163.5);

	this.shape_76 = new cjs.Shape();
	this.shape_76.graphics.f("#000000").s().p("AgFAFQgBgBAAgEQAAgDABgBQADgCACAAQAEAAACACQABABAAADQAAAEgBABQgCACgEAAQgCAAgDgCg");
	this.shape_76.setTransform(189.1,167.3);

	this.shape_77 = new cjs.Shape();
	this.shape_77.graphics.f("#000000").s().p("AAPAfIgEgKIgFgOIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFAOIgEAKIgMAAIgFgNIgGgQIgGgQIgFgQIALAAIACAGIACAIIACAHIACAGIAFANIAEAMIAAAAIAEgMIAFgNIACgGIACgGIACgHIABgHIAJAAIABAHIACAHIADAGIABAGIAFANIAEAMIAFgMIAEgNIACgGIACgHIADgIIABgGIALAAIgFAQIgGAQIgGAQIgFANg");
	this.shape_77.setTransform(182,164.7);

	this.shape_78 = new cjs.Shape();
	this.shape_78.graphics.f("#000000").s().p("AgXAnQgHgIAAgSQAAgQAHgIQAIgIAPAAQARAAAHAIQAHAIAAAQQAAASgHAIQgHAIgRAAQgPAAgIgIgAgPgEQgEAEAAANQAAAOAEAFQAGAHAJAAQALAAAFgHQAEgFAAgOQAAgNgEgEQgFgHgLAAQgJAAgGAHgAgGgcIAPgSIALAAIgRASg");
	this.shape_78.setTransform(172.8,163.4);

	this.shape_79 = new cjs.Shape();
	this.shape_79.graphics.f("#000000").s().p("AgXAeIAAgIQALADALAAQAIAAADgDQAEgDAAgFQAAgEgCgDIgIgDIgIgBIgJgDQgFgBgCgDQgDgEAAgGQAAgJAHgFQAGgEALAAIALABIAJABIgBAIQgJgCgJAAQgHAAgFACQgDADAAAFQAAAEADABQACADAFABIAIACIAJACQAFABACAEQADAEAAAGQAAAJgGAGQgHAEgLAAQgMAAgLgDg");
	this.shape_79.setTransform(165.7,164.7);

	this.shape_80 = new cjs.Shape();
	this.shape_80.graphics.f("#000000").s().p("AgWAdQgHgFAAgKQAAgJAHgEQAGgEANAAQAKAAAHACIAAgIQAAgIgDgEQgFgDgJAAQgKAAgJADIAAgJIAJgBIALgBQAOAAAGAFQAGAGAAANIAAAeIABACIAAAAIAFACIgBAFIgMAAIAAAAIgBAAIAAgGQgFAIgPAAQgMAAgGgEgAgOAFQgEADAAAGQAAAGAEADQAEACAHAAQAJABAFgEQADgDAAgHIAAgGQgHgDgIAAQgJAAgEACg");
	this.shape_80.setTransform(158.9,164.7);

	this.shape_81 = new cjs.Shape();
	this.shape_81.graphics.f("#000000").s().p("AgaAuIAAhRIAAgBIgBAAIgEgCIABgFIAOAAIABABIAAAFQACgEAGgCQAEgCAHAAQAPAAAGAIQAHAIAAARQAAARgHAIQgGAHgPAAQgHAAgEgCIgIgFIAAAhgAgLggQgFAEABAJIAAAZIAHAEQAEACAGAAQALAAAEgGQAEgGAAgMQAAgNgEgGQgEgGgLAAQgJAAgEAFg");
	this.shape_81.setTransform(151.2,166);

	this.shape_82 = new cjs.Shape();
	this.shape_82.graphics.f("#000000").s().p("AgXAdQgFgFgBgKQABgJAFgEQAHgEANAAQAKAAAHACIAAgIQAAgIgDgEQgFgDgJAAQgKAAgKADIAAgJIAKgBIALgBQAOAAAGAFQAHAGgBANIAAAeIABACIABAAIADACIAAAFIgLAAIgBAAIAAAAIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGADADQAFACAHAAQAJABAFgEQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_82.setTransform(143.8,164.7);

	this.shape_83 = new cjs.Shape();
	this.shape_83.graphics.f("#000000").s().p("AgZAfIAAgHIAkguIgjAAIAAgIIAxAAIAAAHIgkAuIAlAAIAAAIg");
	this.shape_83.setTransform(136.8,164.7);

	this.shape_84 = new cjs.Shape();
	this.shape_84.graphics.f("#000000").s().p("AgXAdQgFgFAAgKQAAgJAFgEQAHgEANAAQAKAAAHACIAAgIQAAgIgDgEQgFgDgJAAQgKAAgKADIAAgJIAKgBIALgBQAOAAAGAFQAHAGAAANIAAAeIAAACIABAAIADACIgBAFIgKAAIgBAAIAAAAIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGADADQAEACAIAAQAJABAFgEQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_84.setTransform(123.6,164.7);

	this.shape_85 = new cjs.Shape();
	this.shape_85.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBAAIgDgCIABgFIAMAAIABAAIAAA9gAgDggQgBgBAAgEQAAgDABgCQACgBACgBQAEABACABQABACAAADQAAAHgHAAQgCAAgCgCg");
	this.shape_85.setTransform(118.1,163.4);

	this.shape_86 = new cjs.Shape();
	this.shape_86.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_86.setTransform(112.6,164.6);

	this.shape_87 = new cjs.Shape();
	this.shape_87.graphics.f("#000000").s().p("AgWAdQgHgFABgKQgBgJAHgEQAGgEANAAQAJAAAJACIAAgIQAAgIgFgEQgEgDgJAAQgKAAgJADIAAgJIAKgBIAKgBQAOAAAGAFQAGAGAAANIAAAeIABACIAAAAIAFACIgCAFIgLAAIAAAAIgBAAIAAgGQgGAIgOAAQgNAAgFgEgAgOAFQgEADAAAGQAAAGAEADQADACAJAAQAIABAEgEQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_87.setTransform(105.1,164.7);

	this.shape_88 = new cjs.Shape();
	this.shape_88.graphics.f("#000000").s().p("AgaAuIAAhRIAAgBIgBAAIgEgCIABgFIANAAIABABIAAAFQAEgEAEgCQAFgCAIAAQAOAAAGAIQAHAIAAARQAAARgHAIQgGAHgOAAQgHAAgGgCIgIgFIAAAhgAgLggQgFAEAAAJIAAAZIAIAEQAFACAFAAQAKAAAFgGQAEgGAAgMQAAgNgEgGQgFgGgKAAQgJAAgEAFg");
	this.shape_88.setTransform(97.5,166);

	this.shape_89 = new cjs.Shape();
	this.shape_89.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIANAAIABABIAAAHQAHgJANAAIAEAAIACAAIgBAJIgDAAIgEgBQgGAAgFACIgHAFIAAAwg");
	this.shape_89.setTransform(91.2,164.6);

	this.shape_90 = new cjs.Shape();
	this.shape_90.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgEQAEgFAGgBQAFgCAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_90.setTransform(84.9,164.7);

	this.shape_91 = new cjs.Shape();
	this.shape_91.graphics.f("#000000").s().p("AgYAfIAAgHIAkguIgjAAIAAgIIAvAAIAAAHIgkAuIAlAAIAAAIg");
	this.shape_91.setTransform(78,164.7);

	this.shape_92 = new cjs.Shape();
	this.shape_92.graphics.f("#000000").s().p("AgRAZQgHgHAAgSQAAgRAHgHQAHgIAQAAIAMABIAHABIgCAIIgGgBIgKgBQgKAAgFAGQgGAFAAANQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAJQgIACgKAAQgQAAgHgIg");
	this.shape_92.setTransform(71.5,164.7);

	this.shape_93 = new cjs.Shape();
	this.shape_93.graphics.f("#000000").s().p("AgVAuIgDAAIAAgJIACABIABAAIAGgBIAEgCIADgEIADgGIACgHIgbg/IALAAIAVAyIASgyIALAAIgbBHIgEAJIgEAHIgHADIgJABIgBAAg");
	this.shape_93.setTransform(64.7,166.1);

	this.shape_94 = new cjs.Shape();
	this.shape_94.graphics.f("#000000").s().p("AAPAfIgEgKIgFgOIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFAOIgEAKIgMAAIgFgNIgGgQIgGgQIgFgQIALAAIACAGIACAIIACAHIACAGIAFANIAEAMIAAAAIAEgMIAFgNIACgGIACgGIACgHIABgHIAJAAIABAHIACAHIADAGIABAGIAFANIAEAMIAFgMIAEgNIACgGIACgHIADgIIABgGIALAAIgFAQIgGAQIgGAQIgFANg");
	this.shape_94.setTransform(55.9,164.7);

	this.shape_95 = new cjs.Shape();
	this.shape_95.graphics.f("#000000").s().p("AgWAZQgIgHAAgSQAAgRAIgHQAGgIAQAAQAQAAAIAIQAHAHAAARQAAASgHAHQgIAIgQAAQgQAAgGgIgAgOgSQgGAFAAANQAAANAGAGQAEAGAKAAQALAAAEgGQAGgGAAgNQAAgNgGgFQgEgGgLAAQgKAAgEAGg");
	this.shape_95.setTransform(40.5,164.7);

	this.shape_96 = new cjs.Shape();
	this.shape_96.graphics.f("#000000").s().p("AgYAnQgHgIAAgSQAAgQAHgIQAGgIAPAAQAGAAAFACQAFACADAEIAAgjIALAAIAABSIAAACIABAAIAEACIgBAFIgLAAIgBAAIgBgBIAAgGQgEAEgFACQgGADgGAAQgPAAgGgIgAgQgEQgEAEAAANQAAAOAEAFQAEAHALAAQAGAAAEgCIAHgFIAAgaQABgIgFgEQgEgEgJgBQgLAAgEAHg");
	this.shape_96.setTransform(32.8,163.4);

	this.shape_97 = new cjs.Shape();
	this.shape_97.graphics.f("#000000").s().p("AgJAsQgEgCgEgEIgBAHIgIAAIAAhSIAAgBIgBgBIgEgBIABgGIANAAQABAAAAABQAAAAAAAAQAAAAAAAAQAAABAAAAIAAAhQAEgEAEgCQAGgCAHAAQAOAAAGAIQAHAIAAAQQAAASgHAIQgGAIgOAAQgIgBgGgCgAgLgGQgFAEAAAIIAAAaIAIAFQAEACAGAAQAKAAAEgHQAFgFAAgOQAAgNgFgFQgEgFgKgBQgJAAgEAFg");
	this.shape_97.setTransform(265.7,148.2);

	this.shape_98 = new cjs.Shape();
	this.shape_98.graphics.f("#000000").s().p("AgRAaQgHgGAAgNIAAgeIgBgBIgEgCIABgFIANAAIABABIAAAkQAAAKAEAEQAEAEAIAAQAKAAAEgEQAEgEAAgKIAAglIAKAAIAAAmQAAANgGAGQgHAGgPAAQgNAAgGgGg");
	this.shape_98.setTransform(257.8,149.6);

	this.shape_99 = new cjs.Shape();
	this.shape_99.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIgBgBIAAgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIABABIABABIADABIgBAGg");
	this.shape_99.setTransform(252.4,148.1);

	this.shape_100 = new cjs.Shape();
	this.shape_100.graphics.f("#000000").s().p("AgRAsIAjhOIguAAIAAgJIA5AAIAAAIIgjBPg");
	this.shape_100.setTransform(242.7,148.3);

	this.shape_101 = new cjs.Shape();
	this.shape_101.graphics.f("#000000").s().p("AAHAsIAAhNIgXAKIAAgIIAYgMIAJAAIAABXg");
	this.shape_101.setTransform(234.4,148.3);

	this.shape_102 = new cjs.Shape();
	this.shape_102.graphics.f("#000000").s().p("AgNArQgGgDgFgFQgDgGgCgIQgCgJAAgMQAAgLACgIQACgJADgGQAFgFAGgDQAFgCAIAAQAIAAAHACQAGADAEAFQADAGACAJQACAIAAALQAAAMgCAJQgCAIgDAGQgEAFgGADQgHACgIAAQgIAAgFgCgAgJghQgFACgCAEQgDAEgBAHIgBAQIABARQABAHADAEQACAEAFADQAEACAFAAQAGAAAEgCQAEgDADgEQACgEABgHQACgHAAgKQAAgJgCgHQgBgHgCgEQgDgEgEgCQgEgCgGAAQgFAAgEACg");
	this.shape_102.setTransform(227,148.3);

	this.shape_103 = new cjs.Shape();
	this.shape_103.graphics.f("#000000").s().p("AgcAsIAAgIIAighQAGgEADgGQACgFAAgGQAAgJgEgDQgFgFgKAAQgLABgMADIAAgIIAMgEIANgBQAOAAAHAHQAHAFAAAOQAAAHgDAGQgEAFgGAHIgdAdIArAAIAAAIg");
	this.shape_103.setTransform(218.9,148.2);

	this.shape_104 = new cjs.Shape();
	this.shape_104.graphics.f("#000000").s().p("AgFAFQgBgBAAgEQAAgDABgBQADgCACAAQAEAAABACQACABAAADQAAAEgCABQgBACgEAAQgCAAgDgCg");
	this.shape_104.setTransform(213.4,152.1);

	this.shape_105 = new cjs.Shape();
	this.shape_105.graphics.f("#000000").s().p("AAHAsIAAhNIgXAKIAAgIIAYgMIAJAAIAABXg");
	this.shape_105.setTransform(207.3,148.3);

	this.shape_106 = new cjs.Shape();
	this.shape_106.graphics.f("#000000").s().p("AAHAsIAAhNIgXAKIAAgIIAYgMIAJAAIAABXg");
	this.shape_106.setTransform(199.4,148.3);

	this.shape_107 = new cjs.Shape();
	this.shape_107.graphics.f("#000000").s().p("AgEAFQgCgBAAgEQAAgDACgBQACgCACAAQAEAAACACQABABAAADQAAAEgBABQgCACgEAAQgCAAgCgCg");
	this.shape_107.setTransform(194.2,152.1);

	this.shape_108 = new cjs.Shape();
	this.shape_108.graphics.f("#000000").s().p("AgNArQgGgCgEgFQgEgFgBgIQgCgIAAgLQAAgMACgJQACgJAFgHQAFgFAHgDQAHgDAKAAQAKAAAJADIAAAIIgJgCIgKgBQgIAAgEACQgGACgDAFQgEAEgBAHQgCAHAAAJQACgFAGgDQAGgDAJAAQAPAAAHAHQAGAFAAAPQAAAPgHAHQgHAHgRAAQgHAAgGgCgAgOABQgFAEAAAKQAAAWATAAQALAAAFgFQAFgFAAgLQAAgKgEgFQgFgEgKAAQgLAAgFAEg");
	this.shape_108.setTransform(188.6,148.3);

	this.shape_109 = new cjs.Shape();
	this.shape_109.graphics.f("#000000").s().p("AgXAZQgHgIAAgRQAAgRAHgHQAIgIAPAAQARAAAHAIQAHAHAAARQAAARgHAIQgHAIgRAAQgPAAgIgIgAgPgSQgEAFAAANQAAANAEAGQAGAGAJAAQALAAAFgGQAEgGAAgNQAAgNgEgFQgFgGgLAAQgJAAgGAGg");
	this.shape_109.setTransform(176.7,149.5);

	this.shape_110 = new cjs.Shape();
	this.shape_110.graphics.f("#000000").s().p("AgYAnQgHgIAAgSQAAgQAHgIQAGgIAOAAQAHAAAFACQAFACAEAEIAAgjIAKAAIAABSIAAACIABAAIAEABIgCAGIgKAAIgBAAIAAgBIgBgGQgDAEgGACQgFACgIABQgOAAgGgIgAgQgFQgFAFAAANQAAAOAFAFQAFAHAKAAQAFAAAFgCIAIgFIAAgaQAAgIgFgEQgEgFgJAAQgKABgFAFg");
	this.shape_110.setTransform(169,148.2);

	this.shape_111 = new cjs.Shape();
	this.shape_111.graphics.f("#000000").s().p("AgZAsIAAgHIAkgvIgjAAIAAgIIAxAAIAAAIIgkAuIAkAAIAAAIgAgEggQgCgBAAgEQAAgEACgBQABgBADgBQADABACABQABABABAEQgBAEgBABQgCACgDAAQgDAAgBgCg");
	this.shape_111.setTransform(157.7,148.2);

	this.shape_112 = new cjs.Shape();
	this.shape_112.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBAAIgDgCIABgGIAMAAIABABIAAA9gAgDggQgBgBAAgEQAAgEABgBQACgBACgBQAEABACABQABABAAAEQAAAHgHAAQgCAAgCgCg");
	this.shape_112.setTransform(152.5,148.2);

	this.shape_113 = new cjs.Shape();
	this.shape_113.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_113.setTransform(147,149.4);

	this.shape_114 = new cjs.Shape();
	this.shape_114.graphics.f("#000000").s().p("AgKA6IgCAAIAAgIIACAAIABAAQAHAAACgDQACgDAAgGIAAg9IAAgBIgCAAIgCgCIABgGIAMAAIABABIAABFQAAAKgFAFQgEAGgJgBIgEAAgAACguQgCgBAAgEQAAgEACgBQABgBADgBQAEABACABQABABABAEQAAAHgIAAQgDAAgBgCg");
	this.shape_114.setTransform(136.7,149.6);

	this.shape_115 = new cjs.Shape();
	this.shape_115.graphics.f("#000000").s().p("AgUAZQgIgIAAgRQAAgIACgGQACgGADgEQAEgFAGgCQAFgBAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_115.setTransform(132.1,149.5);

	this.shape_116 = new cjs.Shape();
	this.shape_116.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBAAIgDgCIABgGIAMAAIABABIAAA9gAgDggQgBgBAAgEQAAgEABgBQACgBACgBQAEABACABQABABAAAEQAAAHgHAAQgCAAgCgCg");
	this.shape_116.setTransform(126.6,148.2);

	this.shape_117 = new cjs.Shape();
	this.shape_117.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_117.setTransform(121.2,149.4);

	this.shape_118 = new cjs.Shape();
	this.shape_118.graphics.f("#000000").s().p("AgYAuIAAgHIAkgvIgjAAIAAgIIAvAAIAAAHIgkAvIAlAAIAAAIgAgGgbIAPgSIALAAIgRASg");
	this.shape_118.setTransform(114,148.1);

	this.shape_119 = new cjs.Shape();
	this.shape_119.graphics.f("#000000").s().p("AgWAnQgIgIAAgSQAAgQAIgIQAHgIAPAAQAQAAAIAIQAHAIAAAQQAAASgHAIQgIAIgQAAQgPAAgHgIgAgOgFQgFAFgBANQABAOAFAFQAEAHAKAAQALAAAEgHQAGgFAAgOQAAgNgGgFQgEgFgLgBQgKABgEAFgAgFgcIAOgSIAMAAIgSASg");
	this.shape_119.setTransform(106.8,148.2);

	this.shape_120 = new cjs.Shape();
	this.shape_120.graphics.f("#000000").s().p("AgaAuIAAhRIAAgBIgBAAIgEgCIABgFIANAAIABABIAAAFQAEgEAEgCQAFgCAHAAQAPAAAHAIQAGAIAAARQAAARgGAIQgHAHgPAAQgHAAgFgCIgIgFIAAAhgAgLggQgEAEgBAJIAAAZIAIAEQAFACAFAAQAKAAAFgGQAEgGAAgMQAAgNgEgGQgFgGgKAAQgJAAgEAFg");
	this.shape_120.setTransform(99,150.8);

	this.shape_121 = new cjs.Shape();
	this.shape_121.graphics.f("#000000").s().p("AgUAZQgIgIAAgRQAAgIACgGQACgGADgEQAEgFAGgCQAFgBAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_121.setTransform(87.5,149.5);

	this.shape_122 = new cjs.Shape();
	this.shape_122.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBAAIgDgCIABgGIAMAAIABABIAAA9gAgDggQgBgBAAgEQAAgEABgBQACgBACgBQAEABACABQABABAAAEQAAAHgHAAQgCAAgCgCg");
	this.shape_122.setTransform(82,148.2);

	this.shape_123 = new cjs.Shape();
	this.shape_123.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_123.setTransform(76.6,149.4);

	this.shape_124 = new cjs.Shape();
	this.shape_124.graphics.f("#000000").s().p("AgXAdQgFgFgBgKQABgJAFgEQAHgEANAAQAKAAAHACIAAgIQAAgIgDgEQgFgDgJAAQgKAAgKADIAAgJIAKgCIALAAQAOAAAGAGQAHAFgBANIAAAeIABACIABAAIADACIAAAFIgLAAIgBAAIAAAAIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGAEADQAEACAHAAQAJAAAFgDQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_124.setTransform(65,149.5);

	this.shape_125 = new cjs.Shape();
	this.shape_125.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_125.setTransform(57.4,149.4);

	this.shape_126 = new cjs.Shape();
	this.shape_126.graphics.f("#000000").s().p("AgZAsIAAgHIAlgvIgjAAIAAgIIAvAAIAAAIIgjAuIAkAAIAAAIgAgEggQgBgBAAgEQAAgEABgBQACgBACgBQADABACABQABABAAAEQAAAEgBABQgCACgDAAQgCAAgCgCg");
	this.shape_126.setTransform(50.2,148.2);

	this.shape_127 = new cjs.Shape();
	this.shape_127.graphics.f("#000000").s().p("AgXAdQgFgFAAgKQAAgJAFgEQAHgEANAAQAJAAAJACIAAgIQAAgIgFgEQgEgDgJAAQgKAAgKADIAAgJIALgCIAKAAQANAAAHAGQAGAFABANIAAAeIAAACIABAAIADACIgBAFIgKAAIgBAAIAAAAIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGADADQAFACAIAAQAIAAAEgDQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_127.setTransform(43.3,149.5);

	this.shape_128 = new cjs.Shape();
	this.shape_128.graphics.f("#000000").s().p("AAPAfIgEgKIgFgOIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFAOIgEAKIgMAAIgFgNIgGgQIgGgQIgFgRIALAAIACAHIACAIIACAHIACAGIAFANIAEAMIAAAAIAEgMIAFgNIACgGIACgGIACgIIABgGIAJAAIABAGIACAIIADAGIABAGIAFANIAEAMIAFgMIAEgNIACgGIACgHIADgIIABgHIALAAIgFARIgGAQIgGAQIgFANg");
	this.shape_128.setTransform(34.2,149.5);

	this.shape_129 = new cjs.Shape();
	this.shape_129.graphics.f("#000000").s().p("AAQAuIgVgdIgHAHIAAAWIgKAAIAAhSIAAgBIgBgBIgEgBIABgGIANAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAA3IAbgcIAMAAIgaAbIAbAjg");
	this.shape_129.setTransform(266.5,132.9);

	this.shape_130 = new cjs.Shape();
	this.shape_130.graphics.f("#000000").s().p("AgWAdQgHgFAAgKQAAgJAHgEQAGgEANAAQAJAAAIADIAAgJQAAgIgDgEQgFgDgJAAQgKAAgJADIAAgJIAKgCIAKAAQANAAAHAGQAGAFAAANIAAAeIABACIAAAAIAFACIgBAFIgMAAIAAAAIgBgBIAAgFQgGAIgOAAQgNAAgFgEgAgOAFQgEADAAAGQAAAGAEADQADADAIAAQAJAAAFgEQADgDAAgHIAAgGQgHgDgIAAQgJAAgEACg");
	this.shape_130.setTransform(259.6,134.3);

	this.shape_131 = new cjs.Shape();
	this.shape_131.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_131.setTransform(252,134.2);

	this.shape_132 = new cjs.Shape();
	this.shape_132.graphics.f("#000000").s().p("AgZAnQgGgIAAgSQAAgQAGgIQAHgIAPABQAGAAAFABQAFACADAEIAAgiIALAAIAABRIAAACIABAAIAEABIgBAGIgMAAIAAAAIgBgBIAAgGQgDAEgGACQgGACgGABQgPgBgHgHgAgQgFQgEAFAAANQAAAOAEAFQAEAHALAAQAFAAAFgCIAHgFIAAgaQAAgIgEgEQgEgFgJAAQgLABgEAFg");
	this.shape_132.setTransform(244.2,133);

	this.shape_133 = new cjs.Shape();
	this.shape_133.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgDQAEgEAGgDQAFgBAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgHgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_133.setTransform(236.6,134.3);

	this.shape_134 = new cjs.Shape();
	this.shape_134.graphics.f("#000000").s().p("AgKA6IgCAAIAAgIIACAAIABAAQAHAAACgDQACgDAAgHIAAg8IgBgBIgBgBIgCgBIABgGIAMAAIABABIAABEQAAALgFAFQgEAGgKgBIgDAAgAABguQgBgBAAgEQAAgEABgBQACgCAEAAQADAAACACQABABABAEQAAAGgHABQgEgBgCgBg");
	this.shape_134.setTransform(230.7,134.4);

	this.shape_135 = new cjs.Shape();
	this.shape_135.graphics.f("#000000").s().p("AgIAPIAHgdIAKAAIgJAdg");
	this.shape_135.setTransform(219,137.8);

	this.shape_136 = new cjs.Shape();
	this.shape_136.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgDQAEgEAGgDQAFgBAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgHgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_136.setTransform(213.9,134.3);

	this.shape_137 = new cjs.Shape();
	this.shape_137.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBgBIgDgBIABgGIAMAAIABABIAAA9gAgDggQgBgBAAgEQAAgEABgBQACgCACAAQAEAAACACQABABAAAEQAAAGgHABQgCgBgCgBg");
	this.shape_137.setTransform(208.4,133);

	this.shape_138 = new cjs.Shape();
	this.shape_138.graphics.f("#000000").s().p("AgWAeIAAgJQAKAEALAAQAHAAAEgDQAEgDAAgFQAAgEgCgCIgIgEIgIgBIgJgDQgEgBgDgEQgDgDAAgGQAAgKAGgDQAHgFALAAIALAAIAKACIgCAIQgJgCgKAAQgHAAgEACQgDADAAAFQAAAEADACQADACAEAAIAIACIAKAEQADAAAEAEQACADAAAHQAAAJgHAGQgGAEgLAAQgNAAgJgDg");
	this.shape_138.setTransform(203.7,134.3);

	this.shape_139 = new cjs.Shape();
	this.shape_139.graphics.f("#000000").s().p("AgWAdQgHgFABgKQgBgJAHgEQAGgEANAAQAJAAAJADIAAgJQAAgIgFgEQgEgDgJAAQgKAAgJADIAAgJIAKgCIAKAAQANAAAHAGQAGAFABANIAAAeIAAACIAAAAIAEACIgBAFIgLAAIAAAAIgBgBIAAgFQgGAIgOAAQgNAAgFgEgAgOAFQgEADAAAGQAAAGADADQAFADAIAAQAIAAAEgEQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_139.setTransform(196.8,134.3);

	this.shape_140 = new cjs.Shape();
	this.shape_140.graphics.f("#000000").s().p("AgYAfIAAgHIAkgvIgjAAIAAgIIAvAAIAAAHIgjAvIAkAAIAAAIg");
	this.shape_140.setTransform(189.8,134.3);

	this.shape_141 = new cjs.Shape();
	this.shape_141.graphics.f("#000000").s().p("AgRAaQgHgIAAgSQAAgRAHgHQAHgIAQAAIAMAAIAHACIgCAIIgGgBIgKgBQgKAAgFAGQgGAGAAAMQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAIQgIADgKAAQgQAAgHgHg");
	this.shape_141.setTransform(183.3,134.3);

	this.shape_142 = new cjs.Shape();
	this.shape_142.graphics.f("#000000").s().p("AAPAfIgEgLIgFgNIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFANIgEALIgMAAIgFgNIgGgQIgGgRIgFgQIALAAIACAHIACAIIACAHIACAGIAFANIAEAMIAAAAIAEgMIAFgNIACgGIACgGIACgIIABgGIAJAAIABAGIACAIIADAGIABAGIAFANIAEAMIAFgMIAEgNIACgGIACgHIADgIIABgHIALAAIgFAQIgGARIgGAQIgFANg");
	this.shape_142.setTransform(165.9,134.3);

	this.shape_143 = new cjs.Shape();
	this.shape_143.graphics.f("#000000").s().p("AgWAdQgHgFABgKQgBgJAHgEQAGgEANAAQAJAAAJADIAAgJQAAgIgFgEQgEgDgJAAQgKAAgJADIAAgJIAKgCIAKAAQANAAAHAGQAGAFABANIAAAeIAAACIABAAIADACIgBAFIgLAAIAAAAIgBgBIAAgFQgGAIgOAAQgNAAgFgEgAgOAFQgEADAAAGQAAAGADADQAFADAIAAQAIAAAEgEQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_143.setTransform(148.2,134.3);

	this.shape_144 = new cjs.Shape();
	this.shape_144.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_144.setTransform(140.6,134.2);

	this.shape_145 = new cjs.Shape();
	this.shape_145.graphics.f("#000000").s().p("AgWAaQgIgIAAgSQAAgRAIgHQAHgIAPAAQAQAAAIAIQAHAHAAARQAAASgHAIQgIAHgQAAQgPAAgHgHgAgOgSQgFAGgBAMQABANAFAGQAEAGAKAAQALAAAEgGQAGgGAAgNQAAgMgGgGQgEgGgLAAQgKAAgEAGg");
	this.shape_145.setTransform(132.9,134.3);

	this.shape_146 = new cjs.Shape();
	this.shape_146.graphics.f("#000000").s().p("AgZAfIAAgHIAkgvIgjAAIAAgIIAxAAIAAAHIgkAvIAkAAIAAAIg");
	this.shape_146.setTransform(125.7,134.3);

	this.shape_147 = new cjs.Shape();
	this.shape_147.graphics.f("#000000").s().p("AgRAaQgHgIAAgSQAAgRAHgHQAHgIAQAAIAMAAIAHACIgCAIIgGgBIgKgBQgKAAgFAGQgGAGAAAMQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAIQgIADgKAAQgQAAgHgHg");
	this.shape_147.setTransform(119.2,134.3);

	this.shape_148 = new cjs.Shape();
	this.shape_148.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBgBIgDgBIABgGIAMAAIABABIAAA9gAgDggQgBgBAAgEQAAgEABgBQACgCACAAQAEAAACACQABABAAAEQAAAGgHABQgCgBgCgBg");
	this.shape_148.setTransform(114.1,133);

	this.shape_149 = new cjs.Shape();
	this.shape_149.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_149.setTransform(108.7,134.2);

	this.shape_150 = new cjs.Shape();
	this.shape_150.graphics.f("#000000").s().p("AgWAdQgHgFAAgKQAAgJAHgEQAGgEANAAQAJAAAIADIAAgJQAAgIgDgEQgFgDgJAAQgKAAgJADIAAgJIAKgCIAKAAQANAAAHAGQAGAFAAANIAAAeIABACIAAAAIAFACIgBAFIgMAAIAAAAIgBgBIAAgFQgGAIgOAAQgNAAgFgEgAgOAFQgEADAAAGQAAAGAEADQADADAIAAQAJAAAFgEQADgDAAgHIAAgGQgHgDgIAAQgJAAgEACg");
	this.shape_150.setTransform(101.2,134.3);

	this.shape_151 = new cjs.Shape();
	this.shape_151.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIAAABIAAAHQAHgJANAAIAEAAIACAAIgBAJIgDAAIgEgBQgGAAgFACIgHAFIAAAwg");
	this.shape_151.setTransform(95.1,134.2);

	this.shape_152 = new cjs.Shape();
	this.shape_152.graphics.f("#000000").s().p("AgFAuIgGAAIAAgJIAHABIAFAAQASAAAAgQIAAgKQgDAEgFACQgGACgGAAQgPgBgGgHQgHgIAAgRQAAgRAHgIQAGgIAPABQAFAAAHABQAFACAFADIAAgFIAJAAIAABAQgBAJgCAEQgCAGgEACQgEAEgFABIgKABIgHgBgAgOggQgEAGAAANQAAANAEAFQAFAHAKAAQAJgBAEgEQAFgEAAgJIAAgaIgIgEIgKgCQgKABgFAFg");
	this.shape_152.setTransform(88.3,135.7);

	this.shape_153 = new cjs.Shape();
	this.shape_153.graphics.f("#000000").s().p("AgXAaQgHgIAAgSQAAgRAHgHQAIgIAPAAQAQAAAIAIQAHAHAAARQAAASgHAIQgIAHgQAAQgPAAgIgHgAgPgSQgEAGgBAMQABANAEAGQAFAGAKAAQALAAAEgGQAGgGgBgNQABgMgGgGQgEgGgLAAQgKAAgFAGg");
	this.shape_153.setTransform(80.8,134.3);

	this.shape_154 = new cjs.Shape();
	this.shape_154.graphics.f("#000000").s().p("AgXAdQgFgFgBgKQABgJAFgEQAHgEANAAQAKAAAHADIAAgJQAAgIgDgEQgFgDgJAAQgKAAgKADIAAgJIAKgCIALAAQAOAAAGAGQAHAFgBANIAAAeIABACIABAAIAEACIgBAFIgLAAIgBAAIAAgBIgBgFQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGAEADQAEADAHAAQAJAAAFgEQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_154.setTransform(64.6,134.3);

	this.shape_155 = new cjs.Shape();
	this.shape_155.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_155.setTransform(58.1,133.4);

	this.shape_156 = new cjs.Shape();
	this.shape_156.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIANAAIACABIAAAHQAGgJANAAIADAAIADAAIgCAJIgCAAIgDgBQgIAAgEACIgGAFIAAAwg");
	this.shape_156.setTransform(53.3,134.2);

	this.shape_157 = new cjs.Shape();
	this.shape_157.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgDQAEgEAGgDQAFgBAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgHgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_157.setTransform(46.9,134.3);

	this.shape_158 = new cjs.Shape();
	this.shape_158.graphics.f("#000000").s().p("AgMAuIAAg0IAAgCIgBAAIgHgCIABgGIAHAAIAAgIQAAgLAFgGQAFgEALAAIAIAAIAEABIgCAIIgEgBIgFAAQgGAAgEACQgBAEAAAGIAAAJIASAAIAAAIIgSAAIAAA2g");
	this.shape_158.setTransform(41.4,132.8);

	this.shape_159 = new cjs.Shape();
	this.shape_159.graphics.f("#000000").s().p("AgSArQgHgDgFgFQgGgGgCgIQgDgJAAgMQAAgLADgIQACgJAGgGQAFgFAHgDQAIgCAKAAQALAAAIACQAHADAGAFQAFAGACAJQADAIAAALQAAAMgDAJQgCAIgFAGQgGAFgHADQgIACgLAAQgKAAgIgCgAgNghQgGACgEAEQgDAEgCAHQgBAHAAAJQAAAKABAHQACAHADAEQAEAEAGADQAGACAHAAQAIAAAGgCQAGgDAEgEQADgEABgHQACgHAAgKQAAgJgCgHQgBgHgDgEQgEgEgGgCQgGgCgIAAQgHAAgGACg");
	this.shape_159.setTransform(33.8,133.1);

	this.shape_160 = new cjs.Shape();
	this.shape_160.graphics.f("#000000").s().p("AgFAFQgBgBAAgEQAAgDABgBQACgCADAAQAEAAABACQACABAAADQAAAEgCABQgBACgEAAQgDAAgCgCg");
	this.shape_160.setTransform(268.1,121.7);

	this.shape_161 = new cjs.Shape();
	this.shape_161.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgEQAEgEAGgCQAFgBAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_161.setTransform(262.8,119.1);

	this.shape_162 = new cjs.Shape();
	this.shape_162.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBgBIgDgBIABgGIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgEABgBQACgCACAAQAEAAACACQABABAAAEQAAAGgHABQgCgBgCgBg");
	this.shape_162.setTransform(257.3,117.8);

	this.shape_163 = new cjs.Shape();
	this.shape_163.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_163.setTransform(251.9,119);

	this.shape_164 = new cjs.Shape();
	this.shape_164.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgEQAEgEAGgCQAFgBAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_164.setTransform(244.4,119.1);

	this.shape_165 = new cjs.Shape();
	this.shape_165.graphics.f("#000000").s().p("AgRAaQgHgIAAgSQAAgQAHgJQAHgHAQAAIAMAAIAHACIgCAIIgGgBIgKgBQgKAAgFAGQgGAGAAAMQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAIQgIADgKAAQgQAAgHgHg");
	this.shape_165.setTransform(237.6,119.1);

	this.shape_166 = new cjs.Shape();
	this.shape_166.graphics.f("#000000").s().p("AAPAfIgEgLIgFgNIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFANIgEALIgMAAIgFgNIgGgQIgGgRIgFgQIALAAIACAHIACAIIACAHIACAGIAFANIAEAMIAAAAIAEgMIAFgNIACgGIACgHIACgHIABgGIAJAAIABAGIACAHIADAHIABAGIAFANIAEAMIAFgMIAEgNIACgGIACgHIADgIIABgHIALAAIgFAQIgGARIgGAQIgFANg");
	this.shape_166.setTransform(221.8,119.1);

	this.shape_167 = new cjs.Shape();
	this.shape_167.graphics.f("#000000").s().p("AAmAgQAAAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgDgEgJAAIgKABQgFACgDACIABAFIABAFIAAAoIgKAAIAAgoQAAgIgDgDQgEgEgHAAIgGABIgFABIgFABIgCACIAAAyIgKAAIAAg1IAAgBIgBgBIgFgCIABgFIAOAAIABABIAAAEIAIgFQAFgBAGAAQAJAAADACQAFACACAEQAEgEAFgCQAHgCAHAAQAMAAAFAFQAGAGgBAMIAAAfIAAACIABAAIAFACIgBAFg");
	this.shape_167.setTransform(203.4,119);

	this.shape_168 = new cjs.Shape();
	this.shape_168.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IALAAIAVAyIASgyIALAAIgbBHIgEAJIgEAHIgHADIgJABIgCAAg");
	this.shape_168.setTransform(194,120.5);

	this.shape_169 = new cjs.Shape();
	this.shape_169.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_169.setTransform(186.7,119);

	this.shape_170 = new cjs.Shape();
	this.shape_170.graphics.f("#000000").s().p("AgWAaQgIgIAAgSQAAgQAIgJQAHgHAPAAQARAAAHAHQAHAJAAAQQAAASgHAIQgHAHgRAAQgPAAgHgHgAgOgSQgGAGAAAMQAAANAGAGQAEAGAKAAQALAAAEgGQAFgGABgNQgBgMgFgGQgEgGgLAAQgKAAgEAGg");
	this.shape_170.setTransform(178.9,119.1);

	this.shape_171 = new cjs.Shape();
	this.shape_171.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBgBIgDgBIABgGIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgEABgBQACgCACAAQAEAAACACQABABAAAEQAAAGgHABQgCgBgCgBg");
	this.shape_171.setTransform(173.2,117.8);

	this.shape_172 = new cjs.Shape();
	this.shape_172.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_172.setTransform(167.8,119);

	this.shape_173 = new cjs.Shape();
	this.shape_173.graphics.f("#000000").s().p("AgYAnQgHgIAAgSQAAgQAHgIQAGgIAOABQAHAAAFABQAFACAEAFIAAgjIAKAAIAABRIAAACIABAAIAEABIgCAGIgKAAIgBAAIAAgBIgBgGQgDAEgGACQgFACgIABQgOgBgGgHgAgQgFQgFAFAAANQAAANAFAGQAFAHAKgBQAFAAAFgBIAIgFIAAgZQAAgJgFgEQgEgFgJABQgKAAgFAFg");
	this.shape_173.setTransform(160,117.8);

	this.shape_174 = new cjs.Shape();
	this.shape_174.graphics.f("#000000").s().p("AATAvQgHAAgEgCQgDgDAAgGQAAgIAKgKIgFABIgFAAQgRAAgIgIQgIgIAAgQQAAgJACgGQACgHADgEQAEgEAGgCQAFgBAHAAQAPAAAGAHQAHAIAAARIAAACIgBACIguAAQABAKAGAGQAFAFAMAAQAKAAAKgEIAAAJQgHAHgDAEQgDAEAAAEQAAABAAAAQAAABABAAQAAABAAAAQABABAAAAQACACAEAAIADAAIACgBIAAAHIgDAAIgEAAgAgNgiQgEAFgBAMIAkAAQAAgMgEgFQgEgFgJAAQgJAAgFAFg");
	this.shape_174.setTransform(152.4,120.5);

	this.shape_175 = new cjs.Shape();
	this.shape_175.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIAAgBIgBgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIAAABIABABIAEABIgBAGg");
	this.shape_175.setTransform(147.1,117.7);

	this.shape_176 = new cjs.Shape();
	this.shape_176.graphics.f("#000000").s().p("AgFAuIgGAAIAAgJIAHABIAFAAQASAAAAgQIAAgLQgDAEgFADQgGABgGABQgPgBgGgHQgHgIAAgRQAAgRAHgIQAGgIAPABQAFAAAHABQAFABAFAEIAAgFIAJAAIAABAQgBAIgCAGQgCAEgEADQgEAEgFABIgKAAIgHAAgAgOggQgEAGAAANQAAANAEAFQAFAHAKgBQAJAAAEgEQAFgEAAgJIAAgbIgIgDIgKgBQgKAAgFAFg");
	this.shape_176.setTransform(141.4,120.5);

	this.shape_177 = new cjs.Shape();
	this.shape_177.graphics.f("#000000").s().p("AgZAfIAAgGIAkgwIgjAAIAAgIIAxAAIAAAHIglAvIAmAAIAAAIg");
	this.shape_177.setTransform(134.4,119.1);

	this.shape_178 = new cjs.Shape();
	this.shape_178.graphics.f("#000000").s().p("AAPAfIgEgLIgFgNIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFANIgEALIgMAAIgFgNIgGgQIgGgRIgFgQIALAAIACAHIACAIIACAHIACAGIAFANIAEAMIAAAAIAEgMIAFgNIACgGIACgHIACgHIABgGIAJAAIABAGIACAHIADAHIABAGIAFANIAEAMIAFgMIAEgNIACgGIACgHIADgIIABgHIALAAIgFAQIgGARIgGAQIgFANg");
	this.shape_178.setTransform(125.7,119.1);

	this.shape_179 = new cjs.Shape();
	this.shape_179.graphics.f("#000000").s().p("AgSAaQgGgGAAgNIAAgeIgBgBIgEgCIABgFIANAAIABABIAAAkQAAAKAEAEQAEAEAIAAQAKAAAEgEQAEgEAAgKIAAglIAKAAIAAAmQAAANgGAGQgHAGgPAAQgNAAgHgGg");
	this.shape_179.setTransform(116.3,119.2);

	this.shape_180 = new cjs.Shape();
	this.shape_180.graphics.f("#000000").s().p("AgZAsIAAgGIAkgwIgjAAIAAgIIAxAAIAAAHIgkAvIAlAAIAAAIgAgEggQgBgCgBgDQABgEABgBQABgCADAAQAEAAABACQACABAAAEQAAADgCACQgBABgEABQgDgBgBgBg");
	this.shape_180.setTransform(102.1,117.8);

	this.shape_181 = new cjs.Shape();
	this.shape_181.graphics.f("#000000").s().p("AgRAaQgHgGAAgNIAAgeIgBgBIgEgCIABgFIANAAIABABIAAAkQAAAKAEAEQAEAEAIAAQAKAAAEgEQAEgEAAgKIAAglIAKAAIAAAmQAAANgHAGQgGAGgPAAQgNAAgGgGg");
	this.shape_181.setTransform(94.7,119.2);

	this.shape_182 = new cjs.Shape();
	this.shape_182.graphics.f("#000000").s().p("AgJA6IgDAAIAAgHIACAAIACAAQAGgBACgDQACgDAAgHIAAg8IgBgBIgBgBIgCgBIABgGIAMAAIABABIAABEQAAALgEAFQgFAGgKgBIgCAAgAABguQgBgCAAgDQAAgEABgBQACgCAEAAQADAAACACQACABAAAEQgBAGgGABQgEgBgCgBg");
	this.shape_182.setTransform(88.7,119.2);

	this.shape_183 = new cjs.Shape();
	this.shape_183.graphics.f("#000000").s().p("AAnAgQgBAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgEgEgHAAIgLABQgFACgCACIAAAFIABAFIAAAoIgKAAIAAgoQAAgIgEgDQgDgEgIAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIAAgBIgEgCIABgFIANAAIABABIAAAEIAIgFQAFgBAGAAQAIAAAFACQAEACACAEQAEgEAGgCQAGgCAGAAQAMAAAGAFQAGAGAAAMIAAAfIAAACIABAAIADACIgBAFg");
	this.shape_183.setTransform(74.6,119);

	this.shape_184 = new cjs.Shape();
	this.shape_184.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgEQAEgEAGgCQAFgBAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgEIAAAJIgKACIgLABQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_184.setTransform(65.1,119.1);

	this.shape_185 = new cjs.Shape();
	this.shape_185.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_185.setTransform(58.7,118.2);

	this.shape_186 = new cjs.Shape();
	this.shape_186.graphics.f("#000000").s().p("AgXAcQgFgEAAgKQAAgJAFgEQAHgEANAAQAJAAAJADIAAgJQAAgIgFgEQgEgDgJAAQgKAAgKADIAAgJIALgCIAKAAQANAAAHAGQAGAFABANIAAAeIAAACIABABIADABIgBAFIgKAAIgBAAIAAgBIgBgFQgFAIgPAAQgMAAgHgFgAgOAFQgEADAAAGQAAAGADADQAFADAIAAQAIAAAEgEQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_186.setTransform(52.7,119.1);

	this.shape_187 = new cjs.Shape();
	this.shape_187.graphics.f("#000000").s().p("AgJAsQgEgCgEgEIgBAHIgIAAIAAhRIAAgCIgBgBIgEgBIABgFIANAAQABAAAAAAQAAAAAAAAQAAAAAAAAQAAABAAAAIAAAiQADgFAFgCQAFgBAIAAQAOgBAHAIQAGAIAAAQQAAASgGAIQgHAHgOABQgIgBgGgCgAgLgGQgEAEgBAJIAAAZIAIAFQAEABAGAAQAKABAFgHQAEgGAAgNQAAgNgEgFQgFgFgKAAQgJgBgEAFg");
	this.shape_187.setTransform(45.1,117.8);

	this.shape_188 = new cjs.Shape();
	this.shape_188.graphics.f("#000000").s().p("AgXAcQgFgEAAgKQAAgJAFgEQAHgEANAAQAKAAAHADIAAgJQAAgIgDgEQgFgDgJAAQgKAAgKADIAAgJIAKgCIALAAQAOAAAGAGQAHAFAAANIAAAeIAAACIABABIADABIgBAFIgKAAIgBAAIAAgBIgBgFQgFAIgPAAQgMAAgHgFgAgOAFQgEADAAAGQAAAGADADQAEADAIAAQAJAAAFgEQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_188.setTransform(37.7,119.1);

	this.shape_189 = new cjs.Shape();
	this.shape_189.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIANAAIABABIAAAHQAHgJANAAIADAAIADAAIgCAJIgCAAIgDgBQgIAAgEACIgHAFIAAAwg");
	this.shape_189.setTransform(31.6,119);

	this.shape_190 = new cjs.Shape();
	this.shape_190.graphics.f("#000000").s().p("AgZAfIAAgGIAkgwIgjAAIAAgIIAxAAIAAAHIgkAwIAlAAIAAAHg");
	this.shape_190.setTransform(266.6,103.9);

	this.shape_191 = new cjs.Shape();
	this.shape_191.graphics.f("#000000").s().p("AgXAcQgFgEAAgJQAAgKAFgFQAHgDANAAQAKAAAIADIAAgJQgBgIgEgDQgEgEgJAAQgKAAgKADIAAgIIAKgDIALAAQAOAAAGAGQAHAFAAANIAAAeIAAACIABABIADABIgBAFIgKAAIgBAAIAAgBIgBgFQgFAIgPAAQgMAAgHgFgAgOAGQgEACAAAHQAAAFADADQAEACAJABQAIgBAEgDQAEgDABgHIAAgGQgIgDgIAAQgJAAgEADg");
	this.shape_191.setTransform(259.6,103.9);

	this.shape_192 = new cjs.Shape();
	this.shape_192.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIANAAIACABIAAAHQAGgJANAAIADAAIADAAIgCAJIgCAAIgDgBQgIAAgEACIgGAFIAAAwg");
	this.shape_192.setTransform(253.5,103.8);

	this.shape_193 = new cjs.Shape();
	this.shape_193.graphics.f("#000000").s().p("AgXAaQgHgIAAgSQAAgQAHgJQAIgHAPAAQARAAAHAHQAHAJAAAQQAAASgHAIQgHAHgRAAQgPAAgIgHgAgPgSQgEAGAAAMQAAANAEAGQAGAGAJAAQALAAAFgGQAEgGAAgNQAAgMgEgGQgFgGgLAAQgJAAgGAGg");
	this.shape_193.setTransform(246.9,103.9);

	this.shape_194 = new cjs.Shape();
	this.shape_194.graphics.f("#000000").s().p("AAnAgQgBAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgDgEgJAAIgKABQgFACgDACIABAFIAAAFIAAAoIgJAAIAAgoQAAgIgDgDQgEgEgIAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgDgCIABgFIANAAIABABIAAAEIAIgFQAFgBAHAAQAHAAAEACQAFACACAEQAEgEAGgCQAFgCAHAAQAMAAAGAFQAFAGABAMIAAAfIAAACIAAAAIAEACIgBAFg");
	this.shape_194.setTransform(223.8,103.8);

	this.shape_195 = new cjs.Shape();
	this.shape_195.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgEQAEgDAGgDQAFgBAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKADIgLAAQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_195.setTransform(214.3,103.9);

	this.shape_196 = new cjs.Shape();
	this.shape_196.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBgBIgDgBIABgGIAMAAIABACIAAA8gAgDggQgBgCAAgDQAAgDABgCQACgBACAAQAEAAACABQABACAAADQAAAGgHAAQgCAAgCgBg");
	this.shape_196.setTransform(208.8,102.6);

	this.shape_197 = new cjs.Shape();
	this.shape_197.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_197.setTransform(203.4,103.8);

	this.shape_198 = new cjs.Shape();
	this.shape_198.graphics.f("#000000").s().p("AgUAaQgIgIAAgSQAAgIACgGQACgHADgEQAEgDAGgDQAFgBAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKADIgLAAQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_198.setTransform(195.9,103.9);

	this.shape_199 = new cjs.Shape();
	this.shape_199.graphics.f("#000000").s().p("AgZAsIAAgGIAkgwIgjAAIAAgIIAxAAIAAAHIgkAwIAlAAIAAAHgAgEggQgBgCgBgDQABgDABgCQABgBADAAQAEAAABABQACACAAADQAAADgCACQgBABgEAAQgDAAgBgBg");
	this.shape_199.setTransform(189,102.6);

	this.shape_200 = new cjs.Shape();
	this.shape_200.graphics.f("#000000").s().p("AgXAcQgFgEgBgJQABgKAFgFQAHgDANAAQAKAAAHADIAAgJQAAgIgDgDQgFgEgJAAQgKAAgKADIAAgIIAKgDIALAAQAOAAAGAGQAHAFgBANIAAAeIABACIABABIADABIAAAFIgLAAIgBAAIAAgBIgBgFQgFAIgPAAQgMAAgHgFgAgOAGQgEACAAAHQAAAFADADQAFACAHABQAJgBAFgDQADgDAAgHIAAgGQgGgDgJAAQgJAAgEADg");
	this.shape_200.setTransform(182,103.9);

	this.shape_201 = new cjs.Shape();
	this.shape_201.graphics.f("#000000").s().p("AgXAeIAAgJQALAEALAAQAHAAAEgDQAEgDAAgFQAAgEgDgCIgHgEIgIgCIgJgCQgFgBgCgEQgDgDAAgGQAAgKAGgDQAHgFALAAIAKAAIAKADIgBAHQgJgCgKAAQgGAAgEACQgEADAAAFQAAAEADACQADABAEABIAIACIAKAEQADABADADQADAEAAAGQAAAKgHAEQgGAFgLAAQgMAAgLgDg");
	this.shape_201.setTransform(175.1,103.9);

	this.shape_202 = new cjs.Shape();
	this.shape_202.graphics.f("#000000").s().p("AgWAaQgIgIAAgSQAAgQAIgJQAGgHAQAAQARAAAHAHQAHAJAAAQQAAASgHAIQgHAHgRAAQgQAAgGgHgAgPgSQgEAGAAAMQAAANAEAGQAGAGAJAAQALAAAFgGQAEgGAAgNQAAgMgEgGQgFgGgLAAQgJAAgGAGg");
	this.shape_202.setTransform(168,103.9);

	this.shape_203 = new cjs.Shape();
	this.shape_203.graphics.f("#000000").s().p("AgaAuIAAhRIAAgBIgBAAIgEgCIABgFIAOAAIABABIAAAFQACgEAGgCQAEgCAHAAQAPAAAGAIQAHAIAAARQAAARgHAIQgGAHgPAAQgHAAgEgCIgIgFIAAAhgAgLggQgFAEABAJIAAAZIAHAEQAFACAFAAQALAAAEgGQAEgGAAgMQAAgNgEgGQgEgGgLAAQgJAAgEAFg");
	this.shape_203.setTransform(160.2,105.2);

	this.shape_204 = new cjs.Shape();
	this.shape_204.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IAMAAIATAyIATgyIAKAAIgbBHIgDAJIgFAHIgGADIgIABIgDAAg");
	this.shape_204.setTransform(152.9,105.3);

	this.shape_205 = new cjs.Shape();
	this.shape_205.graphics.f("#000000").s().p("AAPAfIgEgLIgFgNIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFANIgEALIgMAAIgFgNIgGgRIgGgQIgFgQIALAAIACAHIACAIIACAHIACAHIAFAMIAEAMIAAAAIAEgMIAFgMIACgHIACgHIACgHIABgGIAJAAIABAGIACAHIADAHIABAHIAFAMIAEAMIAFgMIAEgMIACgHIACgHIADgIIABgHIALAAIgFAQIgGAQIgGARIgFANg");
	this.shape_205.setTransform(144.1,103.9);

	this.shape_206 = new cjs.Shape();
	this.shape_206.graphics.f("#000000").s().p("AAmAgQAAAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgEgEgHAAIgLABQgFACgCACIAAAFIABAFIAAAoIgKAAIAAgoQAAgIgEgDQgDgEgIAAIgFABIgFABIgFABIgCACIAAAyIgKAAIAAg1IAAgBIgBgBIgFgCIABgFIAOAAIABABIAAAEIAIgFQAFgBAGAAQAIAAAFACQAEACACAEQAEgEAFgCQAGgCAIAAQAMAAAFAFQAGAGgBAMIAAAfIAAACIACAAIAEACIgBAFg");
	this.shape_206.setTransform(119.4,103.8);

	this.shape_207 = new cjs.Shape();
	this.shape_207.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IAMAAIAUAyIASgyIAKAAIgbBHIgDAJIgFAHIgGADIgJABIgCAAg");
	this.shape_207.setTransform(110,105.3);

	this.shape_208 = new cjs.Shape();
	this.shape_208.graphics.f("#000000").s().p("AAPAfIgEgLIgFgNIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFANIgEALIgMAAIgFgNIgGgRIgGgQIgFgQIALAAIACAHIACAIIACAHIACAHIAFAMIAEAMIAAAAIAEgMIAFgMIACgHIACgHIACgHIABgGIAJAAIABAGIACAHIADAHIABAHIAFAMIAEAMIAFgMIAEgMIACgHIACgHIADgIIABgHIALAAIgFAQIgGAQIgGARIgFANg");
	this.shape_208.setTransform(101.2,103.9);

	this.shape_209 = new cjs.Shape();
	this.shape_209.graphics.f("#000000").s().p("AgXAaQgHgIAAgSQAAgQAHgJQAIgHAPAAQAQAAAIAHQAHAJAAAQQAAASgHAIQgIAHgQAAQgPAAgIgHgAgPgSQgEAGgBAMQABANAEAGQAFAGAKAAQALAAAEgGQAGgGgBgNQABgMgGgGQgEgGgLAAQgKAAgFAGg");
	this.shape_209.setTransform(92,103.9);

	this.shape_210 = new cjs.Shape();
	this.shape_210.graphics.f("#000000").s().p("AAQAuIgVgdIgHAHIAAAWIgKAAIAAhSIAAgBIgBgBIgEgBIABgGIANAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAA3IAbgcIAMAAIgaAbIAbAjg");
	this.shape_210.setTransform(84.9,102.5);

	this.shape_211 = new cjs.Shape();
	this.shape_211.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_211.setTransform(78.8,103);

	this.shape_212 = new cjs.Shape();
	this.shape_212.graphics.f("#000000").s().p("AgXAcQgFgEAAgJQAAgKAFgFQAHgDANAAQAKAAAHADIAAgJQAAgIgEgDQgEgEgJAAQgKAAgKADIAAgIIAKgDIALAAQAOAAAGAGQAHAFAAANIAAAeIAAACIABABIADABIgBAFIgKAAIgBAAIAAgBIgBgFQgFAIgPAAQgMAAgHgFgAgOAGQgEACAAAHQAAAFADADQAEACAIABQAJgBAEgDQAEgDAAgHIAAgGQgGgDgJAAQgJAAgEADg");
	this.shape_212.setTransform(72.8,103.9);

	this.shape_213 = new cjs.Shape();
	this.shape_213.graphics.f("#000000").s().p("AgYAnQgHgIAAgSQAAgQAHgIQAGgHAPAAQAGgBAFACQAFACADAFIAAgjIALAAIAABRIAAABIABABIAEABIgBAGIgLAAIgBAAIgBgBIAAgHQgEAFgFACQgGADgGgBQgPAAgGgHgAgQgFQgEAFgBANQABANAEAHQAEAFALAAQAGAAAEgCIAHgEIAAgZQABgJgFgEQgEgFgJABQgLAAgEAFg");
	this.shape_213.setTransform(65.2,102.6);

	this.shape_214 = new cjs.Shape();
	this.shape_214.graphics.f("#000000").s().p("AgXAaQgHgIAAgSQAAgQAHgJQAIgHAPAAQAQAAAIAHQAHAJAAAQQAAASgHAIQgIAHgQAAQgPAAgIgHgAgPgSQgEAGgBAMQABANAEAGQAFAGAKAAQALAAAEgGQAGgGgBgNQABgMgGgGQgEgGgLAAQgKAAgFAGg");
	this.shape_214.setTransform(57.4,103.9);

	this.shape_215 = new cjs.Shape();
	this.shape_215.graphics.f("#000000").s().p("AgYAnQgHgIAAgSQAAgQAHgIQAGgHAOAAQAHgBAFACQAFACADAFIAAgjIALAAIAABRIAAABIABABIAEABIgCAGIgKAAIgBAAIAAgBIgBgHQgDAFgGACQgFADgIgBQgOAAgGgHgAgQgFQgEAFgBANQABANAEAHQAFAFAKAAQAFAAAFgCIAHgEIAAgZQABgJgFgEQgEgFgJABQgKAAgFAFg");
	this.shape_215.setTransform(49.6,102.6);

	this.shape_216 = new cjs.Shape();
	this.shape_216.graphics.f("#000000").s().p("AgCAsIAAg1IgBgBIgBgBIgDgBIABgGIAMAAIABACIAAA8gAgDggQgBgCAAgDQAAgDABgCQACgBACAAQAEAAACABQABACAAADQAAAGgHAAQgCAAgCgBg");
	this.shape_216.setTransform(30.4,102.6);

	this.shape_217 = new cjs.Shape();
	this.shape_217.graphics.f("#000000").s().p("AgNArQgHgDgDgFQgFgGgBgIQgCgJAAgMQAAgLACgIQABgJAFgGQADgFAHgDQAFgCAIAAQAIAAAHACQAFADAEAFQAEAGACAJQACAIAAALQAAAMgCAJQgCAIgEAGQgEAFgFADQgHACgIAAQgIAAgFgCgAgJghQgFACgCAEQgCAEgCAHIgBAQIABARQACAHACAEQACAEAFADQAEACAFAAQAGAAAEgCQAEgDADgEQACgEACgHQABgHAAgKQAAgJgBgHQgCgHgCgEQgDgEgEgCQgEgCgGAAQgFAAgEACg");
	this.shape_217.setTransform(265.8,87.5);

	this.shape_218 = new cjs.Shape();
	this.shape_218.graphics.f("#000000").s().p("AgcAqIAAgJQALADALAAQAMABAGgFQAGgEAAgKQAAgJgGgEQgGgFgOAAIgDAAIAAgEIAYgfIgoAAIAAgJIA0AAIAAAIIgZAfQAPABAHAEQAHAHAAALQAAAOgIAHQgJAFgSAAQgLAAgLgCg");
	this.shape_218.setTransform(257.7,87.6);

	this.shape_219 = new cjs.Shape();
	this.shape_219.graphics.f("#000000").s().p("AAHAsIAAhNIgXAKIAAgIIAYgMIAJAAIAABXg");
	this.shape_219.setTransform(249.5,87.5);

	this.shape_220 = new cjs.Shape();
	this.shape_220.graphics.f("#000000").s().p("AgUAaQgIgJAAgRQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKADIgLAAQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_220.setTransform(229.3,88.7);

	this.shape_221 = new cjs.Shape();
	this.shape_221.graphics.f("#000000").s().p("AgKArQgIgDgEgFQgGgGgCgIQgCgJAAgMQAAgLACgIQACgJAGgGQAEgFAIgDQAIgCAKAAQAIAAAIABIAJACIgBAJIgLgDIgMAAQgIAAgFACQgGACgEAEQgEAEgCAHQgCAHABAJQgBAKACAHQACAHAEAEQAEAEAGADQAFACAIAAIANgBIALgDIAAAJQgLADgOAAQgKAAgIgCg");
	this.shape_221.setTransform(221.7,87.5);

	this.shape_222 = new cjs.Shape();
	this.shape_222.graphics.f("#000000").s().p("AgFAsIAAhOIgeAAIAAgJIBHAAIAAAJIgfAAIAABOg");
	this.shape_222.setTransform(213.4,87.5);

	this.shape_223 = new cjs.Shape();
	this.shape_223.graphics.f("#000000").s().p("AAmAgQAAAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgEgEgHAAIgLABQgFACgCACIAAAFIABAFIAAAoIgKAAIAAgoQAAgIgEgDQgDgEgHAAIgGABIgFABIgFABIgCACIAAAyIgKAAIAAg1IAAgBIgBgBIgFgCIABgFIAOAAIABABIAAAEIAIgFQAFgBAGAAQAJAAADACQAFACACAEQAEgEAFgCQAGgCAIAAQAMAAAFAFQAGAGgBAMIAAAfIAAACIABAAIAFACIgBAFg");
	this.shape_223.setTransform(190.4,88.6);

	this.shape_224 = new cjs.Shape();
	this.shape_224.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IALAAIAVAyIASgyIALAAIgcBHIgDAJIgFAHIgGADIgJABIgCAAg");
	this.shape_224.setTransform(181,90.1);

	this.shape_225 = new cjs.Shape();
	this.shape_225.graphics.f("#000000").s().p("AAPAgIgEgMIgFgNIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFANIgEAMIgMAAIgFgOIgGgRIgGgQIgFgPIALAAIACAGIACAHIACAIIACAHIAFAMIAEAMIAAAAIAEgMIAFgMIACgHIACgHIACgHIABgGIAJAAIABAGIACAHIADAHIABAHIAFAMIAEAMIAFgMIAEgMIACgHIACgIIADgHIABgGIALAAIgFAPIgGAQIgGARIgFAOg");
	this.shape_225.setTransform(172.2,88.7);

	this.shape_226 = new cjs.Shape();
	this.shape_226.graphics.f("#000000").s().p("AgXAaQgHgJAAgRQAAgRAHgIQAIgHAPAAQAQAAAIAHQAHAIAAARQAAARgHAJQgIAHgQAAQgPAAgIgHgAgOgSQgFAGgBAMQABANAFAGQAEAGAKAAQALAAAEgGQAGgGAAgNQAAgMgGgGQgEgGgLAAQgKAAgEAGg");
	this.shape_226.setTransform(163,88.7);

	this.shape_227 = new cjs.Shape();
	this.shape_227.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_227.setTransform(155.3,88.6);

	this.shape_228 = new cjs.Shape();
	this.shape_228.graphics.f("#000000").s().p("AgVAuIgDAAIAAgJIACABIACAAIAFgBIAEgCIADgEIADgGIADgHIgbg/IAKAAIAUAyIASgyIAMAAIgbBHIgEAJIgEAHIgHADIgIABIgCAAg");
	this.shape_228.setTransform(147.9,90.1);

	this.shape_229 = new cjs.Shape();
	this.shape_229.graphics.f("#000000").s().p("AgZAgIAAgHIAkgwIgjAAIAAgHIAxAAIAAAGIglAwIAmAAIAAAIg");
	this.shape_229.setTransform(141.2,88.7);

	this.shape_230 = new cjs.Shape();
	this.shape_230.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_230.setTransform(133.9,88.6);

	this.shape_231 = new cjs.Shape();
	this.shape_231.graphics.f("#000000").s().p("AgUAaQgIgJAAgRQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKADIgLAAQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_231.setTransform(126.4,88.7);

	this.shape_232 = new cjs.Shape();
	this.shape_232.graphics.f("#000000").s().p("AgIAsQgFgCgEgFIgBAIIgIAAIAAhRIAAgCIgBAAIgEgCIABgFIANAAQABAAAAAAQAAAAAAAAQAAAAAAAAQAAABAAAAIAAAiQADgFAFgCQAFgCAIABQAOAAAHAHQAGAIAAARQAAARgGAIQgHAHgOAAQgIABgFgDgAgLgGQgEAEgBAJIAAAZIAIAEQAEACAGAAQAKAAAFgFQAEgGAAgNQAAgOgEgFQgFgFgKAAQgJgBgEAFg");
	this.shape_232.setTransform(118.9,87.4);

	this.shape_233 = new cjs.Shape();
	this.shape_233.graphics.f("#000000").s().p("AAnAgQgBAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgEgEgHAAIgLABQgFACgCACIAAAFIABAFIAAAoIgKAAIAAgoQAAgIgEgDQgDgEgIAAIgFABIgFABIgFABIgCACIAAAyIgLAAIAAg1IAAgBIAAgBIgFgCIACgFIANAAIABABIAAAEIAIgFQAFgBAGAAQAIAAAFACQAEACACAEQAEgEAGgCQAFgCAHAAQANAAAFAFQAGAGAAAMIAAAfIAAACIABAAIADACIAAAFg");
	this.shape_233.setTransform(96.1,88.6);

	this.shape_234 = new cjs.Shape();
	this.shape_234.graphics.f("#000000").s().p("AgUAaQgIgJAAgRQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgEIAAAJIgKADIgLAAQgRAAgIgHgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgGgJAAQgJAAgFAGg");
	this.shape_234.setTransform(86.6,88.7);

	this.shape_235 = new cjs.Shape();
	this.shape_235.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBgBIgDgCIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgEABgBQACgBACAAQAEAAACABQABABAAAEQAAAGgHAAQgCABgCgCg");
	this.shape_235.setTransform(81.2,87.4);

	this.shape_236 = new cjs.Shape();
	this.shape_236.graphics.f("#000000").s().p("AAQAuIgVgdIgHAHIAAAWIgKAAIAAhSIAAgBIgBgBIgEgBIABgGIANAAQAAAAABAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAA3IAbgcIAMAAIgaAbIAbAjg");
	this.shape_236.setTransform(76.4,87.3);

	this.shape_237 = new cjs.Shape();
	this.shape_237.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBgBIgDgCIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgEABgBQACgBACAAQAEAAACABQABABAAAEQAAAGgHAAQgCABgCgCg");
	this.shape_237.setTransform(71.2,87.4);

	this.shape_238 = new cjs.Shape();
	this.shape_238.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_238.setTransform(65.7,88.6);

	this.shape_239 = new cjs.Shape();
	this.shape_239.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIgBgBIAAgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIAAABIABABIAEABIgBAGg");
	this.shape_239.setTransform(60.1,87.3);

	this.shape_240 = new cjs.Shape();
	this.shape_240.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBgBIgDgCIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgEABgBQACgBACAAQAEAAACABQABABAAAEQAAAGgHAAQgCABgCgCg");
	this.shape_240.setTransform(56.6,87.4);

	this.shape_241 = new cjs.Shape();
	this.shape_241.graphics.f("#000000").s().p("AgXAeIAAgJQALAEALAAQAHAAAEgDQAEgDAAgFQAAgEgDgCIgHgDIgIgDIgJgCQgFgBgCgEQgDgDAAgGQAAgKAGgDQAHgFALAAIAKAAIAKADIgBAIQgJgDgKAAQgGAAgEADQgEACAAAFQAAADADACQADACAEABIAIADIAKADQADABADADQADAEAAAGQAAAKgHAEQgGAFgLAAQgMAAgLgDg");
	this.shape_241.setTransform(51.8,88.7);

	this.shape_242 = new cjs.Shape();
	this.shape_242.graphics.f("#000000").s().p("AgYAgIAAgHIAjgwIgjAAIAAgHIAwAAIAAAGIgkAwIAmAAIAAAIg");
	this.shape_242.setTransform(32.2,88.7);

	this.shape_243 = new cjs.Shape();
	this.shape_243.graphics.f("#000000").s().p("AgWAeIAAgIQAKADALAAQAHAAAEgDQAEgDAAgFQAAgEgCgCIgIgDIgIgDIgJgCQgEgBgDgDQgDgEAAgGQAAgKAGgDQAHgFALAAIALABIAKACIgCAIQgJgDgKAAQgHAAgEADQgDACAAAFQAAADADACQADACAEABIAIADIAKACQADACAEADQACADAAAHQAAAKgHAEQgGAFgLAAQgNAAgJgDg");
	this.shape_243.setTransform(266.7,73.5);

	this.shape_244 = new cjs.Shape();
	this.shape_244.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_244.setTransform(259.5,73.4);

	this.shape_245 = new cjs.Shape();
	this.shape_245.graphics.f("#000000").s().p("AgUAZQgIgIAAgRQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_245.setTransform(252,73.5);

	this.shape_246 = new cjs.Shape();
	this.shape_246.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_246.setTransform(245.6,72.6);

	this.shape_247 = new cjs.Shape();
	this.shape_247.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_247.setTransform(239.3,73.4);

	this.shape_248 = new cjs.Shape();
	this.shape_248.graphics.f("#000000").s().p("AgCAsIAAhOQAAgBAAAAQAAAAAAAAQAAgBAAAAQgBAAAAAAIgEgCIABgFIANAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABVg");
	this.shape_248.setTransform(233.3,72.3);

	this.shape_249 = new cjs.Shape();
	this.shape_249.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBgBIgDgCIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgDABgCQACgCACABQAEgBACACQABACAAADQAAAGgHAAQgCABgCgCg");
	this.shape_249.setTransform(220,72.2);

	this.shape_250 = new cjs.Shape();
	this.shape_250.graphics.f("#000000").s().p("AgKA7IgDAAIAAgIIADAAIACAAQAGAAACgDQACgEAAgHIAAg8IAAgBIgBgBIgDgCIABgEIAMAAIABABIAABDQAAAKgFAGQgEAFgJABIgEAAgAACguQgCgCAAgDQAAgDACgCQABgCADABQAEgBACACQACACgBADQABAGgIAAQgDABgBgCg");
	this.shape_250.setTransform(216.1,73.6);

	this.shape_251 = new cjs.Shape();
	this.shape_251.graphics.f("#000000").s().p("AgWAeIAAgIQAKADALAAQAHAAAEgDQAEgDAAgFQAAgEgCgCIgIgDIgIgDIgJgCQgEgBgDgDQgDgEAAgGQAAgKAGgDQAHgFALAAIALABIAKACIgCAIQgJgDgJAAQgHAAgFADQgDACAAAFQAAADADACQADACAEABIAIADIAKACQAEACADADQACADAAAHQAAAKgHAEQgGAFgLAAQgMAAgKgDg");
	this.shape_251.setTransform(211.9,73.5);

	this.shape_252 = new cjs.Shape();
	this.shape_252.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIAAABIAAAHQAHgJANAAIAEAAIACAAIgBAJIgDAAIgEgBQgGAAgFACIgHAFIAAAwg");
	this.shape_252.setTransform(206.2,73.4);

	this.shape_253 = new cjs.Shape();
	this.shape_253.graphics.f("#000000").s().p("AgUAZQgIgIAAgRQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_253.setTransform(199.9,73.5);

	this.shape_254 = new cjs.Shape();
	this.shape_254.graphics.f("#000000").s().p("AAPAgIgEgMIgFgNIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFANIgEAMIgMAAIgFgOIgGgRIgGgQIgFgPIALAAIACAGIACAHIACAIIACAHIAFAMIAEAMIAAAAIAEgMIAFgMIACgHIACgHIACgGIABgHIAJAAIABAHIACAGIADAHIABAHIAFAMIAEAMIAFgMIAEgMIACgHIACgIIADgHIABgGIALAAIgFAPIgGAQIgGARIgFAOg");
	this.shape_254.setTransform(190.9,73.5);

	this.shape_255 = new cjs.Shape();
	this.shape_255.graphics.f("#000000").s().p("AgVAuIgDAAIAAgJIACABIACAAIAFgBIAEgCIADgEIADgGIADgHIgbg/IAKAAIAUAyIASgyIAMAAIgbBHIgEAJIgEAHIgHADIgJABIgBAAg");
	this.shape_255.setTransform(172.2,74.9);

	this.shape_256 = new cjs.Shape();
	this.shape_256.graphics.f("#000000").s().p("AgZAgIAAgHIAkgvIgjAAIAAgIIAxAAIAAAGIglAwIAmAAIAAAIg");
	this.shape_256.setTransform(165.5,73.5);

	this.shape_257 = new cjs.Shape();
	this.shape_257.graphics.f("#000000").s().p("AgRAZQgHgIAAgRQAAgRAHgIQAHgHAQAAIAMABIAHACIgCAIIgGgCIgKgBQgKAAgFAGQgGAGAAAMQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAJQgIACgKAAQgQAAgHgIg");
	this.shape_257.setTransform(159,73.5);

	this.shape_258 = new cjs.Shape();
	this.shape_258.graphics.f("#000000").s().p("AgWAuIgCAAIAAgJIACABIABAAIAGgBIAEgCIAEgEIACgGIACgHIgbg/IALAAIAVAyIASgyIALAAIgbBHIgEAJIgFAHIgGADIgJABIgCAAg");
	this.shape_258.setTransform(152.2,74.9);

	this.shape_259 = new cjs.Shape();
	this.shape_259.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_259.setTransform(146,72.6);

	this.shape_260 = new cjs.Shape();
	this.shape_260.graphics.f("#000000").s().p("AgWAZQgIgIAAgRQAAgRAIgIQAHgHAPAAQARAAAHAHQAHAIAAARQAAARgHAIQgHAIgRAAQgPAAgHgIgAgOgSQgGAGAAAMQAAANAGAGQAEAGAKAAQALAAAFgGQAEgGABgNQgBgMgEgGQgFgGgLAAQgKAAgEAGg");
	this.shape_260.setTransform(139.8,73.5);

	this.shape_261 = new cjs.Shape();
	this.shape_261.graphics.f("#000000").s().p("AgZAnQgGgIAAgRQAAgRAGgIQAHgHAPgBQAGABAFABQAFACADAFIAAgkIALAAIAABSIAAABIABABIAEACIgBAFIgMAAIAAAAIgBgBIAAgHQgEAFgFACQgGACgGAAQgPABgHgIgAgQgEQgFAEABAOQgBAMAFAHQAFAFAKAAQAFAAAFgCIAHgEIAAgZQAAgJgEgEQgEgEgJAAQgKgBgFAHg");
	this.shape_261.setTransform(132,72.2);

	this.shape_262 = new cjs.Shape();
	this.shape_262.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_262.setTransform(114.3,73.4);

	this.shape_263 = new cjs.Shape();
	this.shape_263.graphics.f("#000000").s().p("AgWAZQgIgIAAgRQAAgRAIgIQAGgHAQAAQAQAAAIAHQAHAIAAARQAAARgHAIQgIAIgQAAQgQAAgGgIgAgOgSQgGAGAAAMQAAANAGAGQAEAGAKAAQALAAAEgGQAFgGABgNQgBgMgFgGQgEgGgLAAQgKAAgEAGg");
	this.shape_263.setTransform(106.6,73.5);

	this.shape_264 = new cjs.Shape();
	this.shape_264.graphics.f("#000000").s().p("AgZAnQgGgIAAgRQAAgRAGgIQAHgHAPgBQAGABAFABQAFACADAFIAAgkIALAAIAABSIAAABIABABIAEACIgBAFIgMAAIAAAAIgBgBIAAgHQgDAFgGACQgGACgGAAQgPABgHgIgAgQgEQgEAEAAAOQAAAMAEAHQAEAFALAAQAFAAAFgCIAHgEIAAgZQAAgJgEgEQgEgEgJAAQgLgBgEAHg");
	this.shape_264.setTransform(98.8,72.2);

	this.shape_265 = new cjs.Shape();
	this.shape_265.graphics.f("#000000").s().p("AgZAnQgGgIAAgRQAAgRAGgIQAHgHAPgBQAGABAFABQAFACAEAFIAAgkIAKAAIAABSIAAABIABABIAEACIgCAFIgLAAIAAAAIgBgBIAAgHQgEAFgFACQgGACgGAAQgPABgHgIgAgQgEQgFAEABAOQgBAMAFAHQAFAFAKAAQAFAAAFgCIAIgEIAAgZQgBgJgEgEQgEgEgJAAQgKgBgFAHg");
	this.shape_265.setTransform(91,72.2);

	this.shape_266 = new cjs.Shape();
	this.shape_266.graphics.f("#000000").s().p("AgUAZQgIgIAAgRQAAgIACgGQACgGADgFQAEgDAGgCQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABALAGAGQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_266.setTransform(83.4,73.5);

	this.shape_267 = new cjs.Shape();
	this.shape_267.graphics.f("#000000").s().p("AgFAuIgGgBIAAgIIAHABIAFAAQASAAAAgPIAAgMQgDAFgFABQgFACgHAAQgPABgGgIQgHgIAAgQQAAgSAHgIQAGgHAPgBQAFABAHABQAFACAEADIABgFIAJAAIAABAQgBAJgCAFQgCAFgEADQgEACgFABIgKABIgHAAgAgOgfQgEAFAAAOQAAAMAEAGQAFAFAKAAQAKABADgFQAFgEAAgJIAAgbIgIgDIgKgBQgKgBgFAHg");
	this.shape_267.setTransform(75.8,74.9);

	this.shape_268 = new cjs.Shape();
	this.shape_268.graphics.f("#000000").s().p("AgWAcQgGgEgBgJQABgKAGgFQAGgDANAAQAJAAAIACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgJADIAAgIIAJgCIALgBQAOAAAGAFQAHAGgBANIAAAfIABABIAAABIAFABIgBAGIgLAAIgBAAIgBgBIAAgGQgGAIgOAAQgNAAgFgFgAgOAGQgEACAAAHQAAAFAEADQAEACAHAAQAJAAAFgDQADgDAAgHIAAgGQgGgDgJAAQgJAAgEADg");
	this.shape_268.setTransform(68.5,73.5);

	this.shape_269 = new cjs.Shape();
	this.shape_269.graphics.f("#000000").s().p("AAnAgQgBAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgDgEgIAAIgLABQgFACgDACIABAFIAAAFIAAAoIgJAAIAAgoQAAgIgDgDQgEgEgIAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgDgCIABgFIANAAIABABIAAAEIAIgFQAFgBAHAAQAIAAAEACQAEACACAEQAEgEAGgCQAGgCAGAAQAMAAAGAFQAFAGABAMIAAAfIAAACIABAAIADACIgBAFg");
	this.shape_269.setTransform(59,73.4);

	this.shape_270 = new cjs.Shape();
	this.shape_270.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_270.setTransform(49.2,73.4);

	this.shape_271 = new cjs.Shape();
	this.shape_271.graphics.f("#000000").s().p("AgSAaQgGgGAAgNIAAgeIgBgBIgEgCIABgFIANAAIABABIAAAkQAAAKAEAEQAEAEAJAAQAJAAAEgEQAEgEAAgKIAAglIAKAAIAAAmQAAANgGAGQgHAGgOAAQgOAAgHgGg");
	this.shape_271.setTransform(41.2,73.6);

	this.shape_272 = new cjs.Shape();
	this.shape_272.graphics.f("#000000").s().p("AAYAsIgWgkIgWAAIAAAkIgKAAIAAhOIgBgCIgEgCIABgFIAnAAQAQAAAGAGQAHAGAAAOQAAAMgFAFQgFAGgLABIAXAlgAgUAAIAYAAQALAAAEgDQAEgEAAgKQAAgJgEgEQgEgEgLAAIgYAAg");
	this.shape_272.setTransform(33.2,72.3);

	this.shape_273 = new cjs.Shape();
	this.shape_273.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAHQAGgJANAAIAEAAIACAAIgBAJIgDAAIgEgBQgGAAgFACIgGAFIAAAwg");
	this.shape_273.setTransform(267.4,58.2);

	this.shape_274 = new cjs.Shape();
	this.shape_274.graphics.f("#000000").s().p("AgXAcQgFgEAAgJQAAgKAFgFQAHgDANAAQAJAAAJACIAAgIQAAgIgFgDQgEgEgJAAQgKAAgKADIAAgIIALgCIAKgBQANAAAHAFQAGAGABANIAAAfIAAABIABABIADABIgBAGIgLAAIAAAAIAAgBIgBgGQgFAIgPAAQgMAAgHgFgAgOAGQgEACAAAHQAAAFADADQAFADAIgBQAIAAAEgDQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEADg");
	this.shape_274.setTransform(261.1,58.3);

	this.shape_275 = new cjs.Shape();
	this.shape_275.graphics.f("#000000").s().p("AgJA7IgEAAIAAgIIADAAIACAAQAGAAACgDQACgEAAgGIAAg9IgBgBIAAAAIgDgDIABgEIAMAAIABABIAABEQAAAJgEAGQgFAFgKABIgCAAgAABguQgBgCAAgDQAAgDABgCQACgCAEABQADgBACACQABACAAADQAAAHgGgBQgEAAgCgBg");
	this.shape_275.setTransform(255,58.4);

	this.shape_276 = new cjs.Shape();
	this.shape_276.graphics.f("#000000").s().p("AgZAnQgGgIAAgRQAAgRAGgIQAHgHAOgBQAHABAFABQAFACAEAEIAAgjIAKAAIAABSIAAABIABABIAEACIgCAFIgLAAIAAAAIAAgBIgBgHQgDAFgGACQgFACgIAAQgOABgHgIgAgQgEQgFAEAAAOQAAAMAFAHQAFAFAKABQAGgBAEgCIAIgEIAAgaQAAgIgFgEQgEgEgJgBQgKAAgFAHg");
	this.shape_276.setTransform(250.1,57);

	this.shape_277 = new cjs.Shape();
	this.shape_277.graphics.f("#000000").s().p("AgXAcQgFgEAAgJQAAgKAFgFQAHgDANAAQAKAAAIACIAAgIQgBgIgEgDQgEgEgJAAQgKAAgKADIAAgIIAKgCIALgBQAOAAAGAFQAHAGAAANIAAAfIAAABIABABIADABIgBAGIgKAAIgBAAIAAgBIgBgGQgFAIgPAAQgMAAgHgFgAgOAGQgEACAAAHQAAAFADADQAFADAIgBQAIAAAEgDQAEgDABgHIAAgGQgIgDgIAAQgJAAgEADg");
	this.shape_277.setTransform(242.5,58.3);

	this.shape_278 = new cjs.Shape();
	this.shape_278.graphics.f("#000000").s().p("AAXAsIghgoIgIAIIAAAgIgLAAIAAhOIgBgCIgEgCIABgFIAOAAQAAAAAAAAQAAAAABABQAAAAAAAAQAAAAAAABIAAApIAogrIAMAAIglApIAmAug");
	this.shape_278.setTransform(234.8,57.1);

	this.shape_279 = new cjs.Shape();
	this.shape_279.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_279.setTransform(208.6,57.4);

	this.shape_280 = new cjs.Shape();
	this.shape_280.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIgBgBIAAgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIAAABIABABIAEABIgBAGg");
	this.shape_280.setTransform(204.4,56.9);

	this.shape_281 = new cjs.Shape();
	this.shape_281.graphics.f("#000000").s().p("AgSAaQgGgGAAgNIAAgeIgBgBIgEgCIABgFIANAAIABABIAAAkQAAAKAEAEQAEAEAJAAQAJAAAEgEQAEgEAAgKIAAglIAKAAIAAAmQAAANgGAGQgHAGgOAAQgOAAgHgGg");
	this.shape_281.setTransform(198.7,58.4);

	this.shape_282 = new cjs.Shape();
	this.shape_282.graphics.f("#000000").s().p("AgXAcQgFgEgBgJQABgKAFgFQAHgDANAAQAKAAAHACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgKADIAAgIIAKgCIALgBQAOAAAGAFQAHAGgBANIAAAfIABABIABABIADABIAAAGIgLAAIgBAAIAAgBIgBgGQgFAIgPAAQgMAAgHgFgAgOAGQgEACAAAHQAAAFADADQAFADAHgBQAJAAAFgDQADgDAAgHIAAgGQgGgDgJAAQgJAAgEADg");
	this.shape_282.setTransform(191.5,58.3);

	this.shape_283 = new cjs.Shape();
	this.shape_283.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_283.setTransform(183.9,58.2);

	this.shape_284 = new cjs.Shape();
	this.shape_284.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgFQAEgEAGgBQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_284.setTransform(176.4,58.3);

	this.shape_285 = new cjs.Shape();
	this.shape_285.graphics.f("#000000").s().p("AAYAsIgWgkIgWAAIAAAkIgKAAIAAhOIgBgCIgEgCIABgFIAmAAQAQAAAIAGQAGAGAAAOQAAAMgFAFQgFAGgMABIAYAlgAgUAAIAXAAQAMAAAEgDQAFgEgBgKQABgJgFgEQgEgEgMAAIgXAAg");
	this.shape_285.setTransform(168.4,57.1);

	this.shape_286 = new cjs.Shape();
	this.shape_286.graphics.f("#000000").s().p("AgJA7IgEAAIAAgIIADAAIACAAQAGAAACgDQACgEAAgGIAAg9IgBgBIAAAAIgDgDIABgEIAMAAIABABIAABEQAAAJgEAGQgFAFgKABIgCAAgAABguQgBgCAAgDQAAgDABgCQACgCAEABQADgBACACQABACAAADQAAAHgGgBQgEAAgCgBg");
	this.shape_286.setTransform(142.5,58.4);

	this.shape_287 = new cjs.Shape();
	this.shape_287.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgFQAEgEAGgBQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_287.setTransform(137.9,58.3);

	this.shape_288 = new cjs.Shape();
	this.shape_288.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_288.setTransform(130.4,58.2);

	this.shape_289 = new cjs.Shape();
	this.shape_289.graphics.f("#000000").s().p("AgWAcQgHgEAAgJQAAgKAHgFQAGgDANAAQAKAAAHACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgJADIAAgIIAKgCIAKgBQAOAAAGAFQAGAGAAANIAAAfIABABIAAABIAFABIgBAGIgMAAIAAAAIgBgBIAAgGQgFAIgPAAQgMAAgGgFgAgOAGQgEACAAAHQAAAFAEADQAEADAHgBQAJAAAFgDQADgDAAgHIAAgGQgHgDgIAAQgJAAgEADg");
	this.shape_289.setTransform(122.9,58.3);

	this.shape_290 = new cjs.Shape();
	this.shape_290.graphics.f("#000000").s().p("AAPAgIgEgLIgFgOIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFAOIgEALIgMAAIgFgOIgGgRIgGgPIgFgQIALAAIACAGIACAHIACAIIACAHIAFAMIAEAMIAAAAIAEgMIAFgMIACgHIACgHIACgGIABgHIAJAAIABAHIACAGIADAHIABAHIAFAMIAEAMIAFgMIAEgMIACgHIACgIIADgHIABgGIALAAIgFAQIgGAPIgGARIgFAOg");
	this.shape_290.setTransform(113.8,58.3);

	this.shape_291 = new cjs.Shape();
	this.shape_291.graphics.f("#000000").s().p("AgWAZQgIgHAAgSQAAgRAIgIQAGgHAQAAQARAAAHAHQAHAIAAARQAAASgHAHQgHAIgRAAQgQAAgGgIgAgPgSQgEAFAAANQAAANAEAGQAGAGAJAAQALAAAFgGQAEgGAAgNQAAgNgEgFQgFgGgLAAQgJAAgGAGg");
	this.shape_291.setTransform(104.6,58.3);

	this.shape_292 = new cjs.Shape();
	this.shape_292.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_292.setTransform(97.9,57.4);

	this.shape_293 = new cjs.Shape();
	this.shape_293.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBAAIgDgDIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgDABgCQACgCACABQAEgBACACQABACAAADQAAAHgHgBQgCAAgCgBg");
	this.shape_293.setTransform(93.7,57);

	this.shape_294 = new cjs.Shape();
	this.shape_294.graphics.f("#000000").s().p("AAmAgQAAAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAIAAgnQAAgIgDgDQgDgEgJAAIgKABQgFACgDACIABAFIAAAFIAAAoIgJAAIAAgoQAAgIgDgDQgEgEgHAAIgGABIgFABIgFABIgCACIAAAyIgKAAIAAg1IAAgBIgCgBIgEgCIABgFIAOAAIABABIAAAEIAIgFQAFgBAHAAQAHAAAEACQAFACACAEQAEgEAFgCQAHgCAHAAQALAAAGAFQAFAGAAAMIAAAfIAAACIABAAIAFACIgBAFg");
	this.shape_294.setTransform(86.3,58.2);

	this.shape_295 = new cjs.Shape();
	this.shape_295.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBAAIgDgDIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgDABgCQACgCACABQAEgBACACQABACAAADQAAAHgHgBQgCAAgCgBg");
	this.shape_295.setTransform(78.5,57);

	this.shape_296 = new cjs.Shape();
	this.shape_296.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIgBgBIAAgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIABABIABABIADABIgBAGg");
	this.shape_296.setTransform(75.2,56.9);

	this.shape_297 = new cjs.Shape();
	this.shape_297.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBAAIgDgDIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgDABgCQACgCACABQAEgBACACQABACAAADQAAAHgHgBQgCAAgCgBg");
	this.shape_297.setTransform(52.6,57);

	this.shape_298 = new cjs.Shape();
	this.shape_298.graphics.f("#000000").s().p("AgCAtIAAg2IgBgBIgBAAIgDgDIABgEIAMAAIABABIAAA9gAgDggQgBgCAAgDQAAgDABgCQACgCACABQAEgBACACQABACAAADQAAAHgHgBQgCAAgCgBg");
	this.shape_298.setTransform(49.2,57);

	this.shape_299 = new cjs.Shape();
	this.shape_299.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIANAAIABABIAAAHQAHgJANAAIAEAAIACAAIgBAJIgDAAIgEgBQgGAAgFACIgHAFIAAAwg");
	this.shape_299.setTransform(45.2,58.2);

	this.shape_300 = new cjs.Shape();
	this.shape_300.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgFQAEgEAGgBQAFgCAHAAQAPAAAGAHQAHAIAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKADIgLAAQgRAAgIgIgAgNgTQgEAEgBAMIAkAAQAAgMgEgEQgEgFgJgBQgJABgFAFg");
	this.shape_300.setTransform(38.9,58.3);

	this.shape_301 = new cjs.Shape();
	this.shape_301.graphics.f("#000000").s().p("AgXAeIAAgIQALADALAAQAIAAADgDQAEgDAAgFQAAgEgCgDIgIgCIgIgCIgJgDQgFgBgCgDQgDgEAAgGQAAgKAHgEQAGgEALAAIALABIAKACIgCAIQgJgDgJAAQgHAAgFADQgDACAAAFQAAADADACQACACAFACIAIACIAJACQAFABADAEQACADAAAHQAAAKgGAEQgHAFgLAAQgMAAgLgDg");
	this.shape_301.setTransform(32.1,58.3);

	this.shape_302 = new cjs.Shape();
	this.shape_302.graphics.f("#000000").s().p("AgXAdQgFgFgBgKQABgJAFgFQAHgDANAAQAJAAAIACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgKADIAAgJIAKgBIALgBQAOAAAGAFQAHAGgBANIAAAfIABABIAAAAIAFACIgBAGIgLAAIgBAAIgBgBIAAgGQgGAIgOAAQgNAAgGgEgAgOAFQgEADAAAGQAAAGAEADQAEADAHgBQAJABAFgEQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_302.setTransform(266.1,43.1);

	this.shape_303 = new cjs.Shape();
	this.shape_303.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAhQIgBgBIAAgBIgEgBIABgGIAMAAQAAAAAAAAQABAAAAABQAAAAAAAAQAAAAAAABIAABQIABABIABABIADABIgBAGg");
	this.shape_303.setTransform(260.6,41.7);

	this.shape_304 = new cjs.Shape();
	this.shape_304.graphics.f("#000000").s().p("AgYAnQgHgIAAgRQAAgRAHgIQAGgIAPAAQAGABAFABQAFACADAEIAAgjIALAAIAABSIAAACIABAAIAEACIgBAFIgLAAIgBAAIgBgBIAAgGQgDAEgGACQgGADgGAAQgPAAgGgIgAgQgEQgEAEgBAOQABANAEAGQAEAFALABQAGAAAEgDIAHgEIAAgaQABgIgFgEQgEgEgJgBQgLAAgEAHg");
	this.shape_304.setTransform(255.1,41.8);

	this.shape_305 = new cjs.Shape();
	this.shape_305.graphics.f("#000000").s().p("AgDAuQAAAAAAAAQgBAAAAgBQAAAAAAAAQAAAAAAgBIAAgmIgNAGIAAgIIANgFIAAgjIAAgBIgBgBIgEgBIABgGIAMAAQABAAAAAAQAAAAAAABQAAAAAAAAQAAAAAAABIAAAkIANgGIAAAIIgNAGIAAAkIABABIAAABIAEABIgBAGg");
	this.shape_305.setTransform(240.9,41.7);

	this.shape_306 = new cjs.Shape();
	this.shape_306.graphics.f("#000000").s().p("AgYAgIAAgIIAkguIgjAAIAAgIIAvAAIAAAHIgkAvIAlAAIAAAIg");
	this.shape_306.setTransform(235.5,43.1);

	this.shape_307 = new cjs.Shape();
	this.shape_307.graphics.f("#000000").s().p("AgOArQgFgDgEgFQgEgGgDgIQgBgJAAgMQAAgLABgIQADgJAEgGQAEgFAFgDQAGgCAIAAQAJAAAFACQAHADADAFQAFAGABAJQACAIAAALQAAAMgCAJQgBAIgFAGQgDAFgHADQgFACgJAAQgIAAgGgCgAgJghQgEACgDAEQgCAEgCAHIgBAQIABARQACAHACAEQADAEAEADQAEACAFAAQAGAAAEgCQAEgDADgEQACgEACgHQABgHAAgKQAAgJgBgHQgCgHgCgEQgDgEgEgCQgEgCgGAAQgFAAgEACg");
	this.shape_307.setTransform(220.2,41.9);

	this.shape_308 = new cjs.Shape();
	this.shape_308.graphics.f("#000000").s().p("AgOArQgGgDgEgFQgDgGgDgIQgBgJAAgMQAAgLABgIQADgJADgGQAEgFAGgDQAHgCAHAAQAJAAAFACQAGADAFAFQAEAGABAJQACAIAAALQAAAMgCAJQgBAIgEAGQgFAFgGADQgFACgJAAQgHAAgHgCgAgJghQgEACgDAEQgDAEgBAHIgBAQIABARQABAHADAEQADAEAEADQAEACAFAAQAGAAAEgCQAEgDADgEQACgEABgHQACgHAAgKQAAgJgCgHQgBgHgCgEQgDgEgEgCQgEgCgGAAQgFAAgEACg");
	this.shape_308.setTransform(212.3,41.9);

	this.shape_309 = new cjs.Shape();
	this.shape_309.graphics.f("#000000").s().p("AgaArIAAgJIAJACIAKABQAHAAAGgCQAFgCAEgFQADgEABgHQACgHAAgJQgDAGgFACQgGADgIAAQgPAAgIgGQgGgGAAgPQAAgPAHgHQAHgHAQAAQAJAAAFACQAGADAEAEQADAFACAIQACAIAAALQAAAMgCAJQgCAKgFAGQgFAGgHACQgHADgKAAQgKAAgJgCgAgPgfQgFAFAAALQAAAKAEAFQAFAEALAAQAKAAAFgEQAFgEAAgJQAAgXgUAAQgKAAgFAFg");
	this.shape_309.setTransform(204.3,41.9);

	this.shape_310 = new cjs.Shape();
	this.shape_310.graphics.f("#000000").s().p("AgbApIAAgJQAKAFAKAAQANAAAGgFQAGgFAAgJQAAgKgHgEQgFgEgOAAIgIAAIgJABIAAgsIAyAAIAAAIIgoAAIAAAdIALgBQARAAAHAGQAJAFgBAOQAAAOgIAGQgIAHgSAAQgLgBgKgDg");
	this.shape_310.setTransform(188.5,42);

	this.shape_311 = new cjs.Shape();
	this.shape_311.graphics.f("#000000").s().p("AgXAoQgHgGAAgNQAAgKAEgFQAFgFALgBQgKgCgEgFQgEgFAAgJQAAgMAHgGQAHgFAOAAQAQAAAHAFQAGAGAAAMQAAAJgEAFQgEAFgJACQAKABAFAFQAEAFAAAKQAAANgHAGQgHAFgRAAQgPAAgIgFgAgJAFQgEABgCADQgDACgBADIgBAHQAAAJAFADQAFAEAKAAQAMAAAEgEQAFgDAAgJIgBgHQgBgDgCgCQgDgDgEgBIgKgCIgJACgAgOghQgEAEAAAIQAAAIAEAEQAFAEAJABQALgBAEgEQAEgEAAgIQAAgIgEgEQgEgDgLAAQgJAAgFADg");
	this.shape_311.setTransform(180.5,41.9);

	this.shape_312 = new cjs.Shape();
	this.shape_312.graphics.f("#000000").s().p("AgXAdQgFgFAAgKQAAgJAFgFQAHgDANAAQAKAAAHACIAAgIQAAgIgDgDQgFgEgJAAQgKAAgKADIAAgJIAKgBIALgBQAOAAAGAFQAHAGAAANIAAAfIAAABIABAAIADACIgBAGIgKAAIgBAAIAAgBIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGADADQAEADAIgBQAJABAFgEQADgDAAgHIAAgGQgGgDgJAAQgJAAgEACg");
	this.shape_312.setTransform(165,43.1);

	this.shape_313 = new cjs.Shape();
	this.shape_313.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_313.setTransform(157.4,43);

	this.shape_314 = new cjs.Shape();
	this.shape_314.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgEQAEgFAGgBQAFgCAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_314.setTransform(149.9,43.1);

	this.shape_315 = new cjs.Shape();
	this.shape_315.graphics.f("#000000").s().p("AgRAZQgHgHAAgSQAAgQAHgIQAHgIAQAAIAMABIAHABIgCAJIgGgCIgKgBQgKAAgFAGQgGAFAAANQAAANAGAGQAFAGAKAAQAJAAAIgDIAAAJQgIACgKAAQgQAAgHgIg");
	this.shape_315.setTransform(143.2,43.1);

	this.shape_316 = new cjs.Shape();
	this.shape_316.graphics.f("#000000").s().p("AgXAdQgFgFAAgKQAAgJAFgFQAHgDANAAQAJAAAJACIAAgIQAAgIgFgDQgEgEgJAAQgKAAgKADIAAgJIALgBIAKgBQANAAAHAFQAGAGABANIAAAfIAAABIABAAIADACIgBAGIgKAAIgBAAIAAgBIgBgGQgFAIgPAAQgMAAgHgEgAgOAFQgEADAAAGQAAAGADADQAFADAIgBQAIABAEgEQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_316.setTransform(128.3,43.1);

	this.shape_317 = new cjs.Shape();
	this.shape_317.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_317.setTransform(120.7,43);

	this.shape_318 = new cjs.Shape();
	this.shape_318.graphics.f("#000000").s().p("AgWAdQgHgFAAgKQAAgJAHgFQAGgDANAAQAJAAAJACIAAgIQAAgIgFgDQgEgEgJAAQgKAAgJADIAAgJIAKgBIAKgBQAOAAAGAFQAGAGABANIAAAfIAAABIAAAAIAFACIgCAGIgLAAIAAAAIgBgBIAAgGQgGAIgOAAQgNAAgFgEgAgOAFQgEADAAAGQAAAGAEADQADADAJgBQAIABAEgEQAFgDAAgHIAAgGQgIgDgIAAQgJAAgEACg");
	this.shape_318.setTransform(113.2,43.1);

	this.shape_319 = new cjs.Shape();
	this.shape_319.graphics.f("#000000").s().p("AAPAgIgEgLIgFgOIgBgGIgCgFIgCgGIgBgFIAAAAIAAAFIgCAGIgBAFIgCAGIgFAOIgEALIgMAAIgFgOIgGgQIgGgQIgFgQIALAAIACAGIACAHIACAIIACAGIAFANIAEAMIAAAAIAEgMIAFgNIACgGIACgGIACgHIABgHIAJAAIABAHIACAHIADAGIABAGIAFANIAEAMIAFgMIAEgNIACgGIACgIIADgHIABgGIALAAIgFAQIgGAQIgGAQIgFAOg");
	this.shape_319.setTransform(104.1,43.1);

	this.shape_320 = new cjs.Shape();
	this.shape_320.graphics.f("#000000").s().p("AgXAZQgHgHAAgSQAAgQAHgIQAIgIAPAAQARAAAHAIQAHAIAAAQQAAASgHAHQgHAIgRAAQgPAAgIgIgAgPgSQgEAFAAANQAAANAEAGQAGAGAJAAQALAAAFgGQAEgGAAgNQAAgNgEgFQgFgGgLAAQgJAAgGAGg");
	this.shape_320.setTransform(94.9,43.1);

	this.shape_321 = new cjs.Shape();
	this.shape_321.graphics.f("#000000").s().p("AgGAlQgFgFAAgLIAAghIAAgCIgBAAIgHgCIAAgGIAIAAIAAgTIALAAIAAATIAUAAIAAAIIgUAAIAAAjQAAAHACADQADADAGAAIAFgBIAEgBIAAAJIgKABQgLAAgFgFg");
	this.shape_321.setTransform(88.3,42.2);

	this.shape_322 = new cjs.Shape();
	this.shape_322.graphics.f("#000000").s().p("AATAgQgBAAAAAAQAAAAAAAAQgBAAAAgBQAAAAAAAAIAAglQAAgJgDgEQgEgEgJAAIgFABIgFABIgEABIgDACIAAAyIgLAAIAAg1IAAgBIgBgBIgEgCIABgFIAOAAIABABIAAAFQADgDAFgCQAFgCAGAAQANAAAGAFQAGAGAAAOIAAAdIAAACIABAAIAEACIgBAFg");
	this.shape_322.setTransform(82,43);

	this.shape_323 = new cjs.Shape();
	this.shape_323.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgEQAEgFAGgBQAFgCAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_323.setTransform(74.5,43.1);

	this.shape_324 = new cjs.Shape();
	this.shape_324.graphics.f("#000000").s().p("AgZAgIAAgIIAkguIgjAAIAAgIIAxAAIAAAHIgkAvIAlAAIAAAIg");
	this.shape_324.setTransform(67.6,43.1);

	this.shape_325 = new cjs.Shape();
	this.shape_325.graphics.f("#000000").s().p("AgUAZQgIgHAAgSQAAgIACgGQACgGADgEQAEgFAGgBQAFgCAHAAQAPAAAGAIQAHAHAAARIAAABIgBACIguAAQABAMAGAFQAFAFAMAAQAKAAAKgDIAAAIIgKACIgLABQgRAAgIgIgAgNgUQgEAGgBALIAkAAQAAgLgEgGQgEgEgJAAQgJAAgFAEg");
	this.shape_325.setTransform(60.6,43.1);

	this.shape_326 = new cjs.Shape();
	this.shape_326.graphics.f("#000000").s().p("AgPAgIAAg1IAAgBIgBgBIgEgCIABgFIANAAIACABIAAAHQAGgJANAAIADAAIADAAIgCAJIgCAAIgDgBQgIAAgEACIgGAFIAAAwg");
	this.shape_326.setTransform(54.6,43);

	this.shape_327 = new cjs.Shape();
	this.shape_327.graphics.f("#000000").s().p("AgdAsIAAhOIgBgCIgEgCIABgFIAmAAQAQAAAHAHQAHAGAAAOQAAAOgHAGQgIAGgPAAIgYAAIAAAigAgTACIAXAAQALAAAFgEQAEgEAAgKQAAgJgEgFQgFgEgLAAIgXAAg");
	this.shape_327.setTransform(47.6,41.9);

	this.shape_328 = new cjs.Shape();
	this.shape_328.graphics.f("#000000").s().p("AAAAHIgLARIgGgEIAMgQIgUgGIADgHIATAGIAAgUIAHAAIAAAUIATgGIADAHIgUAGIAMAQIgGAEg");
	this.shape_328.setTransform(32.1,40);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_328},{t:this.shape_327},{t:this.shape_326},{t:this.shape_325},{t:this.shape_324},{t:this.shape_323},{t:this.shape_322},{t:this.shape_321},{t:this.shape_320},{t:this.shape_319},{t:this.shape_318},{t:this.shape_317},{t:this.shape_316},{t:this.shape_315},{t:this.shape_314},{t:this.shape_313},{t:this.shape_312},{t:this.shape_311},{t:this.shape_310},{t:this.shape_309},{t:this.shape_308},{t:this.shape_307},{t:this.shape_306},{t:this.shape_305},{t:this.shape_304},{t:this.shape_303},{t:this.shape_302},{t:this.shape_301},{t:this.shape_300},{t:this.shape_299},{t:this.shape_298},{t:this.shape_297},{t:this.shape_296},{t:this.shape_295},{t:this.shape_294},{t:this.shape_293},{t:this.shape_292},{t:this.shape_291},{t:this.shape_290},{t:this.shape_289},{t:this.shape_288},{t:this.shape_287},{t:this.shape_286},{t:this.shape_285},{t:this.shape_284},{t:this.shape_283},{t:this.shape_282},{t:this.shape_281},{t:this.shape_280},{t:this.shape_279},{t:this.shape_278},{t:this.shape_277},{t:this.shape_276},{t:this.shape_275},{t:this.shape_274},{t:this.shape_273},{t:this.shape_272},{t:this.shape_271},{t:this.shape_270},{t:this.shape_269},{t:this.shape_268},{t:this.shape_267},{t:this.shape_266},{t:this.shape_265},{t:this.shape_264},{t:this.shape_263},{t:this.shape_262},{t:this.shape_261},{t:this.shape_260},{t:this.shape_259},{t:this.shape_258},{t:this.shape_257},{t:this.shape_256},{t:this.shape_255},{t:this.shape_254},{t:this.shape_253},{t:this.shape_252},{t:this.shape_251},{t:this.shape_250},{t:this.shape_249},{t:this.shape_248},{t:this.shape_247},{t:this.shape_246},{t:this.shape_245},{t:this.shape_244},{t:this.shape_243},{t:this.shape_242},{t:this.shape_241},{t:this.shape_240},{t:this.shape_239},{t:this.shape_238},{t:this.shape_237},{t:this.shape_236},{t:this.shape_235},{t:this.shape_234},{t:this.shape_233},{t:this.shape_232},{t:this.shape_231},{t:this.shape_230},{t:this.shape_229},{t:this.shape_228},{t:this.shape_227},{t:this.shape_226},{t:this.shape_225},{t:this.shape_224},{t:this.shape_223},{t:this.shape_222},{t:this.shape_221},{t:this.shape_220},{t:this.shape_219},{t:this.shape_218},{t:this.shape_217},{t:this.shape_216},{t:this.shape_215},{t:this.shape_214},{t:this.shape_213},{t:this.shape_212},{t:this.shape_211},{t:this.shape_210},{t:this.shape_209},{t:this.shape_208},{t:this.shape_207},{t:this.shape_206},{t:this.shape_205},{t:this.shape_204},{t:this.shape_203},{t:this.shape_202},{t:this.shape_201},{t:this.shape_200},{t:this.shape_199},{t:this.shape_198},{t:this.shape_197},{t:this.shape_196},{t:this.shape_195},{t:this.shape_194},{t:this.shape_193},{t:this.shape_192},{t:this.shape_191},{t:this.shape_190},{t:this.shape_189},{t:this.shape_188},{t:this.shape_187},{t:this.shape_186},{t:this.shape_185},{t:this.shape_184},{t:this.shape_183},{t:this.shape_182},{t:this.shape_181},{t:this.shape_180},{t:this.shape_179},{t:this.shape_178},{t:this.shape_177},{t:this.shape_176},{t:this.shape_175},{t:this.shape_174},{t:this.shape_173},{t:this.shape_172},{t:this.shape_171},{t:this.shape_170},{t:this.shape_169},{t:this.shape_168},{t:this.shape_167},{t:this.shape_166},{t:this.shape_165},{t:this.shape_164},{t:this.shape_163},{t:this.shape_162},{t:this.shape_161},{t:this.shape_160},{t:this.shape_159},{t:this.shape_158},{t:this.shape_157},{t:this.shape_156},{t:this.shape_155},{t:this.shape_154},{t:this.shape_153},{t:this.shape_152},{t:this.shape_151},{t:this.shape_150},{t:this.shape_149},{t:this.shape_148},{t:this.shape_147},{t:this.shape_146},{t:this.shape_145},{t:this.shape_144},{t:this.shape_143},{t:this.shape_142},{t:this.shape_141},{t:this.shape_140},{t:this.shape_139},{t:this.shape_138},{t:this.shape_137},{t:this.shape_136},{t:this.shape_135},{t:this.shape_134},{t:this.shape_133},{t:this.shape_132},{t:this.shape_131},{t:this.shape_130},{t:this.shape_129},{t:this.shape_128},{t:this.shape_127},{t:this.shape_126},{t:this.shape_125},{t:this.shape_124},{t:this.shape_123},{t:this.shape_122},{t:this.shape_121},{t:this.shape_120},{t:this.shape_119},{t:this.shape_118},{t:this.shape_117},{t:this.shape_116},{t:this.shape_115},{t:this.shape_114},{t:this.shape_113},{t:this.shape_112},{t:this.shape_111},{t:this.shape_110},{t:this.shape_109},{t:this.shape_108},{t:this.shape_107},{t:this.shape_106},{t:this.shape_105},{t:this.shape_104},{t:this.shape_103},{t:this.shape_102},{t:this.shape_101},{t:this.shape_100},{t:this.shape_99},{t:this.shape_98},{t:this.shape_97},{t:this.shape_96},{t:this.shape_95},{t:this.shape_94},{t:this.shape_93},{t:this.shape_92},{t:this.shape_91},{t:this.shape_90},{t:this.shape_89},{t:this.shape_88},{t:this.shape_87},{t:this.shape_86},{t:this.shape_85},{t:this.shape_84},{t:this.shape_83},{t:this.shape_82},{t:this.shape_81},{t:this.shape_80},{t:this.shape_79},{t:this.shape_78},{t:this.shape_77},{t:this.shape_76},{t:this.shape_75},{t:this.shape_74},{t:this.shape_73},{t:this.shape_72},{t:this.shape_71},{t:this.shape_70},{t:this.shape_69},{t:this.shape_68},{t:this.shape_67},{t:this.shape_66},{t:this.shape_65},{t:this.shape_64},{t:this.shape_63},{t:this.shape_62},{t:this.shape_61},{t:this.shape_60},{t:this.shape_59},{t:this.shape_58},{t:this.shape_57},{t:this.shape_56},{t:this.shape_55},{t:this.shape_54},{t:this.shape_53},{t:this.shape_52},{t:this.shape_51},{t:this.shape_50},{t:this.shape_49},{t:this.shape_48},{t:this.shape_47},{t:this.shape_46},{t:this.shape_45},{t:this.shape_44},{t:this.shape_43},{t:this.shape_42},{t:this.shape_41},{t:this.shape_40},{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

	// Layer 3
	this.shape_329 = new cjs.Shape();
	this.shape_329.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape_329.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape_329).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,300,250), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A3bTiMAAAgnDMAu3AAAMAAAAnDg");
	this.shape.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,300,250), null);


(lib.Symbol36 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AizC0IAAlnIFnAAIAABaIkNAAIAAENg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol36, new cjs.Rectangle(-18,-18,36,36), null);


(lib.Symbol10kopia = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["rgba(255,255,255,0)","rgba(255,255,255,0.51)","rgba(255,255,255,0)"],[0,0.51,1],-75.5,0,75.5,0).s().p("AryShMAAAglBIXlAAMAAAAlBg");
	this.shape.setTransform(75.5,118.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10kopia, new cjs.Rectangle(0,0,151,237), null);


(lib.ClipGroup = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AhuB1QgwgwAAhFQAAgqAVglQAUglAjgWIgiBBQgRAigKAZIgBACQgDAHAAADIADAKQAHARAWAsIAhBDIABACIABABIACgBIAAgBQAPgbATgpQAdg5AAgEIAAAAIADgJIgDgLIgdg9QgZg2gJgOQAngYAtAAQAtAAAoAYIAFAKQAaAuAhBDQAEAJAAAHQAAAGgEAKQgSAngpBKIgJARQgmAWgrAAQhEAAgwgxg");
	mask.setTransform(15.9,16.6);

	// Warstwa 3
	this.instance = new lib.Path();
	this.instance.parent = this;
	this.instance.setTransform(0.1,0);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.ClipGroup, new cjs.Rectangle(0.1,0,31.8,33.2), null);


(lib.Symbol14copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A4II1IAAxpMAwRAAAIAARpg");
	this.shape.setTransform(154.5,56.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14copy, new cjs.Rectangle(0,0,309,113), null);


(lib.tx2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 2
	this.instance = new lib.tx2_2();
	this.instance.parent = this;
	this.instance.setTransform(120.5,46.3,1,1,0,0,0,117.5,18.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.tx2, new cjs.Rectangle(10,111,286.2,46.8), null);


(lib.Symbol37 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol36();
	this.instance.parent = this;
	this.instance.setTransform(13,0,0.2,0.2,135);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:14},5).to({x:13},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(7.9,-5.1,10.2,10.2);


(lib.Symbol8kopia = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AskCvIAAldIZJAAIAAFdg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:80.5,y:17.5}).wait(152).to({graphics:null,x:0,y:0}).wait(50));

	// Warstwa 3
	this.instance = new lib.Symbol10kopia();
	this.instance.parent = this;
	this.instance.setTransform(-53,20.1,0.373,0.758,20,0,0,75.5,118.7);
	this.instance.compositeOperation = "lighter";
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({_off:false},0).wait(1).to({regY:118.5,x:-52,y:20},0).wait(1).to({x:-49.1},0).wait(1).to({x:-43.8},0).wait(1).to({x:-35.8},0).wait(1).to({x:-24.5},0).wait(1).to({x:-9.6,y:20.1},0).wait(1).to({x:9.6},0).wait(1).to({x:33.5,y:20.2},0).wait(1).to({x:61.9,y:20.3},0).wait(1).to({x:93.7,y:20.4},0).wait(1).to({x:126.8,y:20.5},0).wait(1).to({x:158.6,y:20.6},0).wait(1).to({x:186.9,y:20.7},0).wait(1).to({x:210.7,y:20.8},0).wait(1).to({x:229.9},0).wait(1).to({x:244.8,y:20.9},0).wait(1).to({x:256},0).wait(1).to({x:264},0).wait(1).to({x:269.2},0).wait(1).to({x:272.1},0).wait(1).to({regY:118.7,x:273,y:21.1},0).to({_off:true},117).wait(50));

	// Layer 2
	this.instance_1 = new lib.Symbol37("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(128.3,17.3,0.8,0.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#333333").s().p("AANA0QgDgDAAgGQAAgEACgEQADgEAFgFIguAAIAAhFIAAgBIgBgBIgFgBIACgHIA+AAIAAALIgqAAIAAAWIAhAAIAAAMIghAAIAAAWIArAAIAAAMIgJAJQgCADAAACQAAABAAABQAAAAAAABQAAAAABAAQAAABAAAAQACABADABIADgBIACAAIAAAJIgEAAIgEABQgIAAgEgDg");
	this.shape.setTransform(121.2,18.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#333333").s().p("AgHAoIAAhDIgaAAIAAgMIBDAAIAAAMIgaAAIAABDg");
	this.shape_1.setTransform(114.1,17.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#333333").s().p("AATAoIgSgeIgOAAIAAAeIgRAAIAAhEIAAgCIgBgBIgFgBIACgHIAnAAQAQAAAHAGQAGAGAAANQABAKgFAFQgEAFgJACIAUAggAgNgBIARAAQAHAAAEgDQAEgDAAgHQAAgHgEgDQgEgDgHAAIgRAAg");
	this.shape_2.setTransform(106.7,17.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#333333").s().p("AgaAoIAAhEIAAgCIgBgBIgFgBIACgHIA+AAIAAAMIgqAAIAAAVIAhAAIAAALIghAAIAAAXIArAAIAAAMg");
	this.shape_3.setTransform(99.1,17.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#333333").s().p("AgZAoIAAhEIAAgCIgCgBIgEgBIABgHIA+AAIAAAMIgpAAIAAAZIAhAAIAAALIghAAIAAAfg");
	this.shape_4.setTransform(92.1,17.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#333333").s().p("AgSAnQgHgCgFgFQgEgFgDgIQgCgIAAgLQAAgKACgIQADgIAEgFQAFgFAHgCQAJgDAJAAQALAAAHADQAIACAFAFQAFAFACAIQADAIAAAKQAAALgDAIQgCAIgFAFQgFAFgIACQgHADgLAAQgJAAgJgDgAgKgbQgFACgDAEQgCADgBAGQgCAFAAAHQAAAIACAFQABAGACADQADAEAFABQAFACAFAAQANAAAFgHQAGgHAAgPQAAgPgGgGQgFgHgNAAQgFAAgFABg");
	this.shape_5.setTransform(84.2,17.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#333333").s().p("AggA1IAAgLIAtg5IgsAAIAAgMIA/AAIAAALIgtA5IAuAAIAAAMgAgIgiIAOgSIAQAAIgSASg");
	this.shape_6.setTransform(73.6,16.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#333333").s().p("AghAoIAAhEIAAgCIgBgBIgFgBIACgHIAjAAQALAAAIACQAIACAFAFQAFAFACAIQADAHAAAKQAAALgDAHQgCAIgFAFQgFAFgIACQgIACgLAAgAgRAcIAOAAQAHAAAFgCQAFgBADgEQADgDACgFQACgGAAgHQAAgHgCgFQgCgFgDgEQgDgDgFgCQgFgBgHAAIgOAAg");
	this.shape_7.setTransform(65.6,17.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#333333").s().p("AARAoIgFgLIgDgLIgDgJIgDgJIgCgIIgBgIIAAAIIgCAIIgDAJIgCAJIgEALIgFALIgSAAIgHgSIgGgWIgGgUIgGgTIAQAAIADANIAFASIACAIIAEAJIACAJIACAHIADgHIADgJIACgJIADgIIAFgRIACgNIAPAAIACANIAFARIADAIIACAJIADAJIADAHIACgHIADgJIADgJIACgIIAEgSIAEgNIAQAAIgFATIgHAUIgGAWIgHASg");
	this.shape_8.setTransform(55.3,17.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#333333").s().p("AAXAoIgCgJIgDgKIgjAAIgCAKIgDAJIgQAAIADgMIAFgPIAGgOIAFgPIAGgNIAFgKIARAAIAFAKIAGANIAGAPIAFAOIAFAPIADAMgAgNAKIAbAAIgOgjQgHARgGASg");
	this.shape_9.setTransform(45.4,17.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#333333").s().p("AATAoIgSgeIgPAAIAAAeIgQAAIAAhEIAAgCIgBgBIgEgBIABgHIAnAAQAQAAAHAGQAGAGABANQAAAKgFAFQgEAFgJACIAUAggAgOgBIARAAQAJAAADgDQAEgDAAgHQAAgHgEgDQgDgDgJAAIgRAAg");
	this.shape_10.setTransform(37.7,17.3);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#333333").s().p("AgdAoIAAhEIAAgCIgBgBIgEgBIABgHIAmAAQAQAAAIAGQAGAGABAOQAAAHgDAFQgBADgDAEQgEADgGABQgGACgIAAIgSAAIAAAcgAgNAAIARAAQAIAAAEgDQAEgDAAgHQAAgIgEgDQgEgDgIAAIgRAAg");
	this.shape_11.setTransform(29.9,17.3);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#333333").s().p("AgdAmIAAgNQANAEAOAAQAJAAAEgCQAEgDAAgGQAAgFgDgCQgDgCgGgCIgKgCQgGgBgFgDQgFgCgEgEQgDgEAAgIQAAgNAJgFQAIgGAQAAIANABQAHABAEACIgCAMIgLgDIgMgBQgIAAgEADQgFACABAHQAAAEADACIAJADIAJADQAGABAGADQAFACADAEQAEAFAAAIQAAAMgIAGQgIAGgQAAQgOAAgOgEg");
	this.shape_12.setTransform(22.8,17.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1}]}).to({state:[]},152).wait(50));

	// Layer 1
	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFCC33").s().p("AskCvIAAldIZJAAIAAFdg");
	this.shape_13.setTransform(80.5,17.5);

	this.timeline.addTween(cjs.Tween.get(this.shape_13).to({_off:true},152).wait(50));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,161,35);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Warstwa 2 — kopia
	this.instance = new lib.Symbol14copy();
	this.instance.parent = this;
	this.instance.setTransform(142.9,-5.5,1.646,1.893,6.5,0,0,154.3,56.4);
	this.instance.alpha = 0.359;
	this.instance.filters = [new cjs.BlurFilter(50, 50, 1)];
	this.instance.cache(-2,-2,313,117);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

	// Warstwa 2
	this.instance_1 = new lib.bgg();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-147,-12);

	this.instance_2 = new lib.Symbol14copy();
	this.instance_2.parent = this;
	this.instance_2.setTransform(187,1.9,1.646,1.893,-4.3,0,0,154.3,56.5);
	this.instance_2.alpha = 0.359;
	this.instance_2.filters = [new cjs.BlurFilter(50, 50, 1)];
	this.instance_2.cache(-2,-2,313,117);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2},{t:this.instance_1}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1copy, new cjs.Rectangle(-147,-165.2,623.8,483.3), null);


// stage content:
(lib.renault_runmageddon_300x250 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// border
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(1,1,1).p("A3bzhMAu3AAAMAAAAnDMgu3AAAg");
	this.shape.setTransform(150,125);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(455));

	// Warstwa 6
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(-102.9,-10.7,1,1,0,0,0,118.4,42.6);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(147).to({_off:false},0).to({x:115.9,alpha:1},20,cjs.Ease.get(1)).wait(71).to({alpha:0},17).wait(200));

	// Warstwa 8
	this.instance_1 = new lib.tx2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-133,-35,1,1,0,0,0,117.5,18.3);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(50).to({_off:false},0).to({x:115,alpha:1},22,cjs.Ease.get(1)).wait(56).to({alpha:0},18).wait(309));

	// Warstwa 5
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(375,100,1,1,0,0,0,375,100);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({alpha:0},9).wait(436).to({alpha:1},9).wait(1));

	// Warstwa 1
	this.instance_3 = new lib.Symbol9();
	this.instance_3.parent = this;
	this.instance_3.setTransform(375,100,1,1,0,0,0,375,100);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(377).to({_off:false},0).to({alpha:1},10).wait(68));

	// logo
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#000000").s().p("AhCErQgJgBgHgIIglg+Qgkg8gVgmQgphKgSgnQgEgJAAgIQAAgGAEgJIAXgsQAWgrAOgaIA4hgIAmg/QAEgGACgBQAEgDAGAAQAJgBA5AAQA6AAAIABIABAAIAIACQADACAFAFIAlA+IA5BiQAZAuAiBDQAEAJAAAGQAAAHgEAKQgTAngoBKIg3BgQgiA4gFAIQgGAIgKABgAhCkmIgIACIgGAHIgmA/Ig3BgIgkBEIgXAtQgEAJAAAEQAAAGAEAJQARAmAqBLIA4BiIAmA+QADAFAEABIAHACQANABA0AAQAyAAAQgBIAHgCQAEgBADgFIAnhAIA3hgQAqhNARgkQAEgIAAgHQAAgFgEgIQgTgogohJIg4hiIgmg+QgFgHgIgBIgBAAQgNgBg1AAQg0AAgOABg");
	this.shape_1.setTransform(195.5,29.9,0.563,0.563);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#000000").s().p("AgDCOIgjhFIgdg9IgDgMQAAgFAEgGQAEgNAYgwIAjhFQACgEABAAQACAAACAEIAjBFQAaA2ACAHQAEAIAAADQAAAEgEAIIgcA9IgjBFQgCAEgCAAQgBAAgCgEgAAAiLIgjBEQgRAjgLAZIgBACQgDAHAAACIADAKQAHASAWAsIAiBCIABACIAAACIABgCIAAAAQAPgbAUgpQAcg5AAgFIABAAIADgKIgDgJIgdg+IgjhEIgBgCg");
	this.shape_2.setTransform(195.5,29.9,0.563,0.563);

	this.instance_4 = new lib.ClipGroup();
	this.instance_4.parent = this;
	this.instance_4.setTransform(200.9,30.6,0.563,0.563,0,0,0,17.3,17.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhCABIAFgDIADgBQAOgBAzAAIA+ABQgDACgIAAIhwAAIgIABIgGAFg");
	this.shape_3.setTransform(195.1,14,0.563,0.563);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.lf(["#202020","#2A2A2A","#383838","#3D3D3D","#5C5C5C","#747474"],[0.02,0.216,0.6,0.969,0.984,1],0,14.9,0,-14.8).s().p("AhUCKQACAAACgEQAGgIAegzQAjg6AVgoQAmhEAVgtQADgGAAgGIALABQAAAHgEAJQgJAXgyBaIg2BfIgnBAQgEAGgFACg");
	this.shape_4.setTransform(204.2,38.3,0.563,0.563);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.lf(["#8D8D8D","#747474","#C0C0C0"],[0,0.988,1],-12.3,4.9,12.1,-4.5).s().p("AhUCUQAAgGAEgKIAWgsIAlhFIA3hgQAhg4AFgHQAFgGAEgCIAEALIgEAEIgjA6QgkA8gVAoQgjA+gYAyQgDAGAAAFg");
	this.shape_5.setTransform(186.8,21.6,0.563,0.563);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.lf(["#595959","#767676","#919191","#A2A2A2","#A8A8A8","#BFBFBF","#C0C0C0"],[0.008,0.024,0.043,0.063,0.075,0.502,1],6.8,2,-6.9,-1.9).s().p("AhJgDIAHgCICFAAIAHABIgEAKIgCgBIhEgBQgzAAgOABIgDABg");
	this.shape_6.setTransform(195.5,13.5,0.563,0.563);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.lf(["#595959","#747474"],[0,1],-12.5,-6.6,12.8,7.2).s().p("ABKCUQAAgEgDgGQgWguglhDQgVgpgjg6QgegzgGgIIgEgEIAEgKQAFACAEAGQAGAHAgA3QAiA8AVAkQAhA9AaA1QAEAIAAAIg");
	this.shape_7.setTransform(204.2,21.6,0.563,0.563);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.lf(["#666666","#494949","#323232","#252525","#202020"],[0.02,0.039,0.063,0.082,0.106],-5.6,-3.1,5.8,3.4).s().p("AhBAHQgHgBgCgCIAFgKIADADIBDABQAyAAAPgCIABAAIADgBIAFAKIgHACg");
	this.shape_8.setTransform(195.4,46.4,0.563,0.563);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.lf(["#666666","#5D5D5D","#505050","#4C4C4C","#767676","#8D8D8D"],[0,0.184,0.584,0.969,0.988,1],0,14.8,0,-14.8).s().p("ABJCNIgmg+Qgjg7gUgmQgphLgSgmQgEgIAAgIIALAAQAAAFADAGQAYAyAiA/QAWApAjA6IAkA6IACADIgEALQgFgDgCgEg");
	this.shape_9.setTransform(186.7,38.3,0.563,0.563);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.lf(["#FFFFFF","#F9F9F9","#E8E8E8","#CCCCCC","#C0C0C0","#747474"],[0,0.216,0.51,0.851,0.969,0.992],0,14.2,0,-14.4).s().p("AgSAWIAGgUQACAAADgDQAkhHAag7QADgIAAgDIASAAQAAADgHAOQgUAug0BhQgtBUgSAeIgCAFQgFAGgCAAQAgg6AZg/g");
	this.shape_10.setTransform(196.2,38,0.563,0.563);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.lf(["rgba(242,242,242,0.8)","rgba(238,238,238,0)"],[0.012,0.11],6.5,2.7,-8.6,-4.1).s().p("AglhBQgGgMAAgFIASAAIACAMQAXA0AoBNQADAEABAAIgGAUQgqhOghhGg");
	this.shape_11.setTransform(193,34.6,0.563,0.563);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.lf(["#C0C0C0","#BCBCBC","#AEAEAE","#979797","#767676","#666666","#303030"],[0.357,0.475,0.608,0.753,0.902,0.965,1],0,8.4,0,-8.2).s().p("AglhBQgGgMAAgFIASAAIACAMQAXA0AoBNQADAEABAAIgGAUQgqhOghhGg");
	this.shape_12.setTransform(193,34.6,0.563,0.563);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.lf(["#747474","#6E6E6E","#5E5E5E","#444444","#1D1D1D","#131313","#000000"],[0,0.149,0.333,0.533,0.749,0.796,0.922],0,8.3,0,-8.3).s().p("AAbBTQAAgEgEgIQgWg0gohNQgCgEgCAAIAHgUQAkBDAmBRQAGANAAAEg");
	this.shape_13.setTransform(198,25.3,0.563,0.563);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.lf(["#303030","#242424","#171717","#131313","#0D0D0D","#010101","#000000"],[0,0.008,0.024,0.039,0.122,0.227,0.251],0,14.2,0,-14.4).s().p("AhLCPQAAgEAGgNQAZg1AwhbQAvhaATgcQADgGADAAQghA+gYA7IgHAUQgBAAgDADQgnBOgXA0QgDAIAAADg");
	this.shape_14.setTransform(194.8,21.8,0.563,0.563);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.lf(["#E6E6E6","#A8A8A8","#303030","#131313","#090909","#0C0C0C","#171717","#292929","#424242","#606060","#666666"],[0.012,0.337,0.694,0.847,0.953,0.965,0.976,0.984,0.992,1,1],0,14.1,0,-14.3).s().p("AiMCPQAAAAAAAAQgBAAAAAAQAAAAAAAAQgBAAAAgBIAEgGQAOgWAphJQAyhXAWguIAMgdQAIgPAEgCQAEgCAdAAIBUAAQAIAAADgDQAAAAABAAQAAAAAAAAQAAAAAAAAQAAAAAAAAQABADgEAJIgNAZQgfBAgvBPQglA/gaArQgSABgwAAg");
	this.shape_15.setTransform(200.1,38.1,0.563,0.563);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.lf(["#E6E6E6","#A8A8A8","#303030","#131313","#090909","#0C0C0C","#171717","#292929","#424242","#606060","#666666"],[0.012,0.337,0.694,0.847,0.949,0.961,0.973,0.98,0.992,1,1],0,14.3,0,-14.1).s().p("AAvCOIgIgNQhJh1g8h1QgKgVgBgGQAAgHALAAIBlABQAKAAAEgEQAAAAABgBQAAAAAAAAQAAAAAAAAQAAABAAAAIAAABQAAAFAHANQAiBHArBPQgZA9geA4QgBAAAAAAQgBAAAAAAQgBgBAAAAQgBAAAAgBg");
	this.shape_16.setTransform(189.1,38,0.563,0.563);

	this.instance_5 = new lib.CompoundPath_6();
	this.instance_5.parent = this;
	this.instance_5.setTransform(182.6,13.6,0.563,0.563);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.lf(["#E6E6E6","#FFFFFF","#929292"],[0,0.686,1],0,28.9,0,-28.8).s().p("AhDEgIgDgDIgDgDIgkg6Qgig6gXgqQgig/gZgyQgDgGAAgFQAAgEADgGQAZgyAig/QAWgoAkg8IAjg6IAEgEIAEgBQAOgCAzAAIBEABIACABIAEAFQAFAHAfAzQAiA7AXApQAlBEAVAtQAEAGAAAEQAAAFgEAGQgVAtglBEQgXApgiA6QgfAzgFAIQgDAEgCAAIgDABIgBAAQgPACgyAAgAgDiNQgpBPgWAzQgEAIAAADIADAMQAXAzApBPQACAEABAAQACAAACgEQAlhHAag7QAEgIAAgEQAAgDgEgIQgWgzgphPQgCgEgCAAQgBAAgCAEg");
	this.shape_17.setTransform(195.5,29.9,0.563,0.563);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#000000").s().p("AgTAZQgIgIAAgRQAAgQAHgIQAHgHAPAAQAOAAAFAHQAHAIAAAQIAAADIgtAAQAAALAGAFQAFAFAMAAQAKAAAKgDIAAAIQgKADgLAAQgRAAgHgHgAgRgDIAjAAQAAgVgQAAQgTAAAAAVg");
	this.shape_18.setTransform(259.6,40.3,0.563,0.563);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#000000").s().p("AgLAtIAAgzIgBgCIgHgCIAAgFIAIAAIAAgIQgBgVAVAAQAHAAAEACIgBAHQgDgBgGAAQgLAAAAAMIAAAJIARAAIAAAIIgRAAIAAA0g");
	this.shape_19.setTransform(256.8,39.5,0.563,0.563);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#000000").s().p("AgCArIAAg0IgBgBIgEgCIABgFIALAAQABAAAAAAQAAAAABAAQAAAAAAABQAAAAAAAAIAAA7gAgEgkQAAgGAGAAQAGAAAAAGQAAAHgGAAQgGAAAAgHg");
	this.shape_20.setTransform(254.4,39.6,0.563,0.563);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#000000").s().p("AgDAtQAAAAAAgBQAAAAAAAAQgBAAAAAAQAAgBAAAAIAAhNIgBgCIgEgCIABgFIAMAAQAAAAAAAAQABAAAAAAQAAAAAAAAQAAABAAAAIAABOIABACIAEABIgBAGg");
	this.shape_21.setTransform(252.7,39.5,0.563,0.563);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#000000").s().p("AgOAgIAAg0IgBgCIgFgCIABgFIAOAAQAAAAAAAAQAAAAAAAAQAAABAAAAQAAAAAAABIAAAGQAHgJAMAAIAHAAIgCAJIgGgBQgKAAgIAHIAAAvg");
	this.shape_22.setTransform(249.2,40.2,0.563,0.563);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#000000").s().p("AgWAZQgHgIAAgRQAAgQAHgIQAHgHAPAAQAQAAAHAHQAHAIAAAQQAAARgHAIQgHAHgQAAQgPAAgHgHgAgOgSQgFAGAAAMQAAANAFAGQAEAFAKAAQALAAAEgFQAFgGAAgNQAAgMgFgGQgEgFgLAAQgKAAgEAFg");
	this.shape_23.setTransform(245.8,40.3,0.563,0.563);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#000000").s().p("AgLAtIAAgzQAAAAAAAAQAAgBgBAAQAAAAAAAAQAAgBgBAAIgHgCIABgFIAIAAIAAgIQAAgVAUAAQAHAAAFACIgCAHIgJgBQgLAAAAAMIAAAJIARAAIAAAIIgRAAIAAA0g");
	this.shape_24.setTransform(242.9,39.5,0.563,0.563);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#000000").s().p("AASAgQAAAAgBgBQAAAAAAAAQAAAAAAAAQAAgBAAAAIAAgkQAAgQgQAAQgLAAgGAEIAAAyIgKAAIAAg0IgBgCIgEgCIABgFIANAAQAAAAABAAQAAAAAAAAQAAABAAAAQAAAAAAABIAAADQAIgGALAAQANgBAFAGQAGAGAAANIAAAdIABACIAEABIgBAGg");
	this.shape_25.setTransform(237.7,40.2,0.563,0.563);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#000000").s().p("AgWAZQgHgIAAgRQAAgQAHgIQAHgHAPAAQAQAAAHAHQAHAIAAAQQAAARgHAIQgHAHgQAAQgPAAgHgHgAgTAAQAAAYATAAQAUAAAAgYQAAgXgUAAQgTAAAAAXg");
	this.shape_26.setTransform(233.7,40.3,0.563,0.563);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#000000").s().p("AgDArIAAg0IgBgBIgDgCIABgFIALAAQABAAAAAAQAAAAAAAAQAAAAAAABQAAAAAAAAIAAA7gAgEgkQAAgGAGAAQAGAAAAAGQAAAHgGAAQgGAAAAgHg");
	this.shape_27.setTransform(230.8,39.6,0.563,0.563);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#000000").s().p("AgWAdIAAgIQAJADAMAAQAOAAAAgKQAAgIgRgDQgSgDgBgNQAAgSAYAAQAKAAAKADIgCAHQgJgCgJAAQgOAAAAAJQABAHARAEQASADABAOQAAASgYAAQgMAAgKgDg");
	this.shape_28.setTransform(228.3,40.3,0.563,0.563);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#000000").s().p("AgVAdIAAgIQAJADALAAQAPAAAAgKQAAgIgSgDQgSgDAAgNQAAgSAXAAQALAAAJADIgCAHQgJgCgIAAQgOAAAAAJQAAAHARAEQASADAAAOQAAASgXAAQgMAAgJgDg");
	this.shape_29.setTransform(225.1,40.3,0.563,0.563);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#000000").s().p("AgcAOQAAgRAZAAQAIAAAJADIAAgJQAAgIgEgDQgEgDgJAAQgKAAgJADIAAgIQAJgDALAAQANAAAHAFQAFAGAAAMIAAAeIACACIAEABIgBAGIgLAAIgBgBIgBgGQgFAIgOAAQgYAAAAgSgAgRAOQAAALAOAAQARAAAAgOIAAgFQgIgDgIAAQgPAAAAALg");
	this.shape_30.setTransform(221.6,40.3,0.563,0.563);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#000000").s().p("AgcArIAAhNIgBgBIgEgBIABgGIAkAAQAQABAHAGQAHAGAAAOQAAAOgHAEQgHAHgQgBIgWAAIAAAigAgSABIAVAAQALAAAFgCQAFgFAAgJQAAgKgFgEQgFgEgLAAIgVAAg");
	this.shape_31.setTransform(217.7,39.6,0.563,0.563);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FFCE00").s().p("AgiEsIAApXIBFAAIAAJXg");
	this.shape_32.setTransform(287,29.9,0.563,0.563);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#000000").s().p("AAsBCIgIgXIhGAAIgJAXIgjAAQAahCAfhBIAqAAQAgBBAaBCgAAaASQgPgjgLgYQgKAYgPAjIAzAAIAAAAg");
	this.shape_33.setTransform(248,29.9,0.563,0.563);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#000000").s().p("AADBDQhEAAAAg0IAAg+QAAgCgCgEIgDgFIABgIIAmAAIAABQQABAMAGAGQAIAIATAAQAVAAAHgIQAHgGAAgMIAAhQIAiAAIAABRQAAA0hEAAg");
	this.shape_34.setTransform(257.3,29.9,0.563,0.563);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#000000").s().p("AAqBCIg1g7QgQgQgIgMIAABXIgfAAIAAhwQAAgCgCgEIgDgFIABgIIAhAAIAwA0QARASANARIAAhXIAgAAIAACDg");
	this.shape_35.setTransform(238.1,29.9,0.563,0.563);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#000000").s().p("AAiBCIgXgqIgqAAIAAAqIgjAAIAAhuQAAgFgCgDIgDgFIABgIIBPAAQAgAAAOALQAMALAAAVQAAAMgGAKQgHALgLAGIAdAxgAgfAAIAuAAQAHAAAEgEQAGgGAAgLQAAgLgGgEQgGgEgPAAIgkAAg");
	this.shape_36.setTransform(219.8,29.9,0.563,0.563);

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#000000").s().p("Ag5BCIAAiDIBxAAIAAAYIhOAAIAAAdIBHAAIAAAXIhHAAIAAAfIBPAAIAAAYg");
	this.shape_37.setTransform(229,29.9,0.563,0.563);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#000000").s().p("Ag2BCIAAiDIAjAAIAABrIBKAAIAAAYg");
	this.shape_38.setTransform(266.6,29.9,0.563,0.563);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#000000").s().p("AgQBCIAAhrIg0AAIAAgYICJAAIAAAYIg0AAIAABrg");
	this.shape_39.setTransform(272.7,29.9,0.563,0.563);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_39},{t:this.shape_38},{t:this.shape_37},{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.instance_5},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.instance_4},{t:this.shape_2},{t:this.shape_1}]}).wait(455));

	// Warstwa 11
	this.instance_6 = new lib.Symbol8kopia("single",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(82.9,250.2,0.821,0.821,0,0,0,0.2,0.3);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(256).to({_off:false},0).to({y:204.7,alpha:1,mode:"synched",loop:false},11,cjs.Ease.get(1)).to({_off:true},152).wait(36));

	// Warstwa 14
	this.instance_7 = new lib.Symbol13();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-103.1,119.9,1,1,0,0,0,118.3,40.5);
	this.instance_7.alpha = 0.051;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(256).to({_off:false},0).to({x:125.9,alpha:1},20,cjs.Ease.get(1)).wait(179));

	// Warstwa 2
	this.instance_8 = new lib.Symbol1copy();
	this.instance_8.parent = this;
	this.instance_8.setTransform(391.2,100.1,1.065,1.065,0,0,0,395,100);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({scaleX:1,scaleY:1,x:407.6,y:100},50,cjs.Ease.get(1)).wait(191).to({regX:395.2,regY:100.1,scaleX:0.84,scaleY:0.84,x:446.2,y:83.1},35,cjs.Ease.get(1)).wait(179));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.2,-55.9,662.5,513.2);
// library properties:
lib.properties = {
	id: '1FAC60F6ECE79748AB556D12FCE28796',
	width: 300,
	height: 250,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/CompoundPath_6.png", id:"CompoundPath_6"},
		{src:"images/Path.png", id:"Path"},
		{src:"images/bgg.png", id:"bgg"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['1FAC60F6ECE79748AB556D12FCE28796'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;