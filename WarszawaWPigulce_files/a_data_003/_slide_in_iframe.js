(function() {
    var init = function(closeButton) {
        var closeButtonListener = function() {
            var iframe = window.frameElement;
            var idToHide = iframe.getAttribute("av-placement-id");
            var placementToHide = window.parent.document.querySelector("#" + idToHide);

            if (placementToHide != null) {
                placementToHide.style.display = "none";
            } else {
                iframe.style.display = "none";
            }

            if (window.parent.avUtils.avSlide != null) {
                window.parent.avUtils.avSlide.detachScrollListener();
            }
        };
        closeButton.addEventListener("click", closeButtonListener);
    };

    var closeButton = document.querySelector(".close");

    if (closeButton == null && document.readyState !== "complete") {
        // IE11 & ME
        window.addEventListener("load", function() {
            closeButton = document.querySelector(".close");
            init(closeButton);
        });
    } else {
        init(closeButton);
    }
})();