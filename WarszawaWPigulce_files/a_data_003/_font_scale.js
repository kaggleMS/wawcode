var onLoadScale = function() {
    var parentNode = document.querySelector("#av-placement-content");
    avUtils.avFonts.createParentObserverToScaleFont(parentNode, ".title, .description");
};

if (window.addEventListener){
    window.addEventListener('load', onLoadScale, false);
} else if (window.attachEvent){
    window.attachEvent('onload', onLoadScale);
}

