from django.conf.urls import url, include
from piano import views
from django.conf import settings
from django.views.static import serve
from photologue.sitemaps import GallerySitemap, PhotoSitemap

from django.contrib import admin
from django.conf.urls.static import static
from django.views.generic import TemplateView
#rzeczy


urlpatterns = [
    url(r"^practice/$", views.practice_page, name="practice_page"),
    url(r"^update/$", views.update_page, name="update_piano_page"),
    url(r"^delete/(\d+)/", views.delete_page, name="delete_piano_page"),
    url(r"^$", views.demo_base, name="piano_page"),
    #url(r"^$", views.piano_page, name="piano_page"),
    url(r"^demo", views.demo_page, name="demo_page"),
    url(r"^dwademo", views.demo2_page, name="demo2_page"),
    url(r"^basetut", views.base_tut_page, name="base_tut_page"),
    url(r"^childtut", views.child_tut_page, name="child_tut_page"),
    url(r"^childdemo", views.demo_child, name="demo_child"),
    url(r"^basedemo", views.demo_base, name="demo_base"),

    url(r"^upload_pic/(\d+)/", views.upload_pic, name ="upload_pic"),
#    url(r'^photologue/', include('photologue.urls', namespace='photologue')),
    url(r'^homepage/$', TemplateView.as_view(template_name="homepage.html"), name='_photologue_homepage'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

#print 

#urlpatterns += [
#]


# ogarnac to dalej
# print settings.MEDIA_ROOT?
# ciezki temat ,warty dluzszj rozkminy
