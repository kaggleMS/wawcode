# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from blog.models import BlogPost, OFirmie, Opis
from blog.forms import BlogPostForm
import datetime
import urllib2
import json
import os.path
from collections import defaultdict
import string
import requests
from bs4 import BeautifulSoup
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt    
import copy



def souping():
    url = "https://warszawawpigulce.pl/z-ostatniej-chwili-piec-stacji-metra-zamknietych-utrudnienia-w-i-linii-metra/"
    result = requests.get(url)
    c = result.content
    # Now that we have the data, let's process it
    soup = BeautifulSoup(c,'html.parser')
    return

import re
def get_zamkniete():
    #url2 = "https://warszawawpigulce.pl/"
    url2 = "https://warszawawpigulce.pl/category/aktualnosci/"
    result2 = requests.get(url2)
    c2 = result2.content
    soup2 = BeautifulSoup(c2,'html.parser')
    stacje_metra = soup2.find_all("a",href=re.compile("stacj\w-metr*"))
    urls = set([i['href'] for i in stacje_metra])
    
    url2 = list(urls)[0]
    result2 = requests.get(url2)
    c2 = result2.content
    soup2 = BeautifulSoup(c2,'html.parser')
    
    #tml = soup2.prettify("utf-8")
    #ith open("stacja_metra.html", "wb") as file:
    #   file.write(html)
    
    #oup2.find("head")#, string=re.compile("Zamkni?te zosta?y stacje:"))
    zamkniete = soup2.head.find_all("meta", content=re.compile('y stacje:'))
    zamkniete = zamkniete[0]['content'].split(':')[-1].split(',')
    print zamkniete
    out = {}
    for i in zamkniete:
        out[i]=''
    return out
    
def um_aping():
    apikey = 'b04e52e7-0de1-4743-b510-82bbbf215320'
    resource_id = 'f2e5503e-927d-4ad3-9500-4ab9e55deb59'
    url = 'https://api.um.warszawa.pl/api/action/busestrams_get/'
    
    response = urllib2.urlopen('%s?resource_id=%s&apikey=%s&type=1'
                               %(url, resource_id, apikey))
    
    html_all_buses = response.read()
    #przyk?ad wywo?ania dla zapytania o tramwaj o numerze linii 7
    response = urllib2.urlopen('%s?resource_id=%s&apikey=%s&type=2'
                               %(url, resource_id, apikey))
    html_all_trams = response.read()
    j_all_buses = json.loads(html_all_buses)
    j_all_trams = json.loads(html_all_trams)
    
    
    eq_json = {"type":"FeatureCollection",
               "features":[
                   {"type":"Feature",
                    "properties":{"mag":5.4,
                                  "place":"48km SSE of Pondaguitan, Philippines",
                                  "time":1348176066,
                                  "tz":480,
                                  "url":"http://earthquake.usgs.gov/earthquakes/eventpage/usc000csx3",
                                  "felt":2,
                                  "cdi":3.4,
                                  "mmi":None,
                                  "alert":None,
                                  "status":"REVIEWED",
                                  "tsunami":None,
                                  "sig":"449",
                                  "net":"us",
                                  "code":"c000csx3",
                                  "ids":",usc000csx3,",
                                  "sources":",us,",
                                  "types":",dyfi,eq-location-map,general-link,geoserve,historical-moment-tensor-map,historical-seismicity-map,nearby-cities,origin,p-wave-travel-times,phase-data,scitech-link,tectonic-summary,"},
                    "geometry":{"type":"Point","coordinates":[126.3832,5.9775,111.16]},"id":"usc000csx3"}
               ]}
    
    import copy
    beg = 'eqfeed_callback('; end = ');'
    eq_json_bus = copy.deepcopy(eq_json)
    feature_json = {'geometry': {'coordinates': [21.132438, 52.247736], 'type': 'Point'},
     'id': 'usc000csx3',
     'properties': {'alert': None,
      'cdi': 3.4,
      'code': 'c000csx3',
      'felt': 2,
      'ids': ',usc000csx3,',
      'mag': 5.4,
      'mmi': None,
      'net': 'us',
      'place': '48km SSE of Pondaguitan, Philippines',
      'sig': '449',
      'sources': ',us,',
      'status': 'REVIEWED',
      'time': 1348176066,
      'tsunami': None,
      'types': ',dyfi,eq-location-map,general-link,geoserve,historical-moment-tensor-map,historical-seismicity-map,nearby-cities,origin,p-wave-travel-times,phase-data,scitech-link,tectonic-summary,',
      'tz': 480,
      'url': 'http://earthquake.usgs.gov/earthquakes/eventpage/usc000csx3'},
     'type': 'Feature'}
    features=[]
    for bus in j_all_buses.get('result')+j_all_trams.get('result'):
        dict_i = feature_json = {'geometry': {'coordinates': [bus['Lon'], bus['Lat']], 'type': 'Point'},
     'id': 'usc000csx3',
     'properties': {'alert': None,
      'cdi': 3.4,
      'code': 'c000csx3',
      'felt': 2,
      'ids': ',usc000csx3,',
      'mag': 5.4,
      'mmi': None,
      'net': 'us',
      'place': '48km SSE of Pondaguitan, Philippines',
      'sig': '449',
      'sources': ',us,',
      'status': 'REVIEWED',
      'time': 1348176066,
      'tsunami': None,
      'types': ',dyfi,eq-location-map,general-link,geoserve,historical-moment-tensor-map,historical-seismicity-map,nearby-cities,origin,p-wave-travel-times,phase-data,scitech-link,tectonic-summary,',
      'tz': 480,
      'url': 'http://earthquake.usgs.gov/earthquakes/eventpage/usc000csx3'},
     'type': 'Feature'}
        features.append(dict_i)
        #eq_json_bus['features'].append(dict_i.copy())
        #print eq_json_bus
    eq_json_bus['features'] = features
    new_line = beg + json.dumps(eq_json_bus) + end
    new_file = open('/home/soutys/hackathonowe/wawcodowe_2017_10/wawcode/nameless-fortress-22742/blog/static/new_file.js','w')
    new_file.write(new_line)
    new_file.flush()
    new_file.close()
    return
# Create your views here.

def mapy_ciepla(request):
    df = pd.read_csv('/home/soutys/hackathonowe/wawcodowe_2017_10/wawcode/nameless-fortress-22742/media/mapy_ciepla.csv')
    someDjangoVariable = {}
    for i, row in df[df['h']==7.00].iterrows():
        someDjangoVariable[str(row.dve_id)] = {'center' : {'lat' : row.lat, 'lng' : row.lng},
                                               'population': row.temp/100}
    #zamkniete = get_zamkniete()
    zamkniete = {}
    someDjangoVariable = json.dumps(someDjangoVariable)
    return render(request, "mapy_ciepla.html", {"someDjangoVariable": someDjangoVariable, "zamkniete": json.dumps(zamkniete)})

def home_page(request):
    opisy = Opis.objects.all()#.filter(visible=True).order_by("date").reverse()
    latest_post = BlogPost.objects.all().filter(visible=True).order_by("date").last()
    return render(request, "home.html", {"blog_post": latest_post, "opisy" : opisy})

#def home_page(request):
#    latest_post = BlogPost.objects.all().filter(visible=True).order_by("date").last()
#    return render(request, "home.html", {"blog_post": latest_post})

def about_page(request):
    ofirmies = OFirmie.objects.all()
    return render(request, "about.html", {"ofirmies": ofirmies})


def kontakt (request):
    return render(request, "kontakt.html")

def eq_advanced (request):
    df = pd.read_csv('/home/soutys/hackathonowe/wawcodowe_2017_10/wawcode/nameless-fortress-22742/media/mapy_ciepla.csv')
    dict_i = {"type":"FeatureCollection","features":[]}
    f = open('/home/soutys/hackathonowe/wawcodowe_2017_10/wawcode/nameless-fortress-22742/blog/static/tempy.js','w')
    begin = 'eqfeed_callback({"type":"FeatureCollection","features":['
    f.write(begin)
    for i, row in df[df['h']==7.00].iterrows():
        line = '{"type":"Feature","properties":{"mag":%f,"place":"140km WNW of Tobelo, Indonesia","time":1348156354,"tz":480,"url":"http://earthquake.usgs.gov/earthquakes/eventpage/usc000csku","felt":0,"cdi":1,"mmi":null,"alert":null,"status":"REVIEWED","tsunami":null,"sig":"385","net":"us","code":"c000csku","ids":",usc000csku,","sources":",us,","types":",dyfi,eq-location-map,general-link,geoserve,historical-moment-tensor-map,historical-seismicity-map,nearby-cities,origin,p-wave-travel-times,phase-data,scitech-link,tectonic-summary,"},"geometry":{"type":"Point","coordinates":[%f,%f,83.33]},"id":"%d"},'\
        %(row.temp/100.0, row.lng, row.lat, i)
        f.write(line+'\n')

    '''
        dict_i['features'].append({"type":"Feature",
                              "properties":{"mag":row.temp/100.0,
                                            "place":"150km S of False Pass, Alaska",
                                            "time":1347575985,
                                            "tz":-660,
                                            "url":"http://earthquake.usgs.gov/earthquakes/eventpage/ak10559055",
                                            "felt":None,"cdi":None,"mmi":None,"alert":None,
                                            "status":"REVIEWED","tsunami":None,"sig":"104",
                                            "net":"ak","code":"10559055","ids":",ak10559055,",
                                            "sources":",ak,",
                                            "types":",general-link,geoserve,nearby-cities,origin,tectonic-summary,"},
                              "geometry":{"type":"Point","coordinates":[row.lng,row.lat,64.8]},
                              "id":"ak10559055"})
    '''
    #line = 'eqfeed_callback(%s' %json.dumps({"type":"FeatureCollection","features":[]})[:-2]
    #for feature in dict_i['features']:
    #    line = json.dumps(feature)+',\n'
    #    f.write(line)        
    f.write(']});')
    f.flush()
    f.close()

    return render(request, "EQ_advanced.html")


def geolocation (request):
    um_aping()
    someDjangoVariable = {
        'chicago': {
          'center': {'lat': 41.878, 'lng': -87.629},
          'population': 2714856
        },
        'newyork': {
          'center': {'lat': 40.714, 'lng': -74.005},
          'population': 8405837
        },
        'losangeles': {
          'center': {'lat': 34.052, 'lng': -118.243},
          'population': 3857799
        },
        'wilanowska': {
          'center': {'lat': 52.1815113, 'lng': 21.02057849},
          'population': 6 
        },
        'centrum': {
          'center': {'lat': 52.2305263, 'lng': 21.0103370},
          'population': 6 
        }
      };
    zamkniete = get_zamkniete()
    someDjangoVariable = json.dumps(someDjangoVariable)
    return render(request, "geolocation.html", {"someDjangoVariable": someDjangoVariable, "zamkniete": json.dumps(zamkniete)})




def geolocation (request):
    um_aping()
    someDjangoVariable = {
        'chicago': {
          'center': {'lat': 41.878, 'lng': -87.629},
          'population': 2714856
        },
        'newyork': {
          'center': {'lat': 40.714, 'lng': -74.005},
          'population': 8405837
        },
        'losangeles': {
          'center': {'lat': 34.052, 'lng': -118.243},
          'population': 3857799
        },
        'wilanowska': {
          'center': {'lat': 52.1815113, 'lng': 21.02057849},
          'population': 6 
        },
        'centrum': {
          'center': {'lat': 52.2305263, 'lng': 21.0103370},
          'population': 6 
        }
      };
    zamkniete = get_zamkniete()
    someDjangoVariable = json.dumps(someDjangoVariable)
    return render(request, "geolocation.html", {"someDjangoVariable": someDjangoVariable, "zamkniete": json.dumps(zamkniete)})


def earthquakes (request):
    return render(request, "earthquakes.html")


def certyfikaty (request):
    return render(request, "certyfikaty.html")


def blog_page(request):
    blog_posts = BlogPost.objects.all().filter(visible=True).order_by("date").reverse()
    return render(request, "blog.html", {"blog_posts": blog_posts})


@login_required(login_url="/", redirect_field_name=None)
def new_post_page(request):
    if request.method == "POST":
        form = BlogPostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")
        else:
            return render(request, "new_post.html", {"form": form})
    form = BlogPostForm()
    return render(request, "new_post.html", {"form": form})


@login_required(login_url="/", redirect_field_name=None)
def edit_posts_page(request):
    blog_posts = BlogPost.objects.all().order_by("date").reverse()
    return render(request, "edit_posts.html", {"blog_posts": blog_posts})


@login_required(login_url="/", redirect_field_name=None)
def edit_post_page(request, post_id):
    blog_post = BlogPost.objects.get(pk=post_id)
    if request.method == "POST":
        form = BlogPostForm(request.POST, instance=blog_post)
        if form.is_valid():
            form.save()
            return redirect("/blog/")
        else:
            return render(request, "edit_post.html", {"form": form, "id": post_id})
    else:
        form = BlogPostForm(instance=blog_post)
        return render(request, "edit_post.html", {"form": form, "id": post_id})


@login_required(login_url="/", redirect_field_name=None)
def delete_post_page(request, post_id):
    doomed_post = BlogPost.objects.get(pk=post_id)
    if request.method == "POST":
        doomed_post.delete()
        return redirect("/blog/edit/")
    return render(request, "delete_post.html", {"title": doomed_post.title})
