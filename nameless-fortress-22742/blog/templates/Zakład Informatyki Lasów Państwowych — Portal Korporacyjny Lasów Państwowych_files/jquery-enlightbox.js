/**
 * jQuery Enhanced Lightbox plugin
 * This jQuery plugin is based on jQuery lightBox 0.5 by Leandro Vieira (http://leandrovieira.com/projects/jquery/lightbox/).
 * @name jquery-enlightbox.js
 * @author Eduardo Morais - http://www.eduardomorais.com
 * @version 1.1
 * @date February 19, 2012
 * @category jQuery plugin
 * @copyright (c) 2011-2012 Eduardo Morais, based on the work of Leandro Vieira Pinho (leandrovieira.com)
 * @license CCAttribution-ShareAlike 3.0 Unported - http://creativecommons.org/licenses/by-sa/3.0/
 * @example Visit http://www.eduardomorais.com/code/lightbox/ for more informations about this jQuery plugin
 */

// Offering a Custom Alias suport - More info: http://docs.jquery.com/Plugins/Authoring#Custom_Alias
(function($) {
	/**
	 * $ is an alias to jQuery object
	 *
	 */
	$.fn.lightBox = function(settings) {
		// Settings to configure the jQuery lightBox plugin how you like
		settings = jQuery.extend({
			// Configuration related to overlay
			overlayBgColor: 		'#000',		// (string) Background color to overlay; inform a hexadecimal value like: #RRGGBB. Where RR, GG, and BB are the hexadecimal values for the red, green, and blue values of the color.
			overlayOpacity:			0.95,		// (integer) Opacity value to overlay; inform: 0.X. Where X are number from 0 to 9
			// Configuration related to navigation
			fixedNavigation:		true,		// (boolean) Boolean that informs if the navigation (next and prev button) will be fixed or not in the interface.
			// Configuration related to images
			imageLoading:			'/nfh-cp-tiles-theme/images/enlightbox/lightbox-loading.gif',		// (string) Path and the name of the loading icon
			imageBtnPrev:			'/nfh-cp-tiles-theme/images/enlightbox/lightbox-btn-prev.png',	// (string) Path and the name of the prev button image
			imageBtnNext:			'/nfh-cp-tiles-theme/images/enlightbox/lightbox-btn-next.png',	// (string) Path and the name of the next button image
			imageBtnClose:			'/nfh-cp-tiles-theme/images/enlightbox/lightbox-btn-close.png',	// (string) Path and the name of the close btn
			imageBlank:				'/nfh-cp-tiles-theme/images/enlightbox/lightbox-blank.gif',	// (string) Path and the name of a blank image (one pixel)
			imageCurZoom:			'/nfh-cp-tiles-theme/images/enlightbox/lightbox-zoom',			// (string) Path and the name of the zoom mouse cursor w/ NO FILE EXTENSION 
																	// 			-- we need both the .cur and .png for it to work on all browsers
																	
			// Configuration related to container image box
			containerBorderSize:	5,			// (integer) If you adjust the padding in the CSS for the container, #lightbox-container-image-box, you will need to update this value
			containerResizeSpeed:	200,		// (integer) Specify the resize duration of container image. These number are miliseconds. 400 is default.
			// Configuration related to texts in caption. For example: Image 2 of 8. You can alter either "Image" and "of" texts.
			txtImage:				'Zdjęcie',	// (string) Specify text "Image"
			txtOf:					'z',		// (string) Specify text "of"
			txtClose:				'Zamknij',	// (string) Specify text "Close"
			// Configuration related to keyboard navigation
			keyToClose:				'c',		// (string) (c = close) Letter to close the jQuery lightBox interface. Beyond this letter, the letter X and the SCAPE key is used to.
			keyToPrev:				'p',		// (string) (p = previous) Letter to show the previous image
			keyToNext:				'n',		// (string) (n = next) Letter to show the next image.
			// Don�t alter these variables in any way
			imageArray:				[],
			activeImage:			0,
			zoom:					0,
			zoomable:				0
		},settings);
		// Minified by the Closure Compiler (http://closure-compiler.appspot.com/home):
		var jQueryMatchedObj=this;function _initialize(){_start(this,jQueryMatchedObj);return!1}
function _start(a,b){$("embed, object, select").css({visibility:"hidden"});_set_interface();settings.imageArray.length=0;settings.activeImage=0;if(1==b.length)settings.imageArray.push([a.getAttribute("href"),a.getAttribute("title"),a.getAttribute("data-info"),a.getAttribute("data-maximized")]);else for(var c=0;c<b.length;c++)settings.imageArray.push([b[c].getAttribute("href"),b[c].getAttribute("title"),b[c].getAttribute("data-info"),b[c].getAttribute("data-maximized")]);for(;settings.imageArray[settings.activeImage][0]!=
a.getAttribute("href");)settings.activeImage++;_set_image_to_view()}
function _set_interface(){$("body").append('<div id="jquery-overlay"></div><div id="jquery-lightbox"><div id="lightbox-container-image-box"><div id="lightbox-container-image"><img id="lightbox-image" /><div style="" id="lightbox-nav"><a href="#" id="lightbox-nav-btnPrev"></a><a href="#" id="lightbox-nav-btnZoom"></a><a href="#" id="lightbox-nav-btnNext"></a></div><div id="lightbox-loading"><a href="#" id="lightbox-loading-link"><img src="'+settings.imageLoading+'" /></a></div></div></div><div id="lightbox-container-image-data-box"><div id="lightbox-container-image-data"><div id="lightbox-image-details"><div id="lightbox-secNav"><a href="#" id="lightbox-secNav-btnClose"><img src="'+
settings.imageBtnClose+'" /></a></div><span id="lightbox-image-details-caption"></span><span id="lightbox-image-details-currentNumber"></span></div></div></div></div>');___getPageSize();$("#jquery-overlay").css({backgroundColor:settings.overlayBgColor,opacity:settings.overlayOpacity,width:$(document).width(),height:$(document).height(),top:0}).fadeIn();$("#jquery-overlay,#jquery-lightbox").click(function(){_finish()});$("#lightbox-loading-link,#lightbox-secNav-btnClose").click(function(){_finish();
return!1});$(window).resize(function(){$("#jquery-overlay").css({width:$(document).width()});var a=___getPageScroll();$("#jquery-lightbox").css({top:a[1]+$(window).height()/20,left:a[0]});settings.zoom=1;$("#lightbox-container-image-data-box").css({left:Math.round(($(window).width()-$("#lightbox-container-image-data-box").width())/2)-5})})}
function _set_image_to_view(){var a=___getPageScroll();$("#jquery-lightbox").css({top:a[1]+$(window).height()/20,left:a[0]}).show();$("#lightbox-loading").show();settings.fixedNavigation?$("#lightbox-image,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber").hide():$("#lightbox-image,#lightbox-nav,#lightbox-nav-btnPrev,#lightbox-nav-btnNext,#lightbox-nav-btnZoom,#lightbox-container-image-data-box,#lightbox-image-details-currentNumber").hide();var b=new Image;b.onload=function(){$("#lightbox-image").attr("src",
settings.imageArray[settings.activeImage][0]);maximize=0;"always"==settings.imageArray[settings.activeImage][3]&&(maximize=1);imgW=b.width;imgH=b.height;limit=$(window).height()-$(window).height()/5;settings.zoomable=imgH>limit&&0==maximize?1:0;0==settings.zoom&&1==settings.zoomable&&(par=imgW/imgH,imgH=limit,imgW=Math.round(imgH*par));$("#lightbox-image").attr("width",imgW);$("#lightbox-image").attr("height",imgH);_resize_container_image_box(imgW,imgH);b.onload=function(){}};b.src=settings.imageArray[settings.activeImage][0]}
function _resize_container_image_box(a,b){var c=$("#lightbox-container-image-box").width(),d=$("#lightbox-container-image-box").height(),e=a+2*settings.containerBorderSize,f=b+2*settings.containerBorderSize,c=c-e,d=d-f;$("#lightbox-container-image-box").animate({width:e,height:f},settings.containerResizeSpeed,function(){_show_image()});0==c&&0==d&&($.browser.msie?___pause(250):___pause(100));$("#lightbox-container-image-data-box").css({width:e-10});$("#lightbox-container-image-data-box").css({left:Math.round(($(window).width()-
$("#lightbox-container-image-data-box").width())/2)-5});$("#lightbox-nav-btnPrev,#lightbox-nav-btnNext,#lightbox-nav-btnZoom").css({height:b+2*settings.containerBorderSize})}function _show_image(){$("#lightbox-loading").hide();$("#lightbox-image").fadeIn(function(){_show_image_data();_set_navigation()});_preload_neighbor_images()}
function _show_image_data(){var a=settings.imageArray[settings.activeImage][1],b=settings.imageArray[settings.activeImage][2],c=0;$("#lightbox-image-details-caption").hide();a&&(b||(b=""),$("#lightbox-container-image-data-box").slideDown("fast"),c=1,$("#lightbox-image-details-caption").html('<span class="lightbox-title">'+a+"</span>"+b).fadeIn());1<settings.imageArray.length&&(0==c&&$("#lightbox-container-image-data-box").slideDown("fast"),$("#lightbox-image-details-currentNumber").html(settings.txtImage+
" "+(settings.activeImage+1)+" "+settings.txtOf+" "+settings.imageArray.length).fadeIn(1500))}
function _set_navigation(){$("#lightbox-nav").show();$("#lightbox-nav-btnPrev,#lightbox-nav-btnNext,#lightbox-nav-btnZoom").css({background:"transparent url("+settings.imageBlank+") no-repeat"});0!=settings.activeImage&&(settings.fixedNavigation?$("#lightbox-nav-btnPrev").css({background:"url("+settings.imageBtnPrev+") left 50% no-repeat"}).unbind().bind("click",function(){settings.activeImage-=1;settings.zoom=0;_set_image_to_view();return!1}):$("#lightbox-nav-btnPrev").unbind().hover(function(){$(this).css({background:"url("+
settings.imageBtnPrev+") left 50% no-repeat"})},function(){$(this).css({background:"transparent url("+settings.imageBlank+") no-repeat"})}).show().bind("click",function(){settings.activeImage-=1;settings.zoom=0;_set_image_to_view();return!1}));settings.activeImage!=settings.imageArray.length-1&&(settings.fixedNavigation?$("#lightbox-nav-btnNext").css({background:"url("+settings.imageBtnNext+") right 50% no-repeat"}).unbind().bind("click",function(){settings.activeImage+=1;settings.zoom=0;_set_image_to_view();
return!1}):$("#lightbox-nav-btnNext").unbind().hover(function(){$(this).css({background:"url("+settings.imageBtnNext+") right 50% no-repeat"})},function(){$(this).css({background:"transparent url("+settings.imageBlank+") no-repeat"})}).show().bind("click",function(){settings.activeImage+=1;settings.zoom=0;_set_image_to_view();return!1}));1==settings.zoomable&&$("#lightbox-nav-btnZoom").unbind().hover(function(){$(this).css({cursor:"url("+settings.imageCurZoom+".cur),url("+settings.imageCurZoom+".png), move"})},
function(){$(this).css({cursor:"crosshair"})}).show().bind("click",function(){settings.zoom+=1;1<settings.zoom&&(settings.zoom=0);_set_image_to_view();return!1});_enable_keyboard_navigation()}function _enable_keyboard_navigation(){$(document).keydown(function(a){_keyboard_action(a)})}function _disable_keyboard_navigation(){$(document).unbind()}
function _keyboard_action(a){null==a?(keycode=event.keyCode,escapeKey=27):(keycode=a.keyCode,escapeKey=a.DOM_VK_ESCAPE);key=String.fromCharCode(keycode).toLowerCase();(key==settings.keyToClose||"x"==key||keycode==escapeKey)&&_finish();if((key==settings.keyToPrev||37==keycode)&&0!=settings.activeImage)settings.activeImage-=1,settings.zoom=0,_set_image_to_view(),_disable_keyboard_navigation();if((key==settings.keyToNext||39==keycode)&&settings.activeImage!=settings.imageArray.length-1)settings.activeImage+=
1,settings.zoom=0,_set_image_to_view(),_disable_keyboard_navigation()}function _preload_neighbor_images(){settings.imageArray.length-1>settings.activeImage&&(objNext=new Image,objNext.src=settings.imageArray[settings.activeImage+1][0]);0<settings.activeImage&&(objPrev=new Image,objPrev.src=settings.imageArray[settings.activeImage-1][0])}
function _finish(){settings.zoom=0;$("#jquery-lightbox").remove();$("#jquery-overlay").fadeOut(function(){$("#jquery-overlay").remove()});$("embed, object, select").css({visibility:"visible"})}
function ___getPageSize(){var a,b;window.innerHeight&&window.scrollMaxY?(a=window.innerWidth+window.scrollMaxX,b=window.innerHeight+window.scrollMaxY):document.body.scrollHeight>document.body.offsetHeight?(a=document.body.scrollWidth,b=document.body.scrollHeight):(a=document.body.offsetWidth,b=document.body.offsetHeight);var c,d;self.innerHeight?(c=document.documentElement.clientWidth?document.documentElement.clientWidth:self.innerWidth,d=self.innerHeight):document.documentElement&&document.documentElement.clientHeight?
(c=document.documentElement.clientWidth,d=document.documentElement.clientHeight):document.body&&(c=document.body.clientWidth,d=document.body.clientHeight);pageHeight=b<d?d:b;pageWidth=a<c?a:c;return arrayPageSize=[pageWidth,pageHeight,c,d]}
function ___getPageScroll(){var a,b;self.pageYOffset?(b=self.pageYOffset,a=self.pageXOffset):document.documentElement&&document.documentElement.scrollTop?(b=document.documentElement.scrollTop,a=document.documentElement.scrollLeft):document.body&&(b=document.body.scrollTop,a=document.body.scrollLeft);return arrayPageScroll=[a,b]}function ___pause(a){var b=new Date;do var c=new Date;while(c-b<a)};
		return this.unbind('click').click(_initialize);
	};
})(jQuery); // Call and execute the function immediately passing the jQuery object