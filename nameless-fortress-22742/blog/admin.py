from django.contrib import admin

from .models import OFirmie, Opis, Produkt


admin.site.register(OFirmie)
admin.site.register(Opis)
admin.site.register(Produkt)
#
