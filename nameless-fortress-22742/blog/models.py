from django.db import models
from samdown import process_samdown
import os

# Create your models here.
class BlogPost(models.Model):
    title = models.TextField()
    date = models.DateField(unique=True)
    body = models.TextField()
    visible = models.BooleanField()

    @property
    def formatted_body(self):
        return process_samdown(self.body)

# Create your models here.
class OFirmie(models.Model):
    title = models.TextField()
#    date = models.DateField(unique=True)
    body = models.TextField()
#    visible = models.BooleanField()

#    @property
#    def formatted_body(self):
#        return process_samdown(self.body)

# Create your models here.
class Opis(models.Model):
    title = models.TextField()
#    date = models.DateField(unique=True)
    body = models.TextField()
#    visible = models.BooleanField()

#    @property
#    def formatted_body(self):
#        return process_samdown(self.body)

import os

def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)

class Produkt (models.Model):


    nazwa = models.TextField()
    
    WymiaryTechniczne = models.TextField()
    #os.system('mkdir -p pic_folder/'+self.pk )
    model_pic = models.ImageField(upload_to = get_image_path, default = 'pic_folder/None/no-img.jpg')


class ExampleModel(models.Model):
    model_pic = models.ImageField(upload_to = 'pic_folder/', default = 'pic_folder/None/no-img.jpg')

class Picture(models.Model):
    model_pic = models.ImageField(upload_to = get_image_path, default = 'pic_folder/None/no-img.jpg')
    produkt = models.ForeignKey(Produkt, on_delete=models.CASCADE)
    
