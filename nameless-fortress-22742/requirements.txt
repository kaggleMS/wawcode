django==1.8.11
gunicorn==18.0
django_cleanup==0.4.2
psycopg2
requests
django-photologue==3.7
cloudinary==1.8.0
# The following is only required if you plan on using Amazon AWS S3
-r example_storages/s3_requirements.txt
