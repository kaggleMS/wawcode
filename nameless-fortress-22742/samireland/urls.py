"""samireland URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from blog import urls as blog_urls
from piano import urls as piano_urls
from health import urls as health_urls
from media import urls as media_urls
from account import urls as account_urls
from blog.views import about_page, home_page, kontakt, certyfikaty,\
geolocation, earthquakes, mapy_ciepla, eq_advanced
from django.contrib import admin
from django.views.static import serve
from photologue.sitemaps import GallerySitemap, PhotoSitemap
from django.views.generic import TemplateView

sitemaps = {
            'photologue_galleries': GallerySitemap,
            'photologue_photos': PhotoSitemap,
            }



urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^blog/', include(blog_urls)),
    url(r'^piano/', include(piano_urls)),
    url(r'^health/', include(health_urls)),
    url(r'^media/', include(media_urls)),
    url(r'^account/', include(account_urls)),
    url(r'^about/$', about_page, name="about_page"),
    url(r'^kontakt/$', kontakt, name="kontakt"),
    url(r'^eq_advanced/$', eq_advanced, name="eq_advanced"),
    url(r'^geolocation/$', geolocation, name="geolocation"),
    url(r'^mapy_ciepla/$', mapy_ciepla, name="mapy_ciepla"),
    url(r'^earthquakes/$', earthquakes, name="earthquakes"),
    url(r'^certyfikaty/$', certyfikaty, name="certyfikaty"),
    url(r'^homepage/$', TemplateView.as_view(template_name="homepage.html"), name='_photologue_homepage'),
    url(r'^photologue/', include('photologue.urls', namespace='photologue')),    
    url(r'^$', home_page, name="home_page")
] + static(
 settings.MEDIA_URL,
 document_root=settings.MEDIA_ROOT
)

#if settings.DEBUG:
#    urlpatterns += [
#        url(r'^media/(?P<path>.*)$', serve, {
#            'document_root': "",#  settings.MEDIA_ROOT,
#        }),
#    ]

