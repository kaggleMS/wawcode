SECRET_KEY = 'tjw0#7*)#lqyl4%x1x+81#x@!ht*9f$@qlkoc*=fn48&g+5q(s'

from . import secret_settings
#from django.contrib import admin



"""
Django settings for samireland project.

Generated by 'django-admin startproject' using Django 1.8.4.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = secret_settings.SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['zakladdrzewnyrzadwladyslawa.localtunnel.me', '127.0.0.1','0.0.0.0','5f4ac9ee.ngrok.io', '2ad0fb91.ngrok.io','f7d16afc.ngrok.io','zakladrzad.localtunnel.me',
                 'zakladrzad2.localtunnel.me']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django_cleanup',
    'blog',
    'piano',
    'media',
    'account',
    'health',
     'photologue',
     'sortedm2m',
#    'admin'
)

SITE_ID = 1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'samireland.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "samireland", "templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]



WSGI_APPLICATION = 'samireland.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = secret_settings.DATABASES


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

DATE_FORMAT = 'j F, Y'

USE_I18N = True

USE_L10N = False

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "samireland", "static")
]

MEDIA_ROOT =  os.path.join(BASE_DIR, "media")#"media")#, "images")
MEDIA_URL = '/media/photologue/'

# settings.py:

PHOTOLOGUE_PATH = 'piano.utils.get_image_path'

print PHOTOLOGUE_PATH
#PHOTOLOGUE_PATH = ''
# teraz trzeba zmajstrowac swojego template na wzor tego photo detail moze

# Uncomment this for Amazon S3 file storage
from example_storages.settings_s3boto import *
